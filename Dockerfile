FROM registry.gitlab.com/gamerscomplete/commandcontrol/command-control:latest

#Asset Store
COPY artifacts/asset-store /CommandControl/modules/asset-store/asset-store
COPY services/asset-store/service.cfg /CommandControl/modules/asset-store/service.cfg

#Authentication
COPY artifacts/authentication /CommandControl/modules/authentication/authentication
COPY services/authentication/service.cfg /CommandControl/modules/authentication/service.cfg

#Control-stream
COPY artifacts/control-stream /CommandControl/modules/control-stream/control-stream
COPY services/control-stream/service.cfg /CommandControl/modules/control-stream/service.cfg

#Game-generator
COPY artifacts/game-generator /CommandControl/modules/game-generator/game-generator
COPY services/game-generator/service.cfg /CommandControl/modules/game-generator/service.cfg

#Game-server
COPY artifacts/game-server /CommandControl/modules/game-server/game-server
COPY services/game-server/service.cfg /CommandControl/modules/game-server/service.cfg

#Maestro
COPY artifacts/maestro /CommandControl/modules/maestro/maestro
COPY services/maestro/service.cfg /CommandControl/modules/maestro/service.cfg

#state-engine
COPY artifacts/state-engine /CommandControl/modules/state-engine/state-engine
COPY services/state-engine/service.cfg /CommandControl/modules/state-engine/service.cfg

#Launch defaults
CMD ["/CommandControl/genesis", "-service", "cli,maestro", "--log_level", "1"]
