package maestroTypes

import (
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/uuid"
)

type GetGameServer struct {
	Region mapping.Region
}

type GetGameServerReply struct {
	GameServer GameServer
}

type GameServer struct {
	Addr string
	Port int
}

type GetServicesReply struct {
	Services map[string][]uuid.UUID
}

type GetRunningRegionsReply struct {
	Regions []mapping.Region
}

type ShutdownGameserverRequest struct {
	ID uuid.UUID
}

type ShutdownGameserverReply struct {
}
