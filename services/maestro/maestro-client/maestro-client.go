package maestroClient

import (
	"errors"
	"fmt"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/maestro/maestro-types"
	"gitlab.com/gamerscomplete/uuid"
)

const (
	MAESTRO_SERVICE_NAME = "maestro"
	DEFAULT_TIMEOUT      = 10
)

type Client struct {
	conn    *csTools.ServerConnObj
	localID uuid.UUID
}

func NewClient(conn *csTools.ServerConnObj) *Client {
	localID := conn.ID
	return &Client{conn: conn, localID: localID}
}

func (client *Client) GetGameServerFromRegion(region mapping.Region) (maestroTypes.GetGameServerReply, error) {
	if region[0] == 0 || region[1] == 0 || region[2] == 0 {
		return maestroTypes.GetGameServerReply{}, errors.New("Invalid region to get gameserver")
	}

	gameServerRequest := maestroTypes.GetGameServer{Region: region}
	gameServer := maestroTypes.GetGameServerReply{}

	if err := client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GET_GAME_SERVER", MAESTRO_SERVICE_NAME, client.conn.ID, gameServerRequest, &gameServer); err != nil {
		return gameServer, fmt.Errorf("Fetching gameserver from region failed: %v", err)
	}

	if gameServer.GameServer.Addr == "" || gameServer.GameServer.Port == 0 {
		return gameServer, errors.New("Invalid gameserver connection details recieved")
	}

	return gameServer, nil
}

func (client *Client) GetGameServerFromPosition(position mapping.Position) (maestroTypes.GetGameServerReply, error) {
	return client.GetGameServerFromRegion(position.Region())
}

func (client *Client) GetServicesManaged() (map[string][]uuid.UUID, error) {
	var getServicesReply maestroTypes.GetServicesReply
	if err := client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GET_SERVICES_MANAGED", MAESTRO_SERVICE_NAME, client.conn.ID, nil, &getServicesReply); err != nil {
		return nil, fmt.Errorf("Fetching services managed failed: %v", err)
	}
	return getServicesReply.Services, nil
}

func (client *Client) GetRunningRegions() ([]mapping.Region, error) {
	var getRunningRegionsReply maestroTypes.GetRunningRegionsReply
	if err := client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GET_RUNNING_REGIONS", MAESTRO_SERVICE_NAME, client.conn.ID, nil, &getRunningRegionsReply); err != nil {
		return nil, fmt.Errorf("Fetching running regions failed: %v", err)
	}
	return getRunningRegionsReply.Regions, nil
}

//This tells maestro to manage the shutdown of a game server, and it will startup a new one to manage the region
func (client *Client) ShutdownGameserver(ID uuid.UUID) error {
	shutdownGameserverRequest := maestroTypes.ShutdownGameserverRequest{ID: ID}
	var shutdownGameserverReply maestroTypes.ShutdownGameserverReply
	if err := client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "SHUTDOWN_GAMESERVER", MAESTRO_SERVICE_NAME, client.conn.ID, shutdownGameserverRequest, &shutdownGameserverReply); err != nil {
		return fmt.Errorf("Shutdown request failed: %v", err)
	}
	return nil
}
