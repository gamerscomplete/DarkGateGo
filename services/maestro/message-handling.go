package main

import (
	log "github.com/sirupsen/logrus"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/maestro/maestro-types"
)

func processGetGameServer(message csTools.ServerMessage) {
	gameServerRequest := maestroTypes.GetGameServer{}

	replyMessage := csTools.Message{Command: "REPLY"}

	if err := message.Data.UnpackType(&gameServerRequest); err != nil {
		log.Warn("Failed to unpack game server request", err)
		replyMessage.Error = "Failed to unpack game server request"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	gameserver, err := getGameServerForRegion(gameServerRequest.Region)
	if err != nil {
		replyMessage.Error = err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return

	}
	reply := maestroTypes.GetGameServerReply{GameServer: gameserver}

	//TODO: Check for game servers that are running and return the game server serving that area. If no game server is serving that area then start a new gameserver
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}

/*
func getGameServerFromRegion(region mapping.Region) (maestroTypes.GameServer, error) {
	for key, value := range _gameServers {
		for _, regionValue := range value {
			if region == regionValue {
				return key, nil
			}
		}
	}
	return maestroTypes.GameServer{}, errors.New("Region not found")
}
*/
func processGetServicesManaged(message csTools.ServerMessage) {
	reply := maestroTypes.GetServicesReply{Services: _servicesManaged}
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		_serverConn.SendServerMessageReply(message, csTools.Message{Command: "REPLY", Parameters: "FAILED"})
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processGetRunningRegions(message csTools.ServerMessage) {
	reply := maestroTypes.GetRunningRegionsReply{}
	var regionList []mapping.Region
	for _, value := range _gameServers {
		regionList = append(regionList, value...)
	}
	reply.Regions = regionList
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		_serverConn.SendServerMessageReply(message, csTools.Message{Command: "REPLY", Parameters: "FAILED"})
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processShutdownGameserver(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	//TODO: Check to make sure we are managing this server before attempting to shut it down

	shutdownGameserverRequest := maestroTypes.ShutdownGameserverRequest{}
	if err := message.Data.UnpackType(&shutdownGameserverRequest); err != nil {
		log.Warn("Failed to unpack game server shutdown request", err)
		replyMessage.Error = "Failed to unpack request"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	if err := _gameServerClient.ShutdownServer(&shutdownGameserverRequest.ID); err != nil {
		replyMessage.Error = err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	//TODO: Handle removing this server from the list of running gameservers/services
	reply := maestroTypes.ShutdownGameserverReply{}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}
