package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/game-server/server-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/maestro/maestro-types"
	"gitlab.com/gamerscomplete/uuid"
)

var (
	_serverConn      = csTools.NewServerConn()
	_localID         = csTools.GetID()
	_clusterServices map[time.Duration]string
	//_gameServices     = make(map[string][]uuid.UUID)
	_servicesManaged  = make(map[string][]uuid.UUID)
	_gameServers      = make(map[maestroTypes.GameServer][]mapping.Region)
	_gameServerClient serverClient.Client
)

const (
	SERVICE_NAME               = "maestro"
	CONTROL_SERVICE_NAME       = "control-stream"
	AUTH_SERVICE_NAME          = "authentication"
	GENERATOR_SERVICE_NAME     = "game-generator"
	GAME_SERVER_NAME           = "game-server"
	STATE_ENGINE_SERVICE_NAME  = "state-engine"
	ASSET_STORE_SERVICE_NAME   = "asset-store"
	WEB_INTERFACE_SERVICE_NAME = "web-interface"
	BOOTSTRAP_SERVICE_NAME     = "bootstrap"
	CHAT_SERVICE_NAME          = "chat"
)

func main() {
	_serverConn.RegisterHandler("GET_GAME_SERVER", processGetGameServer)
	_serverConn.RegisterHandler("GET_SERVICES_MANAGED", processGetServicesManaged)
	//	_serverConn.RegisterHandler("GET_RUNNING_REGIONS", )
	_serverConn.RegisterHandler("SHUTDOWN_GAMESERVER", processShutdownGameserver)

	if err := _serverConn.Init(SERVICE_NAME); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return
	}

	_gameServerClient = serverClient.NewClient(_serverConn)

	//Get initial list of services
	_clusterServices, err := _serverConn.GetClusterServices("")
	if err != nil {
		return
	}

	//Check if other maestro services running
	if _, found := _clusterServices.Services[SERVICE_NAME]; !found {
		//no peers
	} else {
		//peers
	}

	//Determine services that need to be running
	//Authentication

	if _, found := _clusterServices.Services[AUTH_SERVICE_NAME]; !found {
		if !startService(AUTH_SERVICE_NAME, _serverConn.ID) {
			log.Error("Failed to start " + AUTH_SERVICE_NAME + " service")
		} else {
			log.Info("Successfully started " + AUTH_SERVICE_NAME + " service")
		}
	}

	//state-engine
	if _, found := _clusterServices.Services[STATE_ENGINE_SERVICE_NAME]; !found {
		if !startService(STATE_ENGINE_SERVICE_NAME, _serverConn.ID) {
			log.Error("Failed to start" + STATE_ENGINE_SERVICE_NAME + " service")
		} else {
			log.Info("Successfully started " + STATE_ENGINE_SERVICE_NAME + " service")
		}
	}

	//asset-store
	if _, found := _clusterServices.Services[ASSET_STORE_SERVICE_NAME]; !found {
		if !startService(ASSET_STORE_SERVICE_NAME, _serverConn.ID) {
			log.Error("Failed to start" + ASSET_STORE_SERVICE_NAME + " service")
		} else {
			log.Info("Successfully started " + ASSET_STORE_SERVICE_NAME + " service")
		}
	}

	//web-interface
	if _, found := _clusterServices.Services[WEB_INTERFACE_SERVICE_NAME]; !found {
		if !startService(WEB_INTERFACE_SERVICE_NAME, _serverConn.ID) {
			log.Error("Failed to start" + WEB_INTERFACE_SERVICE_NAME + " service")
		} else {
			log.Info("Successfully started " + WEB_INTERFACE_SERVICE_NAME + " service")
		}
	}

	//game-generator
	if _, found := _clusterServices.Services[GENERATOR_SERVICE_NAME]; !found {
		if !startService(GENERATOR_SERVICE_NAME, _serverConn.ID) {
			log.Error("Failed to start " + GENERATOR_SERVICE_NAME + " service")
		} else {
			log.Info("Successfully started " + GENERATOR_SERVICE_NAME + " service")
		}
	}

	//bootstrap
	if _, found := _clusterServices.Services[BOOTSTRAP_SERVICE_NAME]; !found {
		if !startService(BOOTSTRAP_SERVICE_NAME, _serverConn.ID) {
			log.Error("Failed to start " + BOOTSTRAP_SERVICE_NAME + " service")
		} else {
			log.Info("Successfully started " + BOOTSTRAP_SERVICE_NAME + " service")
		}
	}

	//Control
	if _, found := _clusterServices.Services[CONTROL_SERVICE_NAME]; !found {
		if !startService(CONTROL_SERVICE_NAME, _serverConn.ID) {
			log.Error("Failed to start " + CONTROL_SERVICE_NAME + " service")
		} else {
			log.Info("Successfully started " + CONTROL_SERVICE_NAME + " service")
		}
	}

	//Chat
	if _, found := _clusterServices.Services[CHAT_SERVICE_NAME]; !found {
		if !startService(CHAT_SERVICE_NAME, _serverConn.ID) {
			log.Error("Failed to start " + CHAT_SERVICE_NAME + " service")
		} else {
			log.Info("Successfully started " + CHAT_SERVICE_NAME + " service")
		}
	}

	//gameserver
	//Issue commands to start services
	csTools.Wait()
	shutdown()
}

func getGameServerForRegion(region mapping.Region) (maestroTypes.GameServer, error) {
	var gameServer maestroTypes.GameServer
	for key, value := range _gameServers {
		for _, regionValue := range value {
			if region == regionValue {
				//Found the game server
				gameServer = key
			}
		}
	}

	if gameServer == (maestroTypes.GameServer{}) {
		log.Info("Do not have a game server running. Starting one")
		gameServerReply, err := startGameServer(region)
		if err != nil {
			log.Error("Failed to start game-server", err)
			return gameServer, err
		}
		log.Info("Started game-server successfully")
		var regionList []mapping.Region
		regionList = append(regionList, region)
		_gameServers[gameServerReply] = regionList
		gameServer = gameServerReply
	}

	return gameServer, nil
}

func startService(serviceName string, nodeID uuid.UUID) bool {
	//TODO: Check if service is already running.

	if err := _serverConn.StartService(nodeID, serviceName); err != nil {
		log.Error("Service start query failed.", err)
		return false
	}

	//Check if we have the service already a managed service
	if _, found := _servicesManaged[serviceName]; found {
		//We already have some of these services managed, append
		_servicesManaged[serviceName] = append(_servicesManaged[serviceName], nodeID)
	} else {
		//Not already managing any of these services, add an empty
		_servicesManaged[serviceName] = []uuid.UUID{nodeID}
	}

	return true
}

//This should probably be generalized down to services rather than specific on game servers
func shutdownGameserver(ID uuid.UUID) {
	//Send shutdown request to the gameserver itself
}

func startGameServer(region mapping.Region) (maestroTypes.GameServer, error) {
	//TODO: Proper placement
	serverNodeID := _serverConn.ID
	if !startService(GAME_SERVER_NAME, serverNodeID) {
		return maestroTypes.GameServer{}, errors.New("Game-server failed to start")
	}
	log.Debug("game-server service started successfully")
	//Fire a init packet
	var regionList []mapping.Region
	regionList = append(regionList, region)
	log.Info("Starting game-server at nodeID: ", serverNodeID.String(), " In region: ", region)
	gameServer, err := _gameServerClient.InitServer(&serverNodeID, regionList)
	if err != nil {
		return gameServer, fmt.Errorf("init failed: %v", err)
	}
	log.Info("Game server initialized successfully")
	return gameServer, nil
}

func shutdown() {
	log.Info("Shutdown called")
	os.Exit(0)
}
