//TODO: Rework the whole client connection to use the same setup CC is using with an error built in
package main

import (
	"encoding/gob"
	"errors"
	log "github.com/sirupsen/logrus"
	"io"
	"net"
	"os"
	"strconv"
	"sync"
	"time"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/uuid"
	//	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/asset-store/asset-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/chat/chat-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/control-stream/control-types"
	"gitlab.com/gamerscomplete/DarkGateGo/services/control-stream/game-control-types"
	"gitlab.com/gamerscomplete/DarkGateGo/services/maestro/maestro-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"
)

var (
	_serverConn           = csTools.NewServerConn()
	_connectedClients     clientConnections
	stateEngineClient     *stateClient.Client
	assetStoreClient      *assetClient.Client
	maestro               *maestroClient.Client
	_authenticationClient *authenticationClient.Client
	_chatClient           *chatClient.Client

	//This is just being used for short cutting orchestration. TODO: FIX
	localID = csTools.GetID()
)

//Is this even used?
type client struct {
	Conn          net.Conn
	Encoder       *gob.Encoder
	Decoder       *gob.Decoder
	PlayerID      uuid.UUID
	Token         string
	Authenticated bool
}

type clientConnections struct {
	//[random connection id]*client
	clients map[uuid.UUID]*client
	sync.RWMutex
}

const (
	CLIENT_LISTEN_PORT = 6897
	VERSION            = 1
	ZONE_LOAD_COUNT    = 3
)

func main() {
	log.SetLevel(log.InfoLevel)
	_serverConn.RegisterHandler("CHAT_BROADCAST", processChatBroadcast)

	if err := _serverConn.Init("control-stream"); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return
	}

	_connectedClients.clients = make(map[uuid.UUID]*client)

	gameControlTypes.RegisterTypes()

	stateEngineClient = stateClient.NewClient(_serverConn)
	assetStoreClient = assetClient.NewClient(_serverConn)
	maestro = maestroClient.NewClient(_serverConn)
	_chatClient = chatClient.NewClient(_serverConn)
	_authenticationClient = authenticationClient.NewClient(_serverConn)

	log.Info("Control service started")
	//Start listen socket for external requests
	go startClientSocket()
	csTools.Wait()
	shutdown()
}

func (connections *clientConnections) playerIDFromClientID(clientID *uuid.UUID) (uuid.UUID, error) {
	return uuid.Zero, errors.New("playerIDFromClientID not implemented")
}

func (connections *clientConnections) clientIDFromPlayerID(playerID *uuid.UUID) (uuid.UUID, error) {
	for key, value := range connections.clients {
		if value.PlayerID == *playerID {
			return key, nil
		}
	}
	return uuid.Zero, errors.New("Player not found")
}

func startClientSocket() {
	log.Debug("Starting listening socket")
	l, err := net.Listen("tcp", ":"+strconv.Itoa(CLIENT_LISTEN_PORT))
	if err != nil {
		log.Fatal("listen error: ", err)
	}
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Error("accept error:", err)
		}
		log.Debug("Recieved a new connection")
		go handleIncomingConnection(conn)
	}
}

func shutdown() {
	//TODO: Send clients info that control stream is disconecting and it should reattempt connection
	log.Info("Control stream shutting down")
	os.Exit(0)
}

func handleClientMessage(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	log.WithFields(log.Fields{
		"ClientID": clientID.String(),
		"type":     message.Type,
	}).Debug("Processing message")
	switch message.Type {
	case "AUTHENTICATE":
		processClientLogin(message, clientID)
	case "REGISTER":
		processClientRegistration(message, clientID)
	case "FETCH_ASSET":
		processClientAssetRequest(message, clientID)
	case "CHECK_UPDATE":
		processClientCheckUpdate(message, clientID)
	case "DISCONNECT":
		processClientDisconnect(clientID)
	case "SEND_CHAT_MESSAGE":
		processSendChatMessage(message, clientID)
	case "GET_ITEM_DEFINITION":
		processGetItemDefinition(message, clientID)
	case "GET_ENTITY_DEFINITION":
		processGetEntityDefinition(message, clientID)
	case "SET_SPAWN":
		processSetSpawn(message, clientID)
	default:
		log.WithFields(log.Fields{
			"type": message.Type,
		}).Error("Recieved unknown message type")
	}
}

/////////////////////////////
// Process client requests //
/////////////////////////////

func processSetSpawn(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	setSpawnMessage, asserted := message.Message.(gameControlTypes.SetSpawn)
	if !asserted {
		log.Warn("Failed to assert setSpawn request")
		replyToClient(message, clientID, nil)
		return
	}
	success, err := _authenticationClient.SetSpawn(setSpawnMessage.Position, setSpawnMessage.Rotation)
	if err != nil {
		log.Warn("failed to set spawn:", err)
		replyToClient(message, clientID, nil)
		return
	}
	replyToClient(message, clientID, success)
}

func processGetEntityDefinition(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	requestID, asserted := message.Message.(uuid.UUID)
	if !asserted {
		log.Warn("Failed to assert getEntityDefinition request")
		replyToClient(message, clientID, nil)
		return
	}
	definition, err := stateEngineClient.GetEntityDefinition(requestID)
	if err != nil {
		log.Warn("Failed to fetch entity definition:", err)
		replyToClient(message, clientID, nil)
		return
	}
	replyToClient(message, clientID, definition)
}

func processGetItemDefinition(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	requestID, asserted := message.Message.(uuid.UUID)
	if !asserted {
		log.Warn("Failed to assert getItemDefinition request")
		replyToClient(message, clientID, nil)
		return
	}
	log.WithFields(log.Fields{
		"ID": requestID.String(),
	}).Debug("recieved GET_ITEM_DEFINITION request")
	definition, err := stateEngineClient.GetItemDefinition(requestID)
	if err != nil {
		log.WithFields(log.Fields{
			"ID": requestID,
		}).Warn("Failed to fetch item definition:", err)
		replyToClient(message, clientID, nil)
		return
	}
	log.WithFields(log.Fields{
		"ID": requestID.String(),
	}).Info("successfully processed GET_ITEM_DEFINITION request")
	replyToClient(message, clientID, definition)
}

func processChatBroadcast(message csTools.ServerMessage) {
	log.Debug("Recieved chat broadcast")
	chatBroadcast := controlTypes.ChatBroadcast{}
	if err := message.Data.UnpackType(&chatBroadcast); err != nil {
		log.Error("Failed to unpack chat broadcast")
		return
	}
	for key, value := range _connectedClients.clients {
		log.WithFields(log.Fields{
			"playerID": value.PlayerID.String(),
		}).Info("sending chat broadcast")
		if err := sendToClient(key, "CHAT_BROADCAST", gameControlTypes.ChatMessage{Username: chatBroadcast.Username, Message: chatBroadcast.Message, Private: false}); err != nil {
			log.WithFields(log.Fields{
				"playerID": value.PlayerID.String(),
			}).Warn("Failed to send chat message to client:", err)
		}
	}
	_serverConn.SendServerMessageReply(message, csTools.Message{})

}

/*
func processSendClientChat(message csTools.ServerMessage) {
    sendClientChat := controlTypes.SendClientChat{}
    if err := message.Data.UnpackType(&sendClientChat); err != nil {
        log.Error("Failed to unpack sendClientChat")
        return
    }

	clientID := clientIDFromUUID(sendClientChat.

	sendToClient(clientID, "CHAT_MESSAGE", sendClientChat.Message{})
}
*/

func processSendChatMessage(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	log.Debug("Recieved request to send chat message")
	chatMessage, asserted := message.Message.(gameControlTypes.SendChatMessage)
	if !asserted {
		log.Warn("Failed to assert sendChatMessage request")
		replyToClient(message, clientID, gameControlTypes.SendChatMessageReply{Error: "Failed to assert message"})
		return
	}
	if !_connectedClients.clients[*clientID].Authenticated {
		log.Info("Unauthenticated client tried to send chat message")
		replyToClient(message, clientID, gameControlTypes.SendChatMessageReply{Error: "Must be authenticated to send chat message"})
	}
	log.Debug("Sending chat message")
	if err := _chatClient.SendChatMessage(_connectedClients.clients[*clientID].PlayerID, chatMessage.Message); err != nil {
		log.Info("Failed to send chat message", err)
		replyToClient(message, clientID, gameControlTypes.SendChatMessageReply{Error: "Failed to send chat message"})
		return
	}
	replyToClient(message, clientID, gameControlTypes.SendChatMessageReply{})
}

func processClientDisconnect(clientID *uuid.UUID) {
	log.Info("Client disconnected")
	removeClient(clientID)
}

func processClientRegistration(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	registrationRequest, asserted := message.Message.(gameControlTypes.Registration)
	if !asserted {
		log.Warn("Failed to assert request type")
		replyToClient(message, clientID, gameControlTypes.RegistrationReply{Success: false, Reason: "Invalid registration type"})
		return
	}

	log.Debug("Sending registration request")
	registrationReason, err := _authenticationClient.RegisterUser(registrationRequest.Username, registrationRequest.Password)
	if err != nil {
		registrationReason = "Server error"
	}

	var loginSuccess bool
	if registrationReason == "" {
		loginSuccess = true
	} else {
		loginSuccess = false
	}
	clientRegistrationReturn := gameControlTypes.RegistrationReply{Reason: registrationReason, Success: loginSuccess}

	log.Debug("Returning registration back to client:", clientRegistrationReturn, " Reason: ", registrationReason)
	replyToClient(message, clientID, clientRegistrationReturn)
}

func processClientAssetRequest(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	clientAssetRequest, found := message.Message.(gameControlTypes.AssetRequest)
	if !found {
		log.Warn("Failed to assert message as AssetRequest")
		//		replyToClient(message, clientID, gameControlTypes.AssetReply{Error: errors.New("Failed to assert message as AssetRequest")})
		replyToClient(message, clientID, gameControlTypes.AssetReply{Error: "Failed to assert message as AssetRequest"})
		return
	}

	log.Debug("Fetching asset:", clientAssetRequest.Asset.String())
	asset, err := assetStoreClient.FetchAsset(clientAssetRequest.Asset)
	if err != nil {
		log.Warn("Failed to fetch asset")
		//		replyToClient(message, clientID, gameControlTypes.AssetReply{Error: errors.New("Failed to fetch asset")})
		replyToClient(message, clientID, gameControlTypes.AssetReply{Error: "Failed to fetch asset"})
		return
	}

	replyToClient(message, clientID, gameControlTypes.AssetReply{Asset: asset})
}

func processClientCheckUpdate(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	replyToClient(message, clientID, gameControlTypes.VersionMessage{Version: VERSION})
}

func processClientLogin(message gameControlTypes.ControlMessage, clientID *uuid.UUID) {
	authRequest, found := message.Message.(gameControlTypes.AuthRequest)
	if !found {
		log.Warn("Failed to assert message as AuthRequest")
		replyToClient(message, clientID, gameControlTypes.AuthReply{Reason: "Server error"})
		return
	}

	authServiceReply, err := _authenticationClient.AuthenticateByPassword(authRequest.Username, authRequest.Password, _connectedClients.clients[*clientID].Conn.RemoteAddr().String())
	if err != nil {
		log.Error("Authentication request failed")
		replyToClient(message, clientID, gameControlTypes.AuthReply{Reason: "Server error"})
		return
	}

	if authServiceReply.Reason != "" {
		replyToClient(message, clientID, gameControlTypes.AuthReply{Reason: authServiceReply.Reason})
		return
	}

	//login successful
	updatedClient := _connectedClients.clients[*clientID]
	updatedClient.Authenticated = true
	updatedClient.Token = authServiceReply.Token
	updatedClient.PlayerID = *authServiceReply.PlayerID

	addClient(*clientID, updatedClient)

	//Fetch player position
	playerPosition, err := stateEngineClient.GetEntityPosition(*authServiceReply.EntityID)
	if err != nil {
		log.Error("Failed to get player position", err)
		replyToClient(message, clientID, gameControlTypes.AuthReply{Reason: "Server error"})
		return
	}

	//Fetch gameserver
	log.Info("Fetching game server")
	gameServerReply, err := maestro.GetGameServerFromPosition(playerPosition)
	if err != nil {
		log.Error("Failed to get game server:", err)
		replyToClient(message, clientID, gameControlTypes.AuthReply{Reason: "Server error"})
	}

	authReply := gameControlTypes.AuthReply{GameServer: gameServerReply.GameServer, Token: authServiceReply.Token, PlayerID: *authServiceReply.PlayerID}

	replyToClient(message, clientID, authReply)
}

func addClient(clientID uuid.UUID, newClient *client) {
	_connectedClients.Lock()
	_connectedClients.clients[clientID] = newClient
	if err := _chatClient.AddListener(newClient.PlayerID); err != nil {
		log.Warn("Failed to add chat client")
	}
	_connectedClients.Unlock()
}

/////////////////////////////////
// Comms with client
/////////////////////////////////
func sendToClient(clientID uuid.UUID, messageType string, message interface{}) error {
	controlMessage := gameControlTypes.ControlMessage{Type: messageType, Message: message}
	_connectedClients.RLock()
	defer _connectedClients.RUnlock()
	client, found := _connectedClients.clients[clientID]
	if !found {
		return errors.New("Attempted to send message to disconnected client")
	}
	if err := client.Encoder.Encode(controlMessage); err != nil {
		return err
	}
	return nil
}

func replyToClient(originalMessage gameControlTypes.ControlMessage, clientID *uuid.UUID, newMessage interface{}) {
	controlMessage := gameControlTypes.ControlMessage{ID: originalMessage.ID, Type: "REPLY", Time: time.Now(), Message: newMessage}
	_connectedClients.RLock()
	clientShit, found := _connectedClients.clients[*clientID]
	if !found {
		log.Warn("Attempted to send message to disconnected client")
		_connectedClients.RUnlock()
		return
	}
	_connectedClients.RUnlock()
	if err := clientShit.Encoder.Encode(controlMessage); err != nil {
		log.Error("Failed to encode message: ", err)
	}
}

func handleIncomingConnection(conn net.Conn) {
	log.Info("Recieved incoming connection")
	client := client{Conn: conn, Decoder: gob.NewDecoder(conn), Encoder: gob.NewEncoder(conn)}
	clientID, err := uuid.NewV4()
	if err != nil {
		log.Error("Failed to generate UUID for query")
	}
	_connectedClients.Lock()
	_connectedClients.clients[*clientID] = &client
	_connectedClients.Unlock()
	go controlClientHandler(clientID)
}

func controlClientHandler(clientID *uuid.UUID) {
	for {
		var message gameControlTypes.ControlMessage
		//TODO: Not locking, but dont want to lock while doing this operation. Need a more elegant solution here
		if _, found := _connectedClients.clients[*clientID]; !found {
			return
		}
		err := _connectedClients.clients[*clientID].Decoder.Decode(&message)
		log.Debug("Recieved message")
		if err == io.EOF {
			log.Warn("client hungup")
			removeClient(clientID)
			return
		} else if err != nil {
			log.Warn("Recieved error on client connection:", err)
		}
		go handleClientMessage(message, clientID)
	}
}

func removeClient(clientID *uuid.UUID) {
	deadClient, found := _connectedClients.clients[*clientID]
	if !found {
		//TOOD: Handle this more gracefully. If everything goes right, the client disconnects at the request, then the EOF causes another attempt, causing this error on success
		log.Error("Tried to remove a client that didnt exist")
		return
	}
	_connectedClients.Lock()
	if err := _chatClient.RemoveListener(deadClient.PlayerID); err != nil {
		log.Warn("Failed to remove listener for client")
	}
	delete(_connectedClients.clients, *clientID)
	_connectedClients.Unlock()
}
