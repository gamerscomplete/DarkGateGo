package controlClient

import (
	"encoding/gob"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/uuid"
	"io"
	"net"
	"strconv"
	"sync"
	"time"

	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/control-stream/game-control-types"
)

const (
	//	CONTROL_STREAM_PORT = 6897
	//	CONTROL_STREAM_ADDR = "gameserver.dark-gate.com"
	LOGIN_TIMEOUT       = time.Second * time.Duration(10)
	REGISTER_TIMEOUT    = time.Second * time.Duration(2)
	FETCH_ASSET_TIMEOUT = time.Second * time.Duration(60)
	DEFAULT_TIMEOUT     = time.Second * time.Duration(1)
)

type controlStream struct {
	Conn    net.Conn
	Encoder *gob.Encoder
	Decoder *gob.Decoder
}

type controlQueue struct {
	Queue map[uuid.UUID]chan interface{}
	sync.RWMutex
}

type Client struct {
	cStream     controlStream
	cQueue      controlQueue
	chatHandler func(string, string)
}

func NewClient(connectionAddr string, connectionPort int) (*Client, error) {
	client, err := establishControlStream(connectionAddr, connectionPort)
	if err != nil {
		return client, err
	}
	gameControlTypes.RegisterTypes()
	return client, nil
}

func (client *Client) SetSpawn(position mapping.Position, rotation math32.Vector3) (bool, error) {
	request := gameControlTypes.SetSpawn{Position: position, Rotation: rotation}
	reply, err := client.timedQuery("SET_SPAWN", DEFAULT_TIMEOUT, request)
	if err != nil {
		return false, err
	}
	success, assertSuccess := reply.(bool)
	if !assertSuccess {
		return false, errors.New("setSpawn failed to assert reply success")
	}
	return success, nil
}

func (client *Client) GetEntityDefinition(id uuid.UUID) (*gameCore.EntityDefinition, error) {
	reply, err := client.timedQuery("GET_ENTITY_DEFINITION", DEFAULT_TIMEOUT, id)
	if err != nil {
		return nil, fmt.Errorf("Failed to query the server: %v", err)
	}
	definition, success := reply.(gameCore.EntityDefinition)
	if !success {
		return nil, errors.New("Failed to assert entity definition")
	}
	return &definition, nil
}

func (client *Client) GetItemDefinition(id uuid.UUID) (*gameCore.ItemDefinition, error) {
	if client.cStream.Conn == nil {
		return nil, errors.New("Connection to server unavailable")
	}
	reply, err := client.timedQuery("GET_ITEM_DEFINITION", DEFAULT_TIMEOUT, id)
	if err != nil {
		return nil, fmt.Errorf("Failed to query the server: %v", err)
	}
	definition, success := reply.(gameCore.ItemDefinition)
	if !success {
		return nil, errors.New("Failed to assert item definition")
	}
	return &definition, nil
}

// Authenticate handles login from raw username, password strings
func (client *Client) Authenticate(username, password string) (authReply gameControlTypes.AuthReply, err error) {
	if client.cStream.Conn == nil {
		return authReply, errors.New("Connection to server unavailable")
	}
	reply, err := client.timedQuery("AUTHENTICATE", LOGIN_TIMEOUT, gameControlTypes.AuthRequest{Username: username, Password: password})
	if err != nil {
		err = fmt.Errorf("Failed to query the server: %v", err)
		return
	}
	authReply, success := reply.(gameControlTypes.AuthReply)
	if !success {
		err = errors.New("Failed to assert AuthReply")
		return
	}
	return
}

func (client *Client) Disconnect() error {
	if client.cStream.Conn == nil {
		return errors.New("Connection already disconnected")
	}

	_ = client.queryControlServer(nil, "DISCONNECT")
	client.cStream.Conn.Close()
	return nil
}

func (client *Client) Register(username, password string) (bool, string) {
	//	fmt.Println("Registering: " + username + " + " + password)
	if client.cStream.Conn == nil {
		return false, "Connection to server unavailable"
	}
	reply, err := client.timedQuery("REGISTER", REGISTER_TIMEOUT, gameControlTypes.Registration{Username: username, Password: password})
	if err != nil {
		//log(3, "Failed registration query:", err)
		return false, fmt.Sprintf("Failed to query server: %v", err)
	}
	asserted, success := reply.(gameControlTypes.RegistrationReply)
	if !success {
		return false, "Failed type assert RegistrationReply type"
	}
	if asserted.Reason != "" {
		return false, asserted.Reason
	}
	return asserted.Success, asserted.Reason
}

func (client *Client) CheckUpdate(version int) bool {
	if client.cStream.Conn == nil {
		return true
	}
	timeout := time.Second * time.Duration(1)
	replyChan := client.queryControlServer(gameControlTypes.VersionMessage{Version: version}, "CHECK_UPDATE")
	select {
	case <-time.After(timeout):
		//TODO: This should handle errors
		//log(5, "Timed out checking update")
		return true
	case replyInterface := <-replyChan:
		reply, success := replyInterface.(gameControlTypes.VersionMessage)
		if !success {
			//TODO: ERROR
			//log(3, "Failed to assert reply type")
		}
		if reply.Version != version {
			//TODO: handle a client update
			//            log(3, "Versions differ")
			return true
		} else {
			return false
			//            log(1, "Version OK")
		}
	}
	return false
}

func (client *Client) FetchAsset(assetID *uuid.UUID) (assPackage assetPackage.IAssetPackage, err error) {
	//	fmt.Println("Fetching asset:", assetID.String(), "WIth timeout:", FETCH_ASSET_TIMEOUT)
	reply, err := client.timedQuery("FETCH_ASSET", FETCH_ASSET_TIMEOUT, gameControlTypes.AssetRequest{Asset: assetID})
	if err != nil {
		err = fmt.Errorf("Failed to fetch asset: %v", err)
		return
	}
	asserted, success := reply.(gameControlTypes.AssetReply)
	if !success {
		err = errors.New("Failed to assert message type")
		return
	}
	if len(asserted.Error) > 0 {
		err = errors.New(asserted.Error)
	}
	assPackage = asserted.Asset
	return
}

/////////////////////////////
// Message processing
/////////////////////////////

//This is for messages that originate on the server side coming to the client
func (client *Client) processControlMessage(message gameControlTypes.ControlMessage) {
	switch message.Type {
	case "REPLY":
		client.handleReply(message)
	case "PING":
		client.handlePing(message)
	case "CHAT_BROADCAST":
		client.handleChatBroadcast(message)
	default:
		//TODO: Not sure how to handle this when its a background job and I dont want the lib logging itself
		//log(3, "Recieved an unknown message type", message.Type)
	}
}

func (client *Client) handleChatBroadcast(message gameControlTypes.ControlMessage) {
	//Assert the payload, then call the chat handler
	asserted, success := message.Message.(gameControlTypes.ChatMessage)
	if !success {
		return
	}
	if client.chatHandler == nil {
		log.Error("recieved chat broadcast but no chat handler has been registered")
	}
	client.chatHandler(asserted.Username, asserted.Message)
}

func (client *Client) handlePing(message gameControlTypes.ControlMessage) {
	//TODO: Implement this
	//    log(3, "handlePing not implemented")
}

func (client *Client) RegisterChatHandler(handler func(string, string)) {
	client.chatHandler = handler
}

func (client *Client) SendChatMessage(message string) error {
	reply, err := client.timedQuery("SEND_CHAT_MESSAGE", DEFAULT_TIMEOUT, gameControlTypes.SendChatMessage{Message: message})
	if err != nil {
		return err
	}
	asserted, success := reply.(gameControlTypes.SendChatMessageReply)
	if !success {
		return errors.New("Failed to assert reply")
	}
	if asserted.Error != "" {
		return errors.New(asserted.Error)
	}
	return nil
}

///////////////////////////////
// Client supporting code
///////////////////////////////

func establishControlStream(address string, port int) (*Client, error) {
	var cStream controlStream
	conn, err := net.DialTimeout("tcp", address+":"+strconv.Itoa(port), time.Second*1)
	if err != nil {
		return &Client{}, fmt.Errorf("Failed to establish ControlStream: %v", err)
	}
	cStream.Conn = conn
	cStream.Decoder = gob.NewDecoder(cStream.Conn)
	cStream.Encoder = gob.NewEncoder(cStream.Conn)
	cQueue := controlQueue{Queue: make(map[uuid.UUID]chan interface{})}
	client := &Client{cStream: cStream, cQueue: cQueue}

	go func() {
		for {
			var message gameControlTypes.ControlMessage
			err := cStream.Decoder.Decode(&message)
			if err == io.EOF {
				fmt.Printf("Hunged the fuck up\n")
				//TODO: Handle a hangup signal back
				return
			} else if err != nil {
				//TODO: Need a way to surface these errors
				//                log(2, "Recieved error:", err)
			}
			go client.processControlMessage(message)
		}
	}()
	return client, nil
}

func (client *Client) queryControlServer(message interface{}, messageType string) (returnChan chan interface{}) {
	messageID, err := uuid.NewV4()
	if err != nil {
		log.Error("Failed to generate UUID for query")
		return
	}
	if client.cStream.Encoder == nil {
		log.Error("Encoder not set")
		return
	}

	controlMessage := gameControlTypes.ControlMessage{ID: *messageID, Type: messageType, Time: time.Now(), Message: message}
	if err := client.cStream.Encoder.Encode(controlMessage); err != nil {
		log.Error("Failed to encode message", err)
		return
	}
	client.cQueue.Lock()
	returnChan = make(chan interface{})
	client.cQueue.Queue[*messageID] = returnChan
	client.cQueue.Unlock()
	return returnChan
}

func (client *Client) timedQuery(requestType string, timer time.Duration, input interface{}) (interface{}, error) {
	replyChan := client.queryControlServer(input, requestType)
	select {
	case <-time.After(timer):
		return nil, errors.New("Timed out")
	case output := <-replyChan:
		return output, nil
	}
	return nil, errors.New("Unknown error")
}

func (client *Client) handleReply(message gameControlTypes.ControlMessage) {
	client.cQueue.RLock()
	if queue, found := client.cQueue.Queue[message.ID]; !found {
		//TODO: Raise error
		//log(3, "Recieved a reply message for a message we don't have")
	} else {
		queue <- message.Message
	}
	client.cQueue.RUnlock()
}
