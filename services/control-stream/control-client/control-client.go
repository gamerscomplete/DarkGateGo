package controlClient

import (
	"errors"
	"fmt"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/services/control-stream/control-types"
	"gitlab.com/gamerscomplete/uuid"
)

const (
	CONTROL_SERVICE_NAME = "control-stream"
	VERSION              = 1
)

type Client struct {
	conn    *csTools.ServerConnObj
	timeout int
}

func NewClient(conn *csTools.ServerConnObj) *Client {
	return &Client{conn: conn, timeout: 10}
}

func (client *Client) SetTimeout(timeout int) {
	client.timeout = timeout
}

func (client *Client) ClientConnected(playerID uuid.UUID) (bool, error) {
	var reply bool
	if err := client.conn.PackSendReplyTimeout(1, "CLIENT_CONNECTED", CONTROL_SERVICE_NAME, client.conn.ID, playerID, &reply); err != nil {
		return false, fmt.Errorf("Request failed: %v", err)
	}
	return reply, nil
}

func (client *Client) BroadcastChat(username string, message string) error {
	controlStreamClients, err := client.conn.GetServiceNodesByName(CONTROL_SERVICE_NAME)
	if err != nil {
		return errors.New("Failed to get services: " + err.Error())
	}

	if len(controlStreamClients) < 1 {
		return errors.New("No control-stream services listening")
	}

	broadcastMessage := controlTypes.ChatBroadcast{Username: username, Message: message}
	for _, value := range controlStreamClients {
		//Swallowing errors because we don't want to stop on a send. Would be good to to handle this a bit more gracefully in the future
		client.conn.PackSendReplyTimeout(1, "CHAT_BROADCAST", CONTROL_SERVICE_NAME, value, broadcastMessage, nil)
	}
	return nil
}
