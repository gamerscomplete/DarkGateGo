package controlTypes

type ChatBroadcast struct {
	Username string
	Message  string
}
