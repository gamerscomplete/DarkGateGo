package gameControlTypes

import (
	"encoding/gob"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/maestro/maestro-types"
	"gitlab.com/gamerscomplete/uuid"
	"time"
)

type SetSpawn struct {
	Position mapping.Position
	Rotation math32.Vector3
}

type AuthRequest struct {
	Username string
	Password string
}

type AuthReply struct {
	Token    string
	Reason   string
	PlayerID uuid.UUID

	//TODO: Move these to post login in seperate calls
	//	Objects    map[uuid.UUID]stateTypes.GameObject
	GameServer maestroTypes.GameServer
}

type Registration struct {
	Username  string
	Password  string
	FirstName string
	LastName  string
	Email     string
	Referrer  string
}

type RegistrationReply struct {
	Success bool
	Reason  string
}

type AssetRequest struct {
	Asset *uuid.UUID
}

type AssetReply struct {
	Asset assetPackage.IAssetPackage
	Error string
}

type QueryMessage struct {
	Command    string
	Parameters string
}

type ControlMessage struct {
	//TODO: Why the fuck does this have an ID
	ID      uuid.UUID
	Type    string
	Time    time.Time
	Message interface{}
}

type VersionMessage struct {
	Version int
}

type SendChatMessage struct {
	Message string
}

type SendChatMessageReply struct {
	Error string
}

type ChatMessage struct {
	Private  bool
	Username string
	Message  string
}

/*
type PrivateMessage struct {
	PlayerID     uuid.UUID
	FromUsername string
	Message      string
	SendTime     time.Time
}
*/
func RegisterTypes() {
	gob.Register(SetSpawn{})
	gob.Register(AuthRequest{})
	gob.Register(AuthReply{})
	gob.Register(AssetRequest{})
	gob.Register(AssetReply{})
	gob.Register(QueryMessage{})
	gob.Register(Registration{})
	gob.Register(RegistrationReply{})
	gob.Register(VersionMessage{})
	gob.Register(SendChatMessage{})
	gob.Register(SendChatMessageReply{})
	gob.Register(ChatMessage{})
	//TODO: get rid of all this bullshit registration and move to the system CommandControl is using
	gob.Register(uuid.UUID{})
	gob.Register(gameCore.ItemDefinition{})
	gob.Register(gameCore.EntityDefinition{})
}

/*
 * Asset types
 *	Shaders
 *	Textures
 *	Mesh
 *	Audio
 * Snapshot
 *  Players
 *   Position
 *   Rotation
 *   Health
 *  Objects
 *   ID
 *   Position
 *   Rotation
 *   Scale?
 *
 */
