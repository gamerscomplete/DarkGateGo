package serverTypes

import (
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/maestro/maestro-types"
)

type InitServer struct {
	Regions []mapping.Region
}

type InitServerReply struct {
	GameServer maestroTypes.GameServer
}
