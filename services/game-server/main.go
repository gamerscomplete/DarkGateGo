package main

import (
	log "github.com/sirupsen/logrus"
)

const (
	PORT = 7789
)

func main() {
	log.SetLevel(log.DebugLevel)
	shutdownChan := make(chan interface{})

	//Start a listener
	gameserver := NewGameserver()
	err := gameserver.StartServer(PORT)
	if err != nil {
		log.Error("Failed to start network: " + err.Error())
		return
	}

	_ = <-shutdownChan
}
