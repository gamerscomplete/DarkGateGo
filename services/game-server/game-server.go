//TODO: Protect the map accesses, these are going to create race conditions

package main

import (
	"encoding/gob"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-manager"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/uuid"
	"reflect"
	"sync"

	"fmt"
	"gitlab.com/g3n/engine/core"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/network"
	//	"gitlab.com/gamerscomplete/DarkGateGo/libs/tick"
	"gitlab.com/gamerscomplete/DarkGateGo/services/asset-store/asset-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/game-server/server-types"
	"gitlab.com/gamerscomplete/DarkGateGo/services/maestro/maestro-types"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	SAVE_GAME_PATH    = "game.save"
	SERVICE_NAME      = "game-server"
	STATS_TIMER       = time.Second * time.Duration(10)
	INTERFACE_NAME    = "eth0"
	ASSET_STORAGE_DIR = "assets"
)

type Client struct {
	id             *uuid.UUID
	username       string
	playerEntity   *gameCore.Entity
	playerEntityID uuid.UUID
}

type Gameserver struct {
	clients    map[*network.Client]*Client
	clientLock sync.RWMutex

	NetworkRunning    bool
	network           *network.ServerNetwork
	stateEngineClient *stateClient.Client
	assetStoreClient  *assetClient.Client
	authClient        *authenticationClient.Client

	assetManager *assetManager.AssetManager
	core         *gameCore.GameCore

	initialized bool

	clusterConn *csTools.ServerConnObj

	root *core.Node
}

func NewGameserver() *Gameserver {
	var gameserver Gameserver
	if fileExists(SAVE_GAME_PATH) {
		log.Info("save game file exists. loading")
		gameserver = loadGame()
	}
	gameserver.clients = make(map[*network.Client]*Client)
	gameserver.installSignalHandler()

	gameserver.clusterConn = csTools.NewServerConn()
	gameserver.stateEngineClient = stateClient.NewClient(gameserver.clusterConn)
	gameserver.assetStoreClient = assetClient.NewClient(gameserver.clusterConn)
	gameserver.authClient = authenticationClient.NewClient(gameserver.clusterConn)
	gameserver.clusterConn.RegisterHandler("INIT_SERVER", gameserver.initialize)
	gameserver.clusterConn.RegisterHandler("ACTION_NOTIFICATION", gameserver.processActionNotification)
	gameserver.assetManager = assetManager.NewAssetManager(gameserver.assetStoreClient.FetchAsset, ASSET_STORAGE_DIR)
	gameserver.core = gameCore.NewGameCore(gameserver.assetManager, gameserver.stateEngineClient)

	gameserver.root = core.NewNode()

	//Moving this down to wait to trigger ready status until the clients and entity manager are started up to try to defeat race condition. Maybe not needed
	if err := gameserver.clusterConn.Init(SERVICE_NAME); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return nil
	}
	log.Info("Game server service started and waiting for initialization...")
	return &gameserver
}

func (gameserver *Gameserver) processActionNotification(message csTools.ServerMessage) {
	var request []syncActions.Action
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn("failed to unpack actionNotification message: ", err)
		return
	}
	gameserver.core.ApplyActions(request)
	log.WithFields(log.Fields{
		"count": len(request),
	}).Debug("broadcasting actions to clients")
	gameserver.network.Broadcast(network.ApplyActions{Actions: request}, nil)
}

func (gameserver *Gameserver) initialize(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}

	var initRequest serverTypes.InitServer
	if gameserver.initialized {
		log.Warn("Recieved request to start server after already initialized")
		replyMessage.Error = "Server already initialized"
		gameserver.clusterConn.SendServerMessageReply(message, replyMessage)
		return
	} else if err := message.Data.UnpackType(&initRequest); err != nil {
		log.Warn("Failed to unpack server initialization message: ", err)
		replyMessage.Error = "Failed to unpack request"
		gameserver.clusterConn.SendServerMessageReply(message, replyMessage)
		return
	}

	addr, err := gameserver.initializeServer(initRequest)
	if err != nil {
		log.Error("Server initialization failed:", err)
		replyMessage.Error = err.Error()
		gameserver.clusterConn.SendServerMessageReply(message, replyMessage)
		return
	}

	reply := serverTypes.InitServerReply{GameServer: maestroTypes.GameServer{Addr: addr, Port: PORT}}
	if err := replyMessage.PackType(&reply); err != nil {
		log.Error("Failed to pack reply to server initialization", err)
		replyMessage.Error = "Failed to pack reply"
		gameserver.clusterConn.SendServerMessageReply(message, replyMessage)
		return
	}

	gameserver.clusterConn.SendServerMessageReply(message, replyMessage)
}

//TODO: make this not a scab into the other initialize call
func (gameserver *Gameserver) initializeServer(initRequest serverTypes.InitServer) (string, error) {
	log.Info("Gameserver initializing")
	if len(initRequest.Regions) == 0 {
		//TODO: Reply with error of no regions specified
		log.Debug("Init request had zero regions")
		return "", errors.New("region list empty")
	}

	//Fetch area from state engine
	var zoneList []mapping.Zone
	for _, value := range initRequest.Regions {
		zoneList = append(zoneList, value.Zones()...)
	}

	if len(zoneList) == 0 {
		log.Warn("Failed to build zone list from non-empty region list")
		return "", errors.New("failed to build zonelist")
	}

	//Start game server
	log.Debug("Starting game server")
	if err := gameserver.startGameServer(zoneList); err != nil {
		return "", err
	}

	//Reply with success
	addr, err := getIP(INTERFACE_NAME)
	if err != nil {
		return "", fmt.Errorf("Failed to get IP: %v", err)
	}

	gameserver.initialized = true
	log.Info("Server startup successful")
	return addr, nil
}

func (gameserver *Gameserver) startGameServer(zoneList []mapping.Zone) error {
	//TODO: Tie the notification registration and get snapshot into a single call to eliminate possibility of missed actions and/or duplicated actions
	if err := gameserver.stateEngineClient.RegisterNotifyZones(zoneList); err != nil {
		log.Error("Failed to register zone notification:", err)
		return err
	}

	log.WithFields(log.Fields{
		"zoneCount": len(zoneList),
	}).Debug("sending get snapshot request")
	snapshot, err := gameserver.stateEngineClient.GetSnapshotByZones(zoneList)
	if err != nil {
		log.Error("Failed to fetch snapshot:", err)
		return fmt.Errorf("Failed to fetch snapshot: %v", err)
	}
	log.WithFields(log.Fields{
		"count": len(snapshot.Actions),
	}).Info("applying snapshot")

	gameserver.core.ApplyActions(snapshot.Actions)
	return nil
}

func (gameserver *Gameserver) StartServer(port int) error {
	var err error
	gameserver.network, err = network.StartServer(port, gameserver.messageProcessor)
	if err != nil {
		return err
	}
	return nil
}

func (gameserver *Gameserver) shutdown() {
	gameserver.saveGame()
	gameserver.core.Shutdown()
}

func (gameserver *Gameserver) saveGame() {
	file, err := os.Create(SAVE_GAME_PATH)
	if err != nil {
		log.Error("Failed to create file:", err)
	}
	defer file.Close()
	encoder := gob.NewEncoder(file)
	if err = encoder.Encode(gameserver); err != nil {
		log.Error("Failed to encode:", err)
	}
}

func loadGame() (gameserver Gameserver) {
	file, err := os.Open(SAVE_GAME_PATH)
	if err != nil {
		log.Error("Failed to open file")
	}
	defer file.Close()
	decoder := gob.NewDecoder(file)
	if err = decoder.Decode(&gameserver); err != nil {
		log.Error("Failed to decode:", err)
	}
	return gameserver
}

func (gameserver *Gameserver) messageProcessor(message network.TransitMessage, client *network.Client) {
	log.WithFields(log.Fields{
		"type": reflect.TypeOf(message.Message),
	}).Debug("recieved message from client")
	switch m := message.Message.(type) {
	case *network.AuthMessage:
		gameserver.Authenticate(m, client, message.ID)
	case *network.CreateGrid:
		gameserver.CreateGrid(m)
	case *network.PlaceOnGrid:
		gameserver.PlaceOnGrid(m)
	case *network.RemoveBlock:
		gameserver.RemoveBlock(m)
	case *network.DeadConn:
		gameserver.DeadConn(client)
	case *network.SyncPosition:
		gameserver.SyncPosition(m, client)
	default:
		log.WithFields(log.Fields{
			"type": reflect.TypeOf(m),
		}).Error("received unknown message type from client")
		return
	}
}

func (gameserver *Gameserver) DeadConn(client *network.Client) {
	log.Info("removing dead client")
	gameserver.clientLock.Lock()
	delete(gameserver.clients, client)
	gameserver.clientLock.Unlock()
}

func (gameserver *Gameserver) GetClient(client *network.Client) *Client {
	gameserver.clientLock.RLock()
	defer gameserver.clientLock.RUnlock()
	if _, have := gameserver.clients[client]; have {
		return gameserver.clients[client]
	}
	return nil

}

func (gameserver *Gameserver) SyncPosition(message *network.SyncPosition, networkClient *network.Client) {
	//get players entity
	client := gameserver.GetClient(networkClient)
	if client == nil {
		log.Error("SyncPosition could not find client connected")
		return
	}

	if client.playerEntity == nil {
		client.playerEntity = gameserver.core.GetEntity(client.playerEntityID)
		if client.playerEntity == nil {
			log.WithFields(log.Fields{
				"clientID": client.playerEntityID.String(),
			}).Error("unable to find player entity to sync")
			return
		}
	}

	//create action
	action := syncActions.UpdateEntity{ID: client.playerEntity.ID(), Position: message.Position, Rotation: message.Rotation}

	//apply action to core
	gameserver.core.ApplyActions([]syncActions.Action{&action})

	//send action to players
	gameserver.network.Broadcast(network.ApplyActions{Actions: []syncActions.Action{&action}}, networkClient)

	//send action to state engine
	err := gameserver.stateEngineClient.ApplyAction(&action)
	if err != nil {
		log.Error("failed to send sync position to state engine: ", err.Error())
		return
	}
}

func (gameserver *Gameserver) CreateGrid(message *network.CreateGrid) {
	_, err := gameserver.stateEngineClient.CreateGrid(message.Position, message.Rotation, message.BlockID)
	if err != nil {
		log.Error("failed to create grid in state-engine: ", err.Error())
		return
	}
}

func (gameserver *Gameserver) PlaceOnGrid(message *network.PlaceOnGrid) {
	err := gameserver.stateEngineClient.PlaceOnGrid(message.GridID, message.Position, message.BlockID)
	if err != nil {
		log.WithFields(log.Fields{
			"gridID":   message.GridID.String(),
			"blockID":  message.BlockID.String(),
			"position": message.Position,
		}).Error(err)
	}
}

func (gameserver *Gameserver) RemoveBlock(message *network.RemoveBlock) {
	err := gameserver.stateEngineClient.RemoveBlock(message.GridID, message.Position)
	if err != nil {
		log.Error("failed to remove block from grid")
	}
}

func (gameserver *Gameserver) Authenticate(message *network.AuthMessage, client *network.Client, tMessageID uint32) {

	//Get user from token
	//TODO: Make the authclient not return so many parameters
	playerID, playerEntityID, playerUsername, err := gameserver.authClient.PlayerFromToken(message.Token, client.Conn().RemoteAddr().String())
	if err != nil {
		log.Warn("Failed to validate authtoken:", err)
		return
	}

	//Get players inventory
	inventory, err := gameserver.stateEngineClient.GetInventory(*playerID)
	if err != nil {
		log.Error("Failed to get player inventory: ", err)
		return
	}

	//TODO: Check if player is already connected

	//Using 0 as the since tick because we want a full snapshot
	snapshot := gameserver.core.CreateSnapshot(0)

	gameserver.clientLock.Lock()
	gameserver.clients[client] = &Client{username: playerUsername, id: playerID, playerEntityID: *playerEntityID}
	gameserver.clientLock.Unlock()
	if err := client.ReplyByID(tMessageID, &network.AuthReply{Success: true, PlayerEntity: *playerEntityID, Snapshot: snapshot, Inventory: inventory}); err != nil {
		log.Error("Failed to send auth reply:", err)
	}
}

func (gameserver *Gameserver) installSignalHandler() {
	// Handle common process-killing signals so we can gracefully shut down:
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		os.Interrupt,
		os.Kill,
		syscall.SIGTERM,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGQUIT,
	)
	go func(c chan os.Signal) {
		// Wait for a SIGINT or SIGKILL:
		//        sig := <-c
		_ = <-c
		gameserver.shutdown()
		os.Exit(0)
	}(sigc)
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func getIP(networkInterface string) (string, error) {
	return "gameserver.dark-gate.com", nil
}
