package serverClient

import (
	"errors"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/services/game-server/server-types"
	"gitlab.com/gamerscomplete/DarkGateGo/services/maestro/maestro-types"
	"gitlab.com/gamerscomplete/uuid"
	"time"
)

const (
	GAME_SERVER_SERVICE_NAME = "game-server"
	DEFAULT_TIMEOUT          = 90
)

type Client struct {
	conn    *csTools.ServerConnObj
	timeout time.Duration
}

func NewClient(conn *csTools.ServerConnObj) Client {
	return Client{conn: conn, timeout: time.Second * time.Duration(DEFAULT_TIMEOUT)}
}

func (client *Client) SetTimeout(timeout int) {
	client.timeout = time.Second * time.Duration(timeout)
}

func (client *Client) InitServer(serverID *uuid.UUID, regions []mapping.Region) (maestroTypes.GameServer, error) {
	initServerReply := serverTypes.InitServerReply{}

	if err := client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "INIT_SERVER", GAME_SERVER_SERVICE_NAME, client.conn.ID, serverTypes.InitServer{Regions: regions}, &initServerReply); err != nil {
		return maestroTypes.GameServer{}, err
	}
	return initServerReply.GameServer, nil
}

func (client *Client) GetClientsConnected() ([]uuid.UUID, error) {
	return nil, errors.New("GetClientsConnected not implemented")
}

func (client *Client) GetAvgFps() (float32, error) {
	return 0, errors.New("GetAvgFps not implemented")
}

func (client *Client) DisconnectClient(ID uuid.UUID) error {
	return errors.New("DisconnectClient not implemented")
}

func (client *Client) GetLoadedEntityCount() (int, error) {
	return 0, errors.New("GetLoadedEntityCount not implemented")
}

func (client *Client) GetLoadedEntities() ([]uuid.UUID, error) {
	return nil, errors.New("GetLoadedEntities not implemented")
}

func (client *Client) GetRegionsManaged() ([]mapping.Region, error) {
	return nil, errors.New("GetRegionsManaged not implemented")
}

func (client *Client) ShutdownServer(serverID *uuid.UUID) error {
	return errors.New("ShutdownServer not implemented")
}
