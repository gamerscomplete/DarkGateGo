package main

import (
	"fmt"
	//	"html/template"
	//	"io"
	"net/http"
	"os"
	"time"
	//	"strconv"
	"github.com/tylerb/graceful"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	//    "gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/web-interface/application"
)

const (
	LOG_LEVEL    = 0
	SERVICE_NAME = "web-interface"
	WEB_PORT     = 8080

	TIMEOUT = 1

	STATIC_ROOT string = "static/"
	STATIC_URL  string = "/static/"
)

var (
	serverConn = csTools.NewServerConn()
	localID    = csTools.GetID()

//    stateEngineClient stateClient.Client
)

func main() {
	//    stateEngineClient = stateClient.NewClient(&serverConn)
	go startWebServer()
	if err := serverConn.Init(SERVICE_NAME); err != nil {
		log(5, "Failed to initialize server conn with error:", err)
		return
	}
	csTools.Wait()
	shutdown()
}

func startWebServer() {
	serverAddress := ":8080"

	app, err := application.New(serverConn)
	if err != nil {
		log(5, err)
		return
	}

	middle, err := app.MiddlewareStruct()
	if err != nil {
		log(5, err)
		return
	}

	srv := &graceful.Server{
		Timeout: time.Second * TIMEOUT,
		Server:  &http.Server{Addr: serverAddress, Handler: middle},
	}

	log(1, "Running HTTP server on "+serverAddress)

	err = srv.ListenAndServe()
}

func log(level int, message ...interface{}) {
	if level >= LOG_LEVEL {
		fmt.Print("(", SERVICE_NAME, ")", message, "\n")
	}
}

func shutdown() {
	log(1, "Shutdown called")
	os.Exit(0)
}

/////////////////////////
/// Display functions ///
/////////////////////////

/*
func entity(w http.ResponseWriter, req *http.Request) {
	entityCount, err := stateEngineClient.GetEntityCount()
	if err != nil {
		log(3, "Failed to fetch entity count:", err)
		return
	}

	context := Context{Title: strconv.Itoa(entityCount)}
	render(w, "entity", context)
}
*/
