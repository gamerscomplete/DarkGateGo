package handlers

import (
	"fmt"
	"html/template"
	"net/http"
)

func GetGrid(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	tmpl, err := template.ParseFiles("templates/base.html.tmpl", "templates/grid.html")
	if err != nil {
		fmt.Println("Kaboom:", err)
		//TODO: some logging
		return
	}

	tmpl.Execute(w, nil)
}
