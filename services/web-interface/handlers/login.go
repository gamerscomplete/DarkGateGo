package handlers

import (
	"fmt"
	"github.com/gorilla/sessions"
	authClient "gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-client"
	"html/template"
	"net/http"
	"strings"
)

func HandleLogin(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	fmt.Println("Form dump:", req.Form)
	username, found := req.Form["username"]
	if !found {
		fmt.Println("Login form didnt contain username")
		ServerError(w, req)
		return
	}
	password, found := req.Form["password"]
	if !found {
		fmt.Println("Login form didnt contain password")
		ServerError(w, req)
		return
	}

	//TODO: DO login...
	client := req.Context().Value("authClient").(*authClient.Client)
	authReply, err := client.AuthenticateByPassword(strings.Join(username, ""), strings.Join(password, ""), req.RemoteAddr)
	if err != nil {
		ServerError(w, req)
	}

	if authReply.Reason != "" {
		fmt.Println("Login failed:", authReply.Reason)
		//Need to print reason out to screen
		return
	}

	sessionStore := req.Context().Value("sessionStore").(sessions.Store)
	session, _ := sessionStore.Get(req, "dg-session")
	fmt.Println("Login successful from:", req.RemoteAddr)

	session.Values["token"] = authReply.Token
	session.Values["username"] = username
	session.Values["ID"] = authReply.PlayerID.String()
	session.Save(req, w)
	url, found := session.Values["pre-login-url"].(string)
	if found {
		http.Redirect(w, req, url, 302)
	} else {
		http.Redirect(w, req, "/", 302)
	}
}

func DisplayLogin(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	tmpl, err := template.ParseFiles("templates/login.html")
	if err != nil {
		ServerError(w, req)
		return
	}

	tmpl.Execute(w, nil)
}
