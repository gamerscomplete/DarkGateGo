package handlers

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"
	"html/template"
	"net/http"
)

type EntityPage struct {
	EntityCount int
	Action      string
}

func GetEntity(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	action, _ := vars["action"]

	stateClient := r.Context().Value("stateClient").(*stateClient.Client)
	count, err := stateClient.GetEntityCount()
	if err != nil {
		fmt.Println("kerboom")
	}

	w.Header().Set("Content-Type", "text/html")

	tmpl, err := template.ParseFiles("templates/base.html.tmpl", "templates/header.html.tmpl", "templates/entity.html.tmpl")
	if err != nil {
		fmt.Println("Kaboom:", err)
		//TODO: some logging
		return
	}

	entityPage := EntityPage{EntityCount: count, Action: action}

	tmpl.Execute(w, entityPage)
}
