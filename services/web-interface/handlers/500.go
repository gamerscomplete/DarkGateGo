package handlers

import (
	"fmt"
	"html/template"
	"net/http"
)

func ServerError(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	tmpl, err := template.ParseFiles("templates/500.html")
	if err != nil {
		fmt.Println("Servering 500 server:", err)
		return
	}

	tmpl.Execute(w, nil)
}
