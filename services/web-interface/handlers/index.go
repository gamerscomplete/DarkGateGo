package handlers

import (
	"fmt"
	"html/template"
	"net/http"
)

func GetIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	tmpl, err := template.ParseFiles("templates/base.html.tmpl", "templates/header.html.tmpl", "templates/index.html.tmpl")
	if err != nil {
		fmt.Println("Failed to parse index template", err)
		//TODO: some logging
		return
	}

	if err := tmpl.Execute(w, nil); err != nil {
		fmt.Println("Failed to execute template:", err)
	}
}
