package handlers

import (
	"fmt"
	"github.com/gorilla/sessions"
	"net/http"
)

func HandleLogout(w http.ResponseWriter, req *http.Request) {
	sessionStore := req.Context().Value("sessionStore").(sessions.Store)
	session, _ := sessionStore.Get(req, "dg-session")
	fmt.Println("Logging out user:", session.Values["username"])

	session.Values["token"] = nil
	session.Values["username"] = nil
	session.Values["ID"] = nil
	session.Values["pre-login-url"] = nil
	session.Save(req, w)

	http.Redirect(w, req, "/login", 302)
}
