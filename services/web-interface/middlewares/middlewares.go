package middlewares

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"context"
	"github.com/gorilla/sessions"

	authClient "gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"
)

func SetSessionStore(sessionStore sessions.Store) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			req = req.WithContext(context.WithValue(req.Context(), "sessionStore", sessionStore))

			next.ServeHTTP(res, req)
		})
	}
}

func SetAuthClient(client *authClient.Client) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			req = req.WithContext(context.WithValue(req.Context(), "authClient", client))

			next.ServeHTTP(res, req)
		})
	}
}

func SetStateClient(client *stateClient.Client) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			req = req.WithContext(context.WithValue(req.Context(), "stateClient", client))

			next.ServeHTTP(res, req)
		})
	}
}

func PageSecurity() func(http.Handler) http.Handler {
	return MustLogin
}

// MustLogin is a middleware that checks existence of current user.
func MustLogin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		sessionStore := req.Context().Value("sessionStore").(sessions.Store)
		session, _ := sessionStore.Get(req, "dg-session")
		userRowInterface := session.Values["username"]

		if userRowInterface == nil &&
			req.URL.Path != "/login" && !strings.HasPrefix(req.URL.Path, "/static") &&
			req.URL.Path != "/favicon.ico" {

			session.Values["pre-login-url"] = req.URL.Path
			session.Save(req, res)
			http.Redirect(res, req, "/login", 302)
			return
		}

		next.ServeHTTP(res, req)
	})
}

/*
func SetClients(client stateClient.Client) func(http.Handler) http.Handler {
    return func(next http.Handler) http.Handler {
        return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
            req = req.WithContext(context.WithValue(req.Context(), "assetClient", client))
            req = req.WithContext(context.WithValue(req.Context(), "stateClient", client))
            req = req.WithContext(context.WithValue(req.Context(), "authClient", client))
            req = req.WithContext(context.WithValue(req.Context(), "gameClient", client))

            next.ServeHTTP(res, req)
        })
    }
}
*/

func Logger() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			//TODO: Prolly log this shit to a service or somethin
			fmt.Println("Request: ", time.Now(), "Host:", req.Host, "URI:", req.URL, "From: ", req.RemoteAddr)
			next.ServeHTTP(res, req)
		})
	}
}
