package application

import (
	"github.com/carbocation/interpose"
	gorilla_mux "github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"net/http"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	authClient "gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/web-interface/handlers"
	"gitlab.com/gamerscomplete/DarkGateGo/services/web-interface/middlewares"
)

const (
	COOKIE_SECRET = "CU3vtxXowsQU6uOZ"
)

// New is the constructor for Application struct.
func New(serverConn *csTools.ServerConnObj) (*Application, error) {
	app := &Application{}
	app.serverConn = serverConn
	app.stateClient = stateClient.NewClient(app.serverConn)
	app.authClient = authClient.NewClient(app.serverConn)
	app.sessionStore = sessions.NewCookieStore([]byte(COOKIE_SECRET))

	return app, nil
}

// Application is the application object that runs HTTP server.
type Application struct {
	sessionStore sessions.Store
	serverConn   *csTools.ServerConnObj
	stateClient  *stateClient.Client
	authClient   *authClient.Client
}

func (app *Application) MiddlewareStruct() (*interpose.Middleware, error) {
	middle := interpose.New()
	middle.Use(middlewares.SetSessionStore(app.sessionStore))
	middle.Use(middlewares.SetStateClient(app.stateClient))
	middle.Use(middlewares.SetAuthClient(app.authClient))
	middle.Use(middlewares.Logger())
	middle.Use(middlewares.PageSecurity())

	middle.UseHandler(app.mux())

	return middle, nil
}

func (app *Application) mux() *gorilla_mux.Router {
	router := gorilla_mux.NewRouter()

	router.Handle("/", http.HandlerFunc(handlers.GetIndex)).Methods("GET")
	router.Handle("/entity/{action}", http.HandlerFunc(handlers.GetEntity)).Methods("GET")
	router.Handle("/login", http.HandlerFunc(handlers.DisplayLogin)).Methods("GET")
	router.Handle("/login", http.HandlerFunc(handlers.HandleLogin)).Methods("POST")
	router.Handle("/logout", http.HandlerFunc(handlers.HandleLogout))
	router.Handle("/download", http.HandlerFunc(handlers.HandleDownload))

	router.Handle("/grid", http.HandlerFunc(handlers.GetGrid)).Methods("GET")

	router.Handle("/favicon.ico", http.HandlerFunc(handlers.FaviconHandler))

	// Path of static files must be last! Must strip the static off the url so it doesnt get appended in the fileserver path
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	return router
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "relative/path/to/favicon.ico")
}
