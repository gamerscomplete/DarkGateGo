package main

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/geometry"
	"gitlab.com/g3n/engine/material"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/texture"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	//	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/asset-store/asset-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/game-generator/generator-types"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"
	"gitlab.com/gamerscomplete/uuid"
	"os"
)

var (
	localID = csTools.GetID()
)

const (
	LOG_LEVEL = 1
)
const (
	CUBE_HEIGHT = float32(1)
	CUBE_WIDTH  = float32(1)
	CUBE_LENGTH = float32(1)
)

type GameGenerator struct {
	cubeColor              math32.Color
	texture                *texture.Texture2D
	clusterConn            *csTools.ServerConnObj
	assetStoreClient       *assetClient.Client
	stateClient            *stateClient.Client
	cubeEntityDefinitionID uuid.UUID
}

func NewGameGenerator() *GameGenerator {
	return &GameGenerator{cubeColor: math32.Color{1, 1, 1}}
}

func main() {
	log.SetLevel(log.DebugLevel)
	gameGenerator := NewGameGenerator()
	//	syncActions.RegisterTypes()
	gameGenerator.clusterConn = csTools.NewServerConn()
	gameGenerator.clusterConn.RegisterHandler("GENERATE_ZONE", gameGenerator.processGenerateZoneRequest)
	if err := gameGenerator.clusterConn.Init("game-generator"); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return
	}

	gameGenerator.assetStoreClient = assetClient.NewClient(gameGenerator.clusterConn)
	gameGenerator.stateClient = stateClient.NewClient(gameGenerator.clusterConn)

	if err := gameGenerator.initialize(); err != nil {
		log.Fatal("Failed to initialize game generator:", err)
		return
	}

	csTools.Wait()
	shutdown()
}

func (gameGenerator *GameGenerator) initialize() error {
	rgba, err := texture.DecodeImage("assets/dplate.jpg")
	if err != nil {
		return errors.New("Error decoding texture assets/dplate.jpg: " + err.Error())
	}
	gameGenerator.texture = texture.NewTexture2DFromRGBA(rgba)

	//Load texture
	geom := geometry.NewSegmentedBox(CUBE_HEIGHT, CUBE_WIDTH, CUBE_LENGTH, 1, 1, 1)

	mat := material.NewStandard(&gameGenerator.cubeColor)
	mat.AddTexture(gameGenerator.texture)

	geomPackage := assetPackage.PackageGeometry("gen-cube", geom)
	matPackage := assetPackage.PackageMaterial("gen-mat", mat)

	//	matPackage := assetPackage.PackageMaterial("gen-mat", gameGenerator.texture, &gameGenerator.cubeColor)
	if err := gameGenerator.assetStoreClient.StoreAsset(geomPackage); err != nil {
		return err
	}
	gameGenerator.assetStoreClient.StoreAsset(matPackage)

	definition := gameCore.EntityDefinition{
		Name: "game-generator cube",
		Geometries: map[uuid.UUID][]uuid.UUID{
			*geomPackage.ID(): []uuid.UUID{*matPackage.ID()},
		},
		Scale: &math32.Vector3{20, 20, 20},
	}

	definitionID, err := gameGenerator.stateClient.CreateEntityDefinition(definition)
	if err != nil {
		return err
	}
	gameGenerator.cubeEntityDefinitionID = definitionID
	return nil
}

func (gameGenerator *GameGenerator) processGenerateZoneRequest(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}

	request := generatorTypes.GenerateZone{}
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn("Failed to unpack generation request", err)
		replyMessage.Error = err.Error()
		gameGenerator.clusterConn.SendServerMessageReply(message, replyMessage)
		return
	}
	//	log.Debug("Recieved zone generation request for zone:", request.Zone)

	err := gameGenerator.generateZone(request.Zone)
	if err != nil {
		log.Error("Failed to generate zone:", err)
		replyMessage.Error = err.Error()
		gameGenerator.clusterConn.SendServerMessageReply(message, replyMessage)
		return
	}

	//TODO: not just respond with an empty type
	reply := generatorTypes.GenerateZoneReply{}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		gameGenerator.clusterConn.SendServerMessageReply(message, replyMessage)
		return
	}
	//	log.Debug("Sending reply back to zone generation requestor")
	gameGenerator.clusterConn.SendServerMessageReply(message, replyMessage)
}

func (gameGenerator *GameGenerator) generateZone(zone mapping.Zone) error {
	//Find center of zone
	center := zone.FindCenter()
	/*
		//Store the mesh
		meshPackage := assetPackage.PackageGeometry("Sphere", gameGenerator.)
		if err := assetStoreClient.StoreAsset(*meshPackage); err != nil {
			return actions, err
		}
	*/
	/*
		entityID, err := uuid.NewV4()
		if err != nil {
			return nil, err
		}
	*/

	//Create an entity using a predefined definition
	//Add entity in the state engine

	/*
		var componentList []syncActions.IComponent
		geomComponent := syncActions.GeometryComponent{&syncActions.Component{ComponentID: gameGenerator.geomID}}
		materialComponent := syncActions.MaterialComponent{&syncActions.Component{ComponentID: gameGenerator.matID}}
		componentList = append(componentList, geomComponent, materialComponent)
	*/

	//	entityDefinition := syncActions.EntityDefinition{Name: "test cube", Components: componentList}

	_, err := gameGenerator.stateClient.CreateEntity(gameGenerator.cubeEntityDefinitionID, center, math32.Vector3{})
	if err != nil {
		return err
	}

	return nil
}

/*
func log(level int, message ...interface{}) {
	if level >= LOG_LEVEL {
		fmt.Print("(Game-Generator)", message, "\n")
	}
}
*/

func shutdown() {
	log.Info("Shutdown called")
	os.Exit(0)
}
