package generatorClient

import (
	//	"fmt"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	//	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/game-generator/generator-types"
)

const (
	GENERATOR_SERVICE_NAME = "game-generator"
	VERSION                = 1
	DEFAULT_TIMEOUT        = 10
)

type Client struct {
	conn    *csTools.ServerConnObj
	timeout int
}

func NewClient(conn *csTools.ServerConnObj) Client {
	return Client{conn: conn, timeout: DEFAULT_TIMEOUT}
}

func (client *Client) SetTimeout(timeout int) {
	client.timeout = timeout
}

func (client *Client) GenerateZone(zone mapping.Zone) error {
	generateZoneReply := generatorTypes.GenerateZoneReply{}
	if err := client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GENERATE_ZONE", GENERATOR_SERVICE_NAME, client.conn.ID, generatorTypes.GenerateZone{Zone: zone}, &generateZoneReply); err != nil {
		return err
	}

	return nil
}
