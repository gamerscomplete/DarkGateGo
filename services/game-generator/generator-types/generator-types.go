package generatorTypes

import (
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
)

type GenerateZone struct {
	Zone mapping.Zone
}

type GenerateZoneReply struct {
	Actions []syncActions.Action
}
