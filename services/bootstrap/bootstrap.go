package main

import (
	"errors"
	"fmt"
	"github.com/nfnt/resize"
	//	"gitlab.com/g3n/engine/loader/obj"
	"gitlab.com/gamerscomplete/uuid"
	"image"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/geometry"
	"gitlab.com/g3n/engine/material"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/texture"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/asset-store/asset-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-types"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
)

const (
	SERVICE_NAME = "bootstrap"
)

const (
	CUBE_HEIGHT = float32(1)
	CUBE_WIDTH  = float32(1)
	CUBE_LENGTH = float32(1)
)

var (
	serverConn        = csTools.NewServerConn()
	localID           = csTools.GetID()
	_assetStoreClient *assetClient.Client
	_stateClient      *stateClient.Client
	_authClient       *authenticationClient.Client

	_cubeGeomID *uuid.UUID
)

func main() {
	if err := serverConn.Init(SERVICE_NAME); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return
	}

	_assetStoreClient = assetClient.NewClient(serverConn)
	_stateClient = stateClient.NewClient(serverConn)
	_authClient = authenticationClient.NewClient(serverConn)

	if err := doTheStuff(); err != nil {
		log.Fatal("Failed to bootstrap:", err)
	}
	log.Info("Finished loading character model")
	csTools.Wait()
	shutdown()
}

func doTheStuff() error {
	_cubeGeomID, _ = createCubeGeometry()

	inventory, err := createItems()
	if err != nil {
		return errors.New("Failed to create items: " + err.Error())
	}

	if err := createPlayerTemplate(inventory); err != nil {
		return errors.New("Failed to create player template: " + err.Error())
	}

	if success, err := _authClient.SetSpawn(mapping.Position{300, 20, 300}, math32.Vector3{0, 0, 0}); err != nil {
		return errors.New("failed to set spawn: " + err.Error())
	} else if !success {
		return errors.New("setspawn request failed")
	}

	return nil
}

func createPlayerTemplate(inventory map[uuid.UUID]int) error {
	geoms, packages, err := assetPackage.PackageObj("assets/LegoDarthVader.obj", "assets/LegoDarthVader.mtl")
	if err != nil {
		return err
	}

	for _, value := range packages {
		if err := _assetStoreClient.StoreAsset(value); err != nil {
			log.Fatal("failed to store asset package: ", err)
			return err
		}
	}

	playerEntityDefinitionID, err := _stateClient.CreateEntityDefinition(gameCore.EntityDefinition{
		Name:       "player entity",
		Geometries: geoms,
		Tags:       []string{"player"},
	})
	if err != nil {
		log.Error("Failed to create template")
		return err
	}

	//Set the player entity template
	return _authClient.SetPlayerTemplate(authenticationTypes.PlayerTemplate{
		EntityDefinitionID: playerEntityDefinitionID,
		InventoryItems:     inventory,
	})
}

func createItems() (map[uuid.UUID]int, error) {
	inventoryMap := make(map[uuid.UUID]int)

	yellowDplateID, err := createItem("yellow d-plate", "Yellow diamond plate building block", "assets/yellow_dplate.jpg")
	if err != nil {
		fmt.Println("Failed to create yellow dplate")
		return inventoryMap, err
	}
	inventoryMap[*yellowDplateID] = 1

	redDplateID, err := createItem("red d-plate", "red diamond plate building block", "assets/red_dplate.jpg")
	if err != nil {
		fmt.Println("Failed to create red dplate")
		return inventoryMap, err
	}
	inventoryMap[*redDplateID] = 1
	blueDplateID, err := createItem("blue d-plate", "blue diamond plate building block", "assets/blue_dplate.jpg")
	if err != nil {
		fmt.Println("Failed to create blue dplate")
		return inventoryMap, err
	}
	inventoryMap[*blueDplateID] = 1
	blackDplateID, err := createItem("black d-plate", "black diamond plate building block", "assets/black_dplate.jpg")
	if err != nil {
		fmt.Println("Failed to create black dplate")
		return inventoryMap, err
	}
	inventoryMap[*blackDplateID] = 1

	concreteID, err := createItem("concrete", "Concrete building block", "assets/concrete.jpg")
	if err != nil {
		fmt.Println("Failed to create concrete")
		return inventoryMap, err
	}
	inventoryMap[*concreteID] = 1

	crateID, err := createItem("crate", "Wooden crate", "assets/crate.jpg")
	if err != nil {
		fmt.Println("Failed to create crate")
		return inventoryMap, err
	}
	inventoryMap[*crateID] = 1

	grateID, err := createItem("grate", "Steel grate building block", "assets/grate.jpg")
	if err != nil {
		fmt.Println("Failed to create grate")
		return inventoryMap, err
	}
	inventoryMap[*grateID] = 1

	redBricksID, err := createItem("red bricks", "Red bricks", "assets/red_bricks.jpg")
	if err != nil {
		fmt.Println("Failed to create red bricks")
		return inventoryMap, err
	}
	inventoryMap[*redBricksID] = 1

	tanBricksID, err := createItem("tan bricks", "Tan bricks", "assets/tan_bricks.jpg")
	if err != nil {
		fmt.Println("Failed to create tan bricks")
		return inventoryMap, err
	}
	inventoryMap[*tanBricksID] = 1
	mosaicBricksID, err := createItem("mosaic bricks", "mosaic bricks", "assets/mosaic_bricks.jpg")
	if err != nil {
		fmt.Println("Failed to create mosaic bricks")
		return inventoryMap, err
	}
	inventoryMap[*mosaicBricksID] = 1
	whiteBricksID, err := createItem("white bricks", "white bricks", "assets/white_bricks.jpg")
	if err != nil {
		fmt.Println("Failed to create white bricks")
		return inventoryMap, err
	}
	inventoryMap[*whiteBricksID] = 1

	lightCamoID, err := createItem("light camo", "Light camo", "assets/camo_light.jpg")
	if err != nil {
		fmt.Println("Failed to create light camo")
		return inventoryMap, err
	}
	inventoryMap[*lightCamoID] = 1

	darkCamoID, err := createItem("dark camo", "Dark camo", "assets/camo_dark.jpg")
	if err != nil {
		fmt.Println("Failed to create dark camo")
		return inventoryMap, err
	}
	inventoryMap[*darkCamoID] = 1

	return inventoryMap, nil
}

func createItem(name string, description string, path string) (*uuid.UUID, error) {
	//Create the icon for the item
	texPackageID, err := createIconTexturePackage(name, path)
	if err != nil {
		return nil, errors.New("Failed to create icon texture package: " + err.Error())
	}

	//Create the entity template
	entityDefinitionID, err := createCubeDefinitionFromTexture(name, path)
	if err != nil {
		return nil, errors.New("Failed to crate cube definition from texture: " + err.Error())
	}

	//Create d-plate block
	itemDef := gameCore.ItemDefinition{
		Name:               name,
		Description:        description,
		IconTexPackage:     *texPackageID,
		EntityDefinitionID: *entityDefinitionID,
	}
	itemDefinitionID, err := _stateClient.CreateItem(itemDef)
	if err != nil {
		return nil, errors.New("Failed to create items at the state client: " + err.Error())
	}
	return itemDefinitionID, nil
}

func createIconTexturePackage(name string, path string) (*uuid.UUID, error) {
	rgba, err := texture.DecodeImage(path)
	if err != nil {
		return nil, errors.New("Error decoding texture: " + err.Error())
	}

	icon := resize.Resize(64, 64, rgba, resize.Lanczos3)
	iconRGBA, ok := icon.(*image.RGBA)
	if !ok {
		return nil, fmt.Errorf("Unable to cast image to rgba: %T", icon)
	}
	iconTex := texture.NewTexture2DFromRGBA(iconRGBA)

	//Package the icon
	texPackage := assetPackage.PackageTexture(name, iconTex)

	//Store the icon package
	if err := _assetStoreClient.StoreAsset(texPackage); err != nil {
		return nil, errors.New("failed to store asset " + err.Error())
	}
	return texPackage.ID(), nil
}

func createCubeGeometry() (*uuid.UUID, error) {
	//Load texture
	geom := geometry.NewSegmentedBox(CUBE_HEIGHT, CUBE_WIDTH, CUBE_LENGTH, 1, 1, 1)
	geomPackage := assetPackage.PackageGeometry("gen-cube", geom)
	if err := _assetStoreClient.StoreAsset(geomPackage); err != nil {
		return nil, err
	}

	return geomPackage.ID(), nil
}

func createCubeDefinitionFromTexture(name string, path string) (*uuid.UUID, error) {
	rgba, err := texture.DecodeImage(path)
	if err != nil {
		return nil, errors.New("Error decoding texture: " + err.Error())
	}
	tex := texture.NewTexture2DFromRGBA(rgba)
	cubeColor := math32.Color{1, 1, 1}

	mat := material.NewStandard(&cubeColor)
	mat.AddTexture(tex)

	matPackage := assetPackage.PackageMaterial("gen-mat", mat)
	if err := _assetStoreClient.StoreAsset(matPackage); err != nil {
		log.Fatal("Failed to store material package:", err)
		return nil, err
	}

	//	entityDefinition := gameCore.EntityDefinition{Name: name, Geometry: *_cubeGeomID, Material: *matPackage.ID()}
	entityDefinition := gameCore.EntityDefinition{
		Name: name,
		Geometries: map[uuid.UUID][]uuid.UUID{
			*_cubeGeomID: []uuid.UUID{*matPackage.ID()},
		},
	}

	log.Info("Creating template ", name)
	cubeDefinitionID, err := _stateClient.CreateEntityDefinition(entityDefinition)
	if err != nil {
		log.Error("Failed to create template")
		return nil, err
	}
	return &cubeDefinitionID, nil
}

func shutdown() {
	log.Info("Shutdown called")
	os.Exit(0)
}
