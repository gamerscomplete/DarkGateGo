package chatClient

import (
	//	"errors"
	"fmt"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/services/chat/chat-types"
	"gitlab.com/gamerscomplete/uuid"
)

const (
	CHAT_SERVICE_NAME = "chat"
	VERSION           = 1
)

type Client struct {
	conn    *csTools.ServerConnObj
	timeout int
}

func NewClient(conn *csTools.ServerConnObj) *Client {
	return &Client{conn: conn, timeout: 10}
}

func (client *Client) SetTimeout(timeout int) {
	client.timeout = timeout
}

func (client *Client) SendChatMessage(playerID uuid.UUID, message string) error {
	if err := client.conn.PackSendReplyTimeout(1, "SEND_MESSAGE", CHAT_SERVICE_NAME, client.conn.ID, chatTypes.SendMessage{PlayerID: playerID, Message: message}, nil); err != nil {
		return fmt.Errorf("Failed to send chat message: %v", err)
	}
	return nil
}

func (client *Client) AddListener(id uuid.UUID) error {
	if err := client.conn.PackSendReplyTimeout(1, "ADD_LISTENER", CHAT_SERVICE_NAME, client.conn.ID, chatTypes.Listener{PlayerID: id}, nil); err != nil {
		return err
	}
	return nil
}

func (client *Client) RemoveListener(id uuid.UUID) error {
	if err := client.conn.PackSendReplyTimeout(1, "REMOVE_LISTENER", CHAT_SERVICE_NAME, client.conn.ID, chatTypes.Listener{PlayerID: id}, nil); err != nil {
		return err
	}
	return nil
}
