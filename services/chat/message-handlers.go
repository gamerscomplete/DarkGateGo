package main

import (
	log "github.com/sirupsen/logrus"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/services/chat/chat-types"
	"time"
)

func processRemoveListener(message csTools.ServerMessage) {
	removeListenerMessage := chatTypes.Listener{}
	if err := message.Data.UnpackType(&removeListenerMessage); err != nil {
		log.Error("Failed to unpack listener removal request")
		_serverConn.ReplyError(message, "Failed to unpack request")
		return
	}
	_chatDatabase.RemoveListener(removeListenerMessage.PlayerID)
	_serverConn.SendServerMessageReply(message, csTools.Message{})
}

func processAddListener(message csTools.ServerMessage) {
	registerClient := chatTypes.Listener{}
	if err := message.Data.UnpackType(&registerClient); err != nil {
		log.Error("Failed to unpack listener add request")
		_serverConn.ReplyError(message, "Failed to unpack request")
		return
	}
	_chatDatabase.AddListener(registerClient.PlayerID, message.FromID)
	_serverConn.SendServerMessageReply(message, csTools.Message{})
}

func processSendMessage(message csTools.ServerMessage) {
	log.Debug("Recieved chat message")
	chatMessageRequest := chatTypes.SendMessage{}
	if err := message.Data.UnpackType(&chatMessageRequest); err != nil {
		log.Error("Failed to unpack chat message")
		_serverConn.ReplyError(message, "Failed to unpack request")
		return
	}

	if err := _chatDatabase.AddChatMessage(chatMessage{author: chatMessageRequest.PlayerID, ts: time.Now(), message: chatMessageRequest.Message}); err != nil {
		_serverConn.ReplyError(message, err.Error())
		return
	}
	log.Debug("Sending chat reply")
	_serverConn.SendServerMessageReply(message, csTools.Message{})
}
