package chatTypes

import (
	"gitlab.com/gamerscomplete/uuid"
)

type SendMessage struct {
	PlayerID uuid.UUID
	Message  string
}

type SendMessageReply struct {
	Error string
}

type Listener struct {
	PlayerID uuid.UUID
}
