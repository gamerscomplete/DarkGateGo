package main

import (
	log "github.com/sirupsen/logrus"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/control-stream/control-client"
	"gitlab.com/gamerscomplete/uuid"
	"os"
	"sync"
	"time"
)

const (
	SERVICE_NAME                = "chat"
	CONTROL_STREAM_SERVICE_NAME = "control-stream"
)

var (
	_serverConn           = csTools.NewServerConn()
	_chatDatabase         = newChatDatabase()
	_authenticationClient *authenticationClient.Client
	_controlClient        *controlClient.Client
)

type ChatDatabase struct {
	//[control stream id][]playerID
	listeningClients map[uuid.UUID][]uuid.UUID
	clientLock       sync.RWMutex
	messages         []chatMessage
}

type chatMessage struct {
	author  uuid.UUID
	ts      time.Time
	message string
}

func main() {
	_serverConn.RegisterHandler("SEND_MESSAGE", processSendMessage)
	_serverConn.RegisterHandler("ADD_LISTENER", processAddListener)
	_serverConn.RegisterHandler("REMOVE_LISTENER", processRemoveListener)

	if err := _serverConn.Init(SERVICE_NAME); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return
	}

	_authenticationClient = authenticationClient.NewClient(_serverConn)
	_controlClient = controlClient.NewClient(_serverConn)

	csTools.Wait()
	shutdown()
}

func newChatDatabase() ChatDatabase {
	return ChatDatabase{listeningClients: make(map[uuid.UUID][]uuid.UUID)}
}

func usernameFromID(ID uuid.UUID) (string, error) {
	return _authenticationClient.GetUsernameFromID(&ID)
}

func (database *ChatDatabase) AddListener(playerID uuid.UUID, controlStreamID uuid.UUID) {
	log.Info("Adding listener:", controlStreamID.String())
	database.listeningClients[controlStreamID] = append(database.listeningClients[controlStreamID], playerID)
}

func (database *ChatDatabase) RemoveListener(playerID uuid.UUID) {
	for controlID, controlList := range database.listeningClients {
		for key, value := range controlList {
			if value == playerID {
				if len(database.listeningClients[controlID]) < 2 {
					database.listeningClients[controlID] = nil
				} else {
					//					database.listeningClients[controlID] = append(database.listeningClients[controlID][:key], database.listeningClients[controlID][key+1:]...)
					database.listeningClients[controlID][key] = database.listeningClients[controlID][len(database.listeningClients[controlID])-1]
					database.listeningClients[controlID] = database.listeningClients[controlID][:len(database.listeningClients[controlID])-1]
				}
			}
		}
	}
}

func (database *ChatDatabase) AddChatMessage(message chatMessage) error {
	log.Debug("Adding chat message")
	//Store
	database.messages = append(database.messages, message)
	if err := database.BroadcastMessage(message); err != nil {
		log.Error("Message broadcast error:", err)
	}
	return nil
}

func (database *ChatDatabase) BroadcastMessage(message chatMessage) error {
	log.Debug("Broadcasting message")
	username, err := usernameFromID(message.author)
	if err != nil {
		log.Error("Failed to get username from ID:", err)
		return err
	}

	if err := _controlClient.BroadcastChat(username, message.message); err != nil {
		return err
	}

	return nil
}

func shutdown() {
	log.Info("Shutdown called")
	os.Exit(0)
}
