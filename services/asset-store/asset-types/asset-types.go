package assetClientTypes

import (
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	"gitlab.com/gamerscomplete/uuid"
)

//TODO: Just send the ID and skip the wrapper type
type FetchAsset struct {
	ID *uuid.UUID
}

//TODO: Drop the success boolean and depend on the outter message error type
type FetchAssetReply struct {
	Asset   assetPackage.IAssetPackage
	Success bool
}

type StoreAsset struct {
	Asset assetPackage.IAssetPackage
}

//TODO: just return a bool or find a way around returning success
type StoreAssetReply struct {
	Success bool
}
