package main

import (
	//	"bytes"
	"encoding/gob"
	"errors"
	//	"fmt"
	"os"
	"reflect"
	"sync"

	log "github.com/sirupsen/logrus"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	"gitlab.com/gamerscomplete/DarkGateGo/services/asset-store/asset-types"
	"gitlab.com/gamerscomplete/uuid"
)

const (
	DATABASE_FILE = "modules/asset-store/assets.db"
	STORAGE_DIR   = "assets/"
	LOG_LEVEL     = 1
)

var (
	_serverConn = csTools.NewServerConn()
	_database   Database
)

type Database struct {
	//	Assets map[uuid.UUID]bytes.Buffer
	assetLock sync.RWMutex
}

type WrappedAsset struct {
	Value   string
	Package assetPackage.IAssetPackage
}

func main() {
	_serverConn.RegisterHandler("STORE_ASSET", processStoreAsset)
	_serverConn.RegisterHandler("FETCH_ASSET", processFetchAsset)

	if err := _serverConn.Init("asset-store"); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return
	}

	log.Info("Startup successful")
	csTools.Wait()
	shutdown()
}

func processFetchAsset(message csTools.ServerMessage) {
	//Unpack message
	request := assetClientTypes.FetchAsset{}
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn("Failed to unpack request to fetch asset", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	//Fetch the actual asset
	assPackage, err := fetchAsset(request.ID)
	if err != nil {
		log.Error("Asset fetch failed:", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	//Prepare response
	reply := assetClientTypes.FetchAssetReply{Success: true, Asset: assPackage}
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	//Return the asset
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processStoreAsset(message csTools.ServerMessage) {
	log.Debug("Processing store asset request")

	//Pick the AssetPackage out of the message
	//Check if we already have this assetPackage
	//Store asset package
	//Reply back to the client

	request := assetClientTypes.StoreAsset{}
	if err := message.Data.UnpackType(&request); err != nil {
		log.Error("Failed to unpack request to store asset", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	//	log.Info("Storing asset", request.Asset.Name, "with size:", request.Asset.Size(), "ID:", request.ID.String())

	//Store the asset
	if err := _database.storeAsset(request.Asset); err != nil {
		log.Error("Failed to store asset: ", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	//Prepare response
	reply := assetClientTypes.StoreAssetReply{Success: true}
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	//reply success
	log.WithFields(log.Fields{
		"assetID": request.Asset.ID().String(),
	}).Info("successfully stored asset")
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) storeAsset(asset assetPackage.IAssetPackage) error {
	if asset == nil {
		return errors.New("Cannot encode nil asset")
	}
	if asset.ID() == nil {
		return errors.New("cannot store package with nil ID")
	}

	log.WithFields(log.Fields{
		"assetType": reflect.TypeOf(asset).String(),
	}).Debug("storing asset")
	path := STORAGE_DIR
	mode := os.ModePerm
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, mode)
	}
	assetFile, err := os.Create(path + asset.ID().String() + ".asset")
	if err != nil {
		return err
	}
	assetEncoder := gob.NewEncoder(assetFile)
	wrapper := WrappedAsset{Value: "test", Package: asset}
	if err := assetEncoder.Encode(wrapper); err != nil {
		return err
	}
	assetFile.Close()

	return nil
}

func fetchAsset(id *uuid.UUID) (assetPackage.IAssetPackage, error) {
	log.WithFields(log.Fields{
		"path": STORAGE_DIR + id.String() + ".asset",
	}).Info("fetching asset")
	_, err := os.Stat(STORAGE_DIR + id.String() + ".asset")
	if os.IsNotExist(err) {
		return nil, errors.New("Asset does not exist")
	}
	if err != nil {
		log.Error("failed to stat file: ", err)
	}

	//	var assPackage assetPackage.IAssetPackage
	assetFile, err := os.Open(STORAGE_DIR + id.String() + ".asset")
	if err != nil {
		log.Warn("Failed to open asset file", err)
		return nil, err
	}
	defer assetFile.Close()

	assetDecoder := gob.NewDecoder(assetFile)
	var wrapper WrappedAsset

	err = assetDecoder.Decode(&wrapper)
	if err != nil {
		log.Error("Failed to decode AssetPackage into type", err)
		return nil, err
	}

	if wrapper.Package == nil {
		return nil, errors.New("Decoded into nil asset")
	}

	return wrapper.Package, nil
}

/*
//TODO: This needs to be moved into the assetpackage package
func compressAsset(data []byte) (err error, buffer bytes.Buffer) {
	writer := gzip.NewWriter(&buffer)
	writer.Name = "Compressed Asset"
	writer.Comment = "Assssssss"
	writer.ModTime = time.Now()

	size, err := writer.Write(data)
	if err != nil {
		log.Error("Failed to compress asset", err)
		return
	}
	log.Info("Stored asset of size:", size)

	if err = writer.Close(); err != nil {
		log.Error("Failed to close compressed asset", err)
		return
	}
	return
}
*/

func shutdown() {
	log.Info("Shutdown called")
	os.Exit(0)
}
