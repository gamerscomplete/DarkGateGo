package assetClient

import (
	"errors"
	"time"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	"gitlab.com/gamerscomplete/DarkGateGo/services/asset-store/asset-types"
	"gitlab.com/gamerscomplete/uuid"
)

const (
	TIMEOUT                  = time.Second * time.Duration(60)
	STORE_ASSET_TIMEOUT      = 5
	ASSET_STORE_SERVICE_NAME = "asset-store"
)

type Client struct {
	conn *csTools.ServerConnObj
}

func NewClient(conn *csTools.ServerConnObj) *Client {
	return &Client{conn: conn}
}

func (client *Client) StoreAsset(asset assetPackage.IAssetPackage) error {
	if asset == nil {
		return errors.New("will not attempt to store nil asset")
	}
	storeAssetRequest := assetClientTypes.StoreAsset{Asset: asset}
	reply := assetClientTypes.StoreAssetReply{}
	if err := client.conn.PackSendReplyTimeout(5, "STORE_ASSET", ASSET_STORE_SERVICE_NAME, client.conn.ID, storeAssetRequest, &reply); err != nil {
		return err
	}

	if !reply.Success {
		return errors.New("Failed to store asset in asset-store")
	}

	return nil
}

func (client *Client) FetchAsset(id *uuid.UUID) (assetPackage.IAssetPackage, error) {
	//make request to asset-store
	assetRequestMessage := csTools.Message{Command: "FETCH_ASSET"}
	if err := assetRequestMessage.PackType(assetClientTypes.FetchAsset{ID: id}); err != nil {
		return nil, err
	}
	//TODO: Replace conn.ID with a node from asset-store
	assetRequestReply, err := client.conn.TimedQuery(TIMEOUT, assetRequestMessage, client.conn.ID, ASSET_STORE_SERVICE_NAME)
	if err != nil {
		return nil, err
	}
	assetReply := assetClientTypes.FetchAssetReply{}
	if err = assetRequestReply.UnpackType(&assetReply); err != nil {
		return nil, err
	}
	if !assetReply.Success {
		return nil, errors.New("Failed to fetch asset from asset-store")
	}
	if assetReply.Asset == nil {
		return nil, errors.New("Fetched invalid asset")
	}
	return assetReply.Asset, nil
}

func (client *Client) GetDatabaseSize() (int, error) {
	return 0, errors.New("GetDatabaseSize not implemeneted")
}

func (client *Client) GetAssetCount() (int, error) {
	return 0, errors.New("Get asset count not implemented")
}

func (client *Client) GetDiskUsage() (int, error) {
	return 0, errors.New("GetDiskusage not implemeneted")
}

func (client *Client) GetAssetStatistics() (map[string]string, error) {
	return nil, errors.New("GetAssetStatistics not implemented")
}
