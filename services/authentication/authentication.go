package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"gitlab.com/g3n/engine/math32"
	//	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"time"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-types"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-client"
	//	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-types"
	"gitlab.com/gamerscomplete/uuid"
)

var (
	_serverConn       = csTools.NewServerConn()
	localID           = csTools.GetID()
	stateEngineClient *stateClient.Client
)

const (
	STATE_ENGINE_SERVICE_NAME = "state-engine"
	LOG_LEVEL                 = 0
	MAX_USERNAME_SIZE         = 255
)

type Database struct {
	Players        map[string]*authenticationTypes.Player
	playerTemplate *authenticationTypes.PlayerTemplate
	spawnPosition  mapping.Position
	spawnRotation  math32.Vector3
}

func main() {
	database := &Database{
		Players: make(map[string]*authenticationTypes.Player),
	}
	database.registerHandlers()

	if err := _serverConn.Init("authentication"); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return
	}

	stateEngineClient = stateClient.NewClient(_serverConn)
	log.Info("Authentication service started")
	csTools.Wait()
	shutdown()
}

func (database *Database) SetSpawn(position mapping.Position, rotation math32.Vector3) {
	database.spawnPosition = position
	database.spawnRotation = rotation
}

func (database *Database) SetPlayerTemplate(template authenticationTypes.PlayerTemplate) {
	//	log(2, "Setting template:", id.St)
	database.playerTemplate = &template
}

func logout(logoutRequest authenticationTypes.Logout) error {
	//TODO: Remove the authentication token and change the player status
	return errors.New("logout not implemented")
}

//TODO: Validate against IP as well
func (database *Database) validateToken(token string, ip string) (bool, error) {
	for _, value := range database.Players {
		if value.Token == token {
			return true, nil
		}
	}
	return false, nil
}

//TODO: Validate against IP as well
func (database *Database) playerFromToken(token string, ip string) (string, *uuid.UUID, *uuid.UUID, error) {
	for _, value := range database.Players {
		if value.Token == token {
			return value.Username, value.ID, value.EntityID, nil
		}
	}
	return "", nil, nil, errors.New("Token invalid")
}

func (database *Database) authenticateByPassword(username, password string) (authenticationTypes.AuthenticationReply, error) {
	var authReply authenticationTypes.AuthenticationReply
	if database.checkAuthentication(username, password) {
		authToken := generateToken()
		authReply.Token = authToken
		authReply.PlayerID = database.Players[username].ID
		authReply.EntityID = database.Players[username].EntityID
		updatePlayer := database.Players[username]
		updatePlayer.LastLoginTime = time.Now()
		updatePlayer.Token = authToken
		database.Players[username] = updatePlayer
		log.Info("Auth successful for ", username, "Token:", authToken)
	} else {
		authReply.Reason = "Invalid credentials"
		log.Info("Auth failure for ", username)
	}
	return authReply, nil
}

func (database *Database) registerUser(registrationRequest authenticationTypes.Registration) (string, error) {
	if _, found := database.Players[registrationRequest.Username]; found {
		return "Username already taken", nil
	}

	if len(registrationRequest.Username) > MAX_USERNAME_SIZE {
		return "Username too long", nil
	}

	//Generate a ID for the player
	playerID, err := uuid.NewV4()
	if err != nil {
		return "", fmt.Errorf("Failed to create UUID: %v", err)
	}

	if database.playerTemplate == nil {
		return "", errors.New("Template not set")
	}

	entityID, err := stateEngineClient.CreateEntity(database.playerTemplate.EntityDefinitionID, database.spawnPosition, database.spawnRotation)
	if err != nil {
		return "", fmt.Errorf("Failed to create entity in the state-engine: %v", err)
	}

	//Use the entity ID from before for the inventory ID to keep them all linked
	if err := stateEngineClient.CreateInventoryWithID(playerID, database.playerTemplate.InventoryItems); err != nil {
		return "", fmt.Errorf("Failed to create inventory in the state-engine: %v", err)
	}

	newPlayer := authenticationTypes.Player{ID: playerID, Username: registrationRequest.Username, Password: registrationRequest.Password, EntityID: entityID}
	//TODO: swap this out with id instead of username
	database.Players[registrationRequest.Username] = &newPlayer
	log.WithFields(log.Fields{}).Info("Registration successful")

	return "", nil
}

/////////////////////////////
// Tools
/////////////////////////////
func (database *Database) getPlayerByID(ID uuid.UUID) *authenticationTypes.Player {
	if !database.playerExists(ID) {
		return nil
	}
	for _, value := range database.Players {
		if *value.ID == ID {
			return value
		}
	}
	return nil
}

func (database *Database) playerExists(ID uuid.UUID) bool {
	for _, value := range database.Players {
		if *value.ID == ID {
			return true
		}
	}
	return false
}

//TODO: Rename to something more specific
func (database *Database) checkAuthentication(username, password string) bool {
	player, success := database.getPlayer(username)
	if !success {
		log.Debug("Username does not exist: ", username)
		return false
	}
	if player.Password != password {
		log.Debug("Incorrect password for", username)
		return false
	}
	return true
}

func generateToken() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		log.Error("Failed to generate token", err)
	}
	return fmt.Sprintf("%x", b)
}

func (database *Database) getPlayer(username string) (player *authenticationTypes.Player, found bool) {
	if player, found = database.Players[username]; found {
		return
	}
	return
}

/////////////////////////////
// Utility
/////////////////////////////

func shutdown() {
	log.Info("Shutdown initiated")
}
