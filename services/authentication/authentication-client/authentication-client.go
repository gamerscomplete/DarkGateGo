package authenticationClient

import (
	"errors"
	"fmt"
	"gitlab.com/g3n/engine/math32"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-types"
	"gitlab.com/gamerscomplete/uuid"
	"time"
)

const (
	AUTH_SERVICE_NAME    = "authentication"
	REGISTRATION_TIMEOUT = 2
	LOGIN_TIMEOUT        = time.Second * time.Duration(2)
)

type Client struct {
	conn *csTools.ServerConnObj
}

func NewClient(conn *csTools.ServerConnObj) *Client {
	return &Client{conn: conn}
}

func (client *Client) validateConn() bool {
	if client.conn != nil {
		return true
	}
	return false
}

func (client *Client) SetSpawn(position mapping.Position, rotation math32.Vector3) (bool, error) {
	var reply bool
	if err := client.conn.PackSendReplyTimeout(1, "SET_SPAWN", AUTH_SERVICE_NAME, client.conn.ID, authenticationTypes.SetSpawn{Position: position, Rotation: rotation}, &reply); err != nil {
		return false, err
	}
	if !reply {
		return false, errors.New("unknown failure setting spawn")
	}
	return true, nil
}

func (client *Client) SetPlayerTemplate(template authenticationTypes.PlayerTemplate) error {
	reply := authenticationTypes.SetTemplateReply{}
	if err := client.conn.PackSendReplyTimeout(1, "SET_PLAYER_TEMPLATE", AUTH_SERVICE_NAME, client.conn.ID, template, &reply); err != nil {
		return err
	}
	if !reply.Success {
		return errors.New("Failed to set template")
	}
	return nil
}

func (client *Client) GetUsernameFromID(ID *uuid.UUID) (string, error) {
	if !client.validateConn() {
		return "", errors.New("Invalid connection")
	}

	usernameReply := authenticationTypes.GetUsernameReply{}
	if err := client.conn.PackSendReplyTimeout(2, "USERNAME_FROM_ID", AUTH_SERVICE_NAME, client.conn.ID, authenticationTypes.GetUsername{ID: ID}, &usernameReply); err != nil {
		return "", fmt.Errorf("Username request failed %v", err)
	}

	return usernameReply.Username, nil

}
func (client *Client) RegisterUser(username string, password string) (string, error) {
	registrationRequest := authenticationTypes.Registration{Username: username, Password: password}

	registrationReply := authenticationTypes.RegistrationReply{}
	if err := client.conn.PackSendReplyTimeout(REGISTRATION_TIMEOUT, "REGISTER", AUTH_SERVICE_NAME, client.conn.ID, registrationRequest, &registrationReply); err != nil {
		return "Server error", err
	}

	return registrationReply.Reason, nil
}

func (client *Client) AuthenticateByPassword(username string, password string, IP string) (authenticationTypes.AuthenticationReply, error) {
	authRequest := authenticationTypes.AuthenticateByPassword{Username: username, Password: password, IP: IP}

	var authReply authenticationTypes.AuthenticationReply
	if err := client.conn.PackSendReplyTimeout(2, "AUTHENTICATE_BY_PASSWORD", AUTH_SERVICE_NAME, client.conn.ID, authRequest, &authReply); err != nil {
		return authenticationTypes.AuthenticationReply{}, fmt.Errorf("Authentication failed: %v", err)
	}

	return authReply, nil
}

func (client *Client) ValidateToken(token string, IP string) (bool, error) {
	validationReply := authenticationTypes.ValidateTokenReply{}
	if err := client.conn.PackSendReplyTimeout(2, "VALIDATE_TOKEN", AUTH_SERVICE_NAME, client.conn.ID, authenticationTypes.ValidateToken{Token: token, IP: IP}, &validationReply); err != nil {
		return false, fmt.Errorf("Authentication failed: %v", err)
	}
	return validationReply.Valid, nil
}

//Returns playerID, players entity id, players username, if the request failed
func (client *Client) PlayerFromToken(token string, IP string) (*uuid.UUID, *uuid.UUID, string, error) {
	request := authenticationTypes.GetPlayerFromToken{Token: token, IP: IP}
	var reply authenticationTypes.GetPlayerFromTokenReply
	if err := client.conn.PackSendReplyTimeout(2, "PLAYER_FROM_TOKEN", AUTH_SERVICE_NAME, client.conn.ID, request, &reply); err != nil {
		return reply.ID, reply.EntityID, reply.Username, fmt.Errorf("Authentication failed: %v", err)
	}
	if reply.EntityID == nil {
		return nil, nil, "", errors.New("received nil entityID")
	}
	return reply.ID, reply.EntityID, reply.Username, nil
}

func (client *Client) Logout(playerID *uuid.UUID, IP string) error {
	logoutRequest := authenticationTypes.Logout{ID: playerID, IP: IP}
	var logoutReply authenticationTypes.LogoutReply

	if err := client.conn.PackSendReplyTimeout(2, "LOGOUT", AUTH_SERVICE_NAME, client.conn.ID, logoutRequest, &logoutReply); err != nil {
		return fmt.Errorf("Get logged in players request failed: %v", err)
	}

	return nil
}

func (client *Client) GetPlayersByReqion(region mapping.Region) ([]uuid.UUID, error) {
	return nil, errors.New("Get players by region not implemeneted")
}

func (client *Client) GetPlayersByZone(zone mapping.Zone) ([]uuid.UUID, error) {
	return nil, errors.New("Get players by zone not implemented")
}

func (client *Client) GetUserCount() (int, error) {
	return 0, errors.New("GetUserCount not implemented")
	//	getUserCountRequest := authenticationTypes.GetUserCount{}
}

func (client *Client) GetLoggedInPlayers() ([]uuid.UUID, error) {
	getLoggedInPlayersRequest := authenticationTypes.GetLoggedInPlayers{}
	var getLoggedInPlayersReply authenticationTypes.GetLoggedInPlayersReply
	if err := client.conn.PackSendReplyTimeout(2, "GET_LOGGED_IN_PLAYERS", AUTH_SERVICE_NAME, client.conn.ID, getLoggedInPlayersRequest, &getLoggedInPlayersReply); err != nil {
		return nil, fmt.Errorf("Get logged in players request failed: %v", err)
	}

	return getLoggedInPlayersReply.Players, nil
}

//Takes number of hours to check against
//Should maybe take a "since" time instead of hour offset, but this is good enough for nao
func (client *Client) RegisteredUserCountSince(timestamp time.Time) (uint32, error) {
	getRegisteredUserCountRequest := authenticationTypes.GetRegisteredUserCountRequest{SinceTimestamp: timestamp}

	var getRegisteredUserCountReply authenticationTypes.GetRegisteredUserCountReply
	if err := client.conn.PackSendReplyTimeout(2, "GET_REGISTERED_USER_COUNT", AUTH_SERVICE_NAME, client.conn.ID, getRegisteredUserCountRequest, &getRegisteredUserCountReply); err != nil {
		return 0, fmt.Errorf("Get registered user count request failed: %v", err)
	}
	return getRegisteredUserCountReply.Count, nil
}

func (client *Client) InvalidateToken(token string) error {
	invalidateTokenRequest := authenticationTypes.InvalidateTokenRequest{Token: token}

	var invalidateTokenReply authenticationTypes.InvalidateTokenReply
	if err := client.conn.PackSendReplyTimeout(2, "INVALIDATE_TOKEN", AUTH_SERVICE_NAME, client.conn.ID, invalidateTokenRequest, &invalidateTokenReply); err != nil {
		return fmt.Errorf("Token invalidation request failed: %v", err)
	}
	return nil
}
