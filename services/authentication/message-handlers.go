package main

import (
	log "github.com/sirupsen/logrus"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/services/authentication/authentication-types"
	//	"gitlab.com/gamerscomplete/uuid"
)

func (database *Database) registerHandlers() {
	_serverConn.RegisterHandler("REGISTER", database.processRegistration)
	_serverConn.RegisterHandler("AUTHENTICATE_BY_PASSWORD", database.processAuthenticateByPassword)
	_serverConn.RegisterHandler("PLAYER_FROM_TOKEN", database.processPlayerFromToken)
	_serverConn.RegisterHandler("LOGOUT", database.processLogout)
	_serverConn.RegisterHandler("USERNAME_FROM_ID", database.processUsernameFromID)
	_serverConn.RegisterHandler("SET_PLAYER_TEMPLATE", database.processSetPlayerTemplate)
	_serverConn.RegisterHandler("SET_SPAWN", database.processSetSpawn)
}

func (database *Database) processSetSpawn(message csTools.ServerMessage) {
	log.Info("received set spawn request")
	var setSpawn authenticationTypes.SetSpawn
	if err := message.Data.UnpackType(&setSpawn); err != nil {
		log.Warn("failed to unpackage setSpawn request")
		_serverConn.ReplyError(message, err.Error())
		return
	}

	database.SetSpawn(setSpawn.Position, setSpawn.Rotation)

	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(true); err != nil {
		log.Error("failed to pack reply", err)
		_serverConn.ReplyError(message, err.Error())
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processSetPlayerTemplate(message csTools.ServerMessage) {
	log.Info("Recieved set player template message")
	var template authenticationTypes.PlayerTemplate
	if err := message.Data.UnpackType(&template); err != nil {
		log.Warn("Failed to unpack set template request")
		_serverConn.ReplyError(message, err.Error())
		return
	}
	database.SetPlayerTemplate(template)
	reply := authenticationTypes.SetTemplateReply{Success: true}
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		_serverConn.ReplyError(message, err.Error())
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processRegistration(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	registrationReply := authenticationTypes.RegistrationReply{}

	registrationRequest := authenticationTypes.Registration{}
	if err := message.Data.UnpackType(&registrationRequest); err != nil {
		log.Info("Failed to unpack registration request", err)
		replyMessage.Error = "Failed to unpack registration request"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	var err error
	registrationReply.Reason, err = database.registerUser(registrationRequest)
	if err != nil {
		log.Warn("User registration failed:", err)
		registrationReply.Reason = "Server error"
	}

	if err := replyMessage.PackType(registrationReply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply: " + err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	log.WithFields(log.Fields{
		"Username": registrationRequest.Username,
		"email":    registrationRequest.Email,
		"IP":       registrationRequest.IP,
	}).Info("registration processed")
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processLogout(message csTools.ServerMessage) {
	log.Error("logout called but not implemented")
	_serverConn.ReplyError(message, "logout not implemented in authentication service")
	//Blackholing this as I dont give a fuck about it right now and its making errors
	/*    var logoutReply authenticationTypes.LogoutReply
	    logoutRequest := authenticationTypes.Logout{}

	    if err := message.Data.UnpackType(logoutRequest); err != nil {
	        log.Error("Failed to unpack logout request", err)
			logoutReply.Error = "Failed to unpack message"
		    _serverConn.SendServerMessageReply(message, replyMessage)
	        return
	    }
		if err := logout(logoutRequest); err != nil {
			logoutReply.Error= "Logout failed: " + err.Error()
		    _serverConn.SendServerMessageReply(message, replyMessage)
		}

	    replyMessage := csTools.Message{Command: "REPLY"}
	    if err := replyMessage.PackType(logoutReply); err != nil {
	        log.Error("Failed to pack reply", err)
	        return
	    }
	    _serverConn.SendServerMessageReply(message, replyMessage)
	*/
}

func (database *Database) processAuthenticateByPassword(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}

	authRequest := authenticationTypes.AuthenticateByPassword{}
	if err := message.Data.UnpackType(&authRequest); err != nil {
		log.Error("Failed to unpack login", err)
		replyMessage.Error = "Failed to unpack login. " + err.Error()
		return
	}
	authReply, err := database.authenticateByPassword(authRequest.Username, authRequest.Password)
	if err != nil {
		replyMessage.Error = err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	if err := replyMessage.PackType(authReply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply. " + err.Error()
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)

}

func (database *Database) processValidateToken(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}

	token := authenticationTypes.ValidateToken{}
	if err := message.Data.UnpackType(&token); err != nil {
		log.Error("Failed to unpack token validation", err)
		replyMessage.Error = "Failed to unpack token validation message. " + err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	validated, err := database.validateToken(token.Token, token.IP)
	if err != nil {
		replyMessage.Error = err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
	}

	reply := authenticationTypes.ValidateTokenReply{Valid: validated}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
}

func (database *Database) processPlayerFromToken(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}

	request := authenticationTypes.GetPlayerFromToken{}

	if err := message.Data.UnpackType(&request); err != nil {
		log.Error("Failed to unpack request", err)
		replyMessage.Error = "Failed to unpack token validation message. " + err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	username, ID, entityID, err := database.playerFromToken(request.Token, request.IP)
	if err != nil {
		log.Warn("Failed to get player from token")
		replyMessage.Error = err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
	}

	reply := authenticationTypes.GetPlayerFromTokenReply{Username: username, ID: ID, EntityID: entityID}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	log.WithFields(log.Fields{
		"PlayerID": ID.String(),
	}).Info("successfully got player from token")
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processUsernameFromID(message csTools.ServerMessage) {
	request := authenticationTypes.GetUsername{}
	if err := message.Data.UnpackType(&request); err != nil {
		log.Error("Failed to unpack username from id request", err)
		return
	}

	//TODO: Handle this better. just trying to get basic flow laid out right now
	if !database.playerExists(*request.ID) {
		log.Warn("Request for non existant player")
		return
	}

	player := database.getPlayerByID(*request.ID)

	if player == nil {
		log.Error("Failed to get player")
		return
	}

	usernameReply := authenticationTypes.GetUsernameReply{Username: player.Username}

	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(usernameReply); err != nil {
		log.Error("Failed to pack reply", err)
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}
