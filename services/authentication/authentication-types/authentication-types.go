package authenticationTypes

import (
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/security"
	"gitlab.com/gamerscomplete/uuid"
	"time"
)

type GetUsername struct {
	ID *uuid.UUID
}

type GetUsernameReply struct {
	Username string
}

type Registration struct {
	Username  string
	Password  string
	FirstName string
	LastName  string
	Email     string
	Referrer  string
	IP        string
}

type RegistrationReply struct {
	Reason string
}

type SetSpawn struct {
	Position mapping.Position
	Rotation math32.Vector3
}

type PlayerTemplate struct {
	EntityDefinitionID uuid.UUID
	//map[item id]count of items
	InventoryItems map[uuid.UUID]int
}

type SetTemplateReply struct {
	Success bool
}

type AuthenticateByPassword struct {
	Username string
	Password string
	IP       string
}

type ValidateToken struct {
	Token string
	IP    string
}

type ValidateTokenReply struct {
	Valid bool
}

type GetPlayerFromToken struct {
	Token string
	IP    string
}

type GetPlayerFromTokenReply struct {
	ID       *uuid.UUID
	EntityID *uuid.UUID
	Username string
}

type AuthenticationReply struct {
	Token       string
	PlayerID    *uuid.UUID
	EntityID    *uuid.UUID
	Permissions security.PermissionMask
	Reason      string
}

type Logout struct {
	ID *uuid.UUID
	IP string
}

type LogoutReply struct {
}

type Player struct {
	ID       *uuid.UUID
	Username string
	Password string
	EntityID *uuid.UUID
	//TODO: This should prolly be removed from player and a token system of some sort built to handle this because we're already giving out multiple tokens easily
	Token            string
	LastLoginTime    time.Time
	LastLoginIP      string
	RegistrationDate time.Time
	Permissions      security.PermissionMask
	Email            string
	Phone            string
	DOB              time.Time
}

/// TESTING
type Token struct {
	ID        *uuid.UUID
	IP        string
	IssueDate time.Time
	//Questionable
	ExpirationDate time.Time
	//Name of service that requested the token. Kind of a shitty field
	RequestingService string
}

type GetLoggedInPlayers struct {
}

type GetLoggedInPlayersReply struct {
	Players []uuid.UUID
}

type GetRegisteredUserCountRequest struct {
	SinceTimestamp time.Time
}

type GetRegisteredUserCountReply struct {
	Count uint32
}

type InvalidateTokenRequest struct {
	Token string
}

type InvalidateTokenReply struct {
}
