//TODO: Side process to monitor notifications end points in case one shuts down and doesnt tell us to unregister
//TOOD: Regarding ^ may be able to set up a monitor on genesis to tell us when a service shuts down

package main

import (
	"encoding/gob"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/math32"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/game-generator/generator-client"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-types"
	"gitlab.com/gamerscomplete/uuid"
	"os"
	"sync"
	"time"
)

var (
	_serverConn      = csTools.NewServerConn()
	_generatorClient = generatorClient.Client{}
	_database        Database
	localID          = csTools.GetID()
)

const (
	DATABASE_FILE = "modules/stateEngine/state-engine.db"
)

type Database struct {
	//Updated everytime something in the db is changed

	//map[region][Zone][GameObject ID]game object
	entities   map[mapping.Region]map[mapping.Zone]map[uuid.UUID]*Entity
	entityLock sync.RWMutex

	entityDefinitions    map[uuid.UUID]*gameCore.EntityDefinition
	entityDefinitionLock sync.RWMutex

	grids    map[uuid.UUID]*Grid
	gridLock sync.RWMutex

	//Notify registrations
	RegionFollowers map[mapping.Region][]followerService
	ZoneFollowers   map[mapping.Zone][]followerService

	//map[inventory ID]map[item ID]item count
	inventories   map[uuid.UUID]map[uuid.UUID]int
	inventoryLock sync.RWMutex

	itemDefinitions    map[uuid.UUID]*gameCore.ItemDefinition
	itemDefinitionLock sync.RWMutex

	Version int64
	sync.RWMutex
}

type Grid struct {
	sync.RWMutex
	position math32.Vector3
	rotation math32.Vector3
	blocks   map[gameCore.GridPos]uuid.UUID
}

type followerService struct {
	ID   uuid.UUID
	name string
}

type Entity struct {
	ID           *uuid.UUID
	definition   uuid.UUID
	position     mapping.Position
	rotation     math32.Vector3
	lastAccessed time.Time
	lastUpdated  time.Time
}

func main() {
	log.SetLevel(log.DebugLevel)
	if err := loadDatabase(); err != nil {
		log.Warn("Failed to load database. Starting new database")
		//Load should be handling this, but doing it now to get shit moving
		_database.entities = make(map[mapping.Region]map[mapping.Zone]map[uuid.UUID]*Entity)
		_database.grids = make(map[uuid.UUID]*Grid)
		_database.RegionFollowers = make(map[mapping.Region][]followerService)
		_database.ZoneFollowers = make(map[mapping.Zone][]followerService)
		_database.entityDefinitions = make(map[uuid.UUID]*gameCore.EntityDefinition)
		_database.itemDefinitions = make(map[uuid.UUID]*gameCore.ItemDefinition)
		_database.inventories = make(map[uuid.UUID]map[uuid.UUID]int)
	}

	_database.RegisterHandlers()

	if err := _serverConn.Init("state-engine"); err != nil {
		log.Fatal("Failed to initialize server conn with error:", err)
		return
	}

	log.Info("Service started")
	_generatorClient = generatorClient.NewClient(_serverConn)

	csTools.Wait()
	shutdown()
}

func (database *Database) RemoveGrid(id uuid.UUID) bool {
	database.gridLock.Lock()
	defer database.gridLock.Unlock()
	grid, found := database.grids[id]
	if !found {
		return false
	}
	action := syncActions.RemoveGrid{GridID: id}
	database.notifyListeners(&action, mapping.Position(grid.position))
	delete(database.grids, id)
	return true
}

func (database *Database) RemoveBlock(gridID uuid.UUID, position [3]int) error {
	grid := database.GetGrid(gridID)
	if grid == nil {
		return errors.New("grid not found")
	}
	grid.Lock()
	delete(grid.blocks, gameCore.GridPos{X: position[0], Y: position[1], Z: position[2]})
	grid.Unlock()
	if len(grid.blocks) < 1 {
		log.WithFields(log.Fields{
			"gridID": gridID,
		}).Info("removing last block from grid, removing grid")
		database.RemoveGrid(gridID)
		return nil
	}
	action := syncActions.RemoveBlock{GridID: gridID, Pos: position}
	database.notifyListeners(&action, mapping.Position(grid.position))
	return nil
}

func (database *Database) CreateGrid(position math32.Vector3, rotation math32.Vector3, blockID uuid.UUID) (*uuid.UUID, error) {
	//add to database
	gridID, err := uuid.NewV4()
	if err != nil {
		return nil, errors.New("failed to create uuid: " + err.Error())
	}

	newGrid := Grid{position: position, rotation: rotation, blocks: map[gameCore.GridPos]uuid.UUID{gameCore.GridPos{X: 0, Y: 0, Z: 0}: blockID}}
	database.gridLock.Lock()
	database.grids[*gridID] = &newGrid
	database.gridLock.Unlock()

	gridAction := syncActions.AddGrid{
		GridID:   *gridID,
		Position: position,
		Rotation: rotation,
		Blocks: map[[3]int]uuid.UUID{
			[3]int{0, 0, 0}: blockID,
		},
	}
	database.notifyListeners(&gridAction, mapping.Position(position))
	log.WithFields(log.Fields{
		"gridID":          gridID.String(),
		"startingBlockID": blockID.String(),
		"position":        position,
		"rotation":        rotation,
	}).Debug("grid succesfully created")
	return gridID, nil
}

func (database *Database) PlaceOnGrid(gridID uuid.UUID, position [3]int, blockID uuid.UUID) error {
	grid := database.GetGrid(gridID)
	if grid == nil {
		return errors.New("grid not found")
	}
	grid.Lock()
	defer grid.Unlock()
	if _, found := grid.blocks[gameCore.GridPos{X: position[0], Y: position[1], Z: position[2]}]; found {
		return errors.New("failed to place block. block exists at position already")
	}
	grid.blocks[gameCore.GridPos{X: position[0], Y: position[1], Z: position[2]}] = blockID
	action := syncActions.PlaceOnGrid{GridID: gridID, BlockID: blockID, Position: position}
	database.notifyListeners(&action, mapping.Position(grid.position))
	log.WithFields(log.Fields{
		"gridID":   gridID.String(),
		"position": position,
		"blockID":  blockID.String(),
	}).Debug("succesfully placed block on grid")
	return nil
}

func (database *Database) GetGrid(id uuid.UUID) *Grid {
	database.gridLock.RLock()
	defer database.gridLock.RUnlock()
	if grid, found := database.grids[id]; found {
		return grid
	}
	return nil
}

func (database *Database) CreateItem(id *uuid.UUID, definition gameCore.ItemDefinition) (*uuid.UUID, error) {
	if id == nil {
		var err error
		id, err = uuid.NewV4()
		if err != nil {
			return nil, err
		}
	}
	database.entityDefinitionLock.Lock()
	database.itemDefinitions[*id] = &definition
	database.entityDefinitionLock.Unlock()
	return id, nil
}

func (database *Database) GetItemDefinition(id uuid.UUID) (gameCore.ItemDefinition, error) {
	definition, found := database.itemDefinitions[id]
	var err error
	if !found {
		err = errors.New("Item not found")
	}
	return *definition, err
}

func (database *Database) GetInventory(id uuid.UUID) (map[uuid.UUID]int, error) {
	if inventory, found := database.inventories[id]; found {
		return inventory, nil
	}
	return nil, errors.New("inventory not found")
}

func (database *Database) CreateInventory(request stateTypes.CreateInventory) (uuid.UUID, error) {
	var id *uuid.UUID
	if request.ID == nil {
		var err error
		id, err = uuid.NewV4()
		if err != nil {
			return *id, err
		}
	} else {
		id = request.ID
	}
	database.inventoryLock.Lock()
	database.inventories[*id] = request.Contents
	database.inventoryLock.Unlock()
	return *id, nil
}

func (database *Database) addItemToInventory(inventoryID uuid.UUID, itemID uuid.UUID, quantity int) error {
	database.inventoryLock.Lock()
	defer database.inventoryLock.Unlock()
	if _, found := database.inventories[inventoryID]; !found {
		return errors.New("Inventory not found")
	}
	if _, found := database.inventories[inventoryID][itemID]; found {
		database.inventories[inventoryID][itemID] += quantity
	} else {
		database.inventories[inventoryID][itemID] = quantity
	}

	return nil
}

func (database *Database) getInventoryByID(id uuid.UUID) (map[uuid.UUID]int, error) {
	database.inventoryLock.RLock()
	defer database.inventoryLock.RUnlock()
	inventory, found := database.inventories[id]
	if !found {
		return nil, errors.New("Inventory not found")
	}

	return inventory, nil
}

func (database *Database) CreateEntityDefinition(definition gameCore.EntityDefinition) (*uuid.UUID, error) {
	//Create uuid for definition
	definitionID, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	//store it in the db
	database.entityDefinitionLock.Lock()
	database.entityDefinitions[*definitionID] = &definition
	database.entityDefinitionLock.Unlock()
	log.Info("Entity definition created:", definitionID.String())
	return definitionID, nil
}

func (database *Database) CreateEntity(definitionID uuid.UUID, pos mapping.Position, rot math32.Vector3) (*uuid.UUID, error) {
	//Make sure the definition exists before adding it to the entity
	if definition := database.GetEntityDefinition(definitionID); definition == nil {
		return nil, errors.New("Definition not found")
	}

	entityID, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	newEntity := Entity{definition: definitionID, position: pos, rotation: rot, lastAccessed: time.Now(), lastUpdated: time.Now(), ID: entityID}

	err = addEntity(newEntity)
	if err != nil {
		return nil, err
	}

	addAction := syncActions.AddEntity{EntityID: *entityID, Position: pos, Rotation: rot, DefinitionID: definitionID}
	database.notifyListeners(&addAction, mapping.Position(pos))

	return entityID, nil
}

func (database *Database) GetEntityDefinition(id uuid.UUID) *gameCore.EntityDefinition {
	database.entityDefinitionLock.RLock()
	defer database.entityDefinitionLock.RUnlock()
	if _, found := database.entityDefinitions[id]; found {
		return database.entityDefinitions[id]
	}
	return nil
}

func (database *Database) GetEntityByID(id uuid.UUID) *Entity {
	for region, _ := range _database.entities {
		for zone, _ := range _database.entities[region] {
			for entityID, _ := range _database.entities[region][zone] {
				if entityID == id {
					return _database.entities[region][zone][entityID]
				}
			}
		}
	}
	return nil
}

func cloneEntity(id uuid.UUID) (*uuid.UUID, error) {
	entity := _database.GetEntityByID(id)
	if entity == nil {
		return nil, errors.New("cannot clone, source entity not found")
	}

	newEntityID, err := _database.CreateEntity(entity.definition, entity.position, entity.rotation)
	if err != nil {
		return nil, err
	}
	return newEntityID, nil
}

func registerNotifyZones(zones []mapping.Zone, serviceName string, serviceID uuid.UUID) error {
	follower := followerService{ID: serviceID, name: serviceName}
	for _, zone := range zones {
		_database.ZoneFollowers[zone] = append(_database.ZoneFollowers[zone], follower)
	}
	return nil
}

func registerNotifyRegions(regions []mapping.Region, serviceName string, serviceID uuid.UUID) error {
	follower := followerService{ID: serviceID, name: serviceName}
	for _, region := range regions {
		_database.RegionFollowers[region] = append(_database.RegionFollowers[region], follower)
	}
	return nil
}

func (database *Database) notifyListeners(action syncActions.Action, position mapping.Position) {
	for key, value := range _database.RegionFollowers {
		if key == position.Region() {
			for _, client := range value {
				if err := _serverConn.PackSend("ACTION_NOTIFICATION", client.name, client.ID, []syncActions.Action{action}); err != nil {
					log.Warn("Failed to send message notify to region follower: ", err)
					//TODO: Potentionally remove a client on failure to send to clean up clients that have gone away and not reported an unlisten
				}
			}
		}
	}

	for key, value := range _database.ZoneFollowers {
		if key == position.Zone() {
			for _, client := range value {
				if err := _serverConn.PackSend("ACTION_NOTIFICATION", client.name, client.ID, []syncActions.Action{action}); err != nil {
					log.Warn("Failed to send message notify to zone follower: ", err)
					//TODO: Potentionally remove a client on failure to send to clean up clients that have gone away and not reported an unlisten
				}
			}
		}
	}
}

//////////////////////
/// Internal tools ///
//////////////////////
func getDatabaseSize() int {
	log.Error("Get database size requested. Not implemented")
	return 0
}

func getManagedZones() []mapping.Zone {
	var zones []mapping.Zone
	for _, zoneMap := range _database.entities {
		for key, _ := range zoneMap {
			zones = append(zones, key)
		}
	}
	return zones
}

func applyActions(actions []syncActions.Action) error {
	for _, action := range actions {
		if err := applyAction(action); err != nil {
			return err
		}
	}
	return nil
}

func getEntityByID(ID *uuid.UUID) *Entity {
	for _, zones := range _database.entities {
		for _, ids := range zones {
			for entityID, entity := range ids {
				if entityID == *ID {
					return entity
				}
			}
		}
	}
	return nil
}

func getEntityCount() int {
	count := 0
	for key, _ := range _database.entities {
		count += len(_database.entities[key])
	}
	return count
}

func getSnapshotByZones(zones []mapping.Zone) (stateTypes.Snapshot, error) {
	if len(zones) < 1 {
		return stateTypes.Snapshot{}, errors.New("empty zone request")
	}

	var snapshot stateTypes.Snapshot

	//Get entities
	for _, value := range zones {
		//check if we have the zones stored already
		if !haveZone(value) {
			//Dont have zone Generate it now. Add result to database, and then add to return list
			if err := generateZone(value); err != nil {
				return stateTypes.Snapshot{}, err
			}
		}

		//		for entityID, entity := range _database.entities[value.Region()][value] {
		for _, entity := range _database.entities[value.Region()][value] {
			addAction := syncActions.AddEntity{EntityID: *entity.ID, Position: entity.position, Rotation: entity.rotation, DefinitionID: entity.definition}
			snapshot.Actions = append(snapshot.Actions, &addAction)
		}
	}

	return snapshot, nil
}

func haveZone(zone mapping.Zone) bool {
	if _, found := _database.entities[zone.Region()][zone]; found {
		return true
	}
	return false
}

func uuidInSlice(id uuid.UUID, slice []uuid.UUID) bool {
	for _, value := range slice {
		if id == value {
			return true
		}
	}
	return false
}

func generateZone(zone mapping.Zone) error {
	log.Debug("Generating zone", zone)

	err := _generatorClient.GenerateZone(zone)
	if err != nil {
		return err
	}
	/*
		if err := applyActions(actions); err != nil {
			return err
		}
	*/
	return nil
}

func applyAction(action syncActions.Action) error {
	switch assertedAction := action.(type) {
	case *syncActions.AddEntity:
		return errors.New("applyActions AddEntity not implemented")
		//		return addEntity(assertedAction.Definition, assertedAction.Position, assertedAction.Rotation)
	case *syncActions.UpdateEntity:
		return updateEntity(assertedAction)
	case *syncActions.RemoveEntity:
		return removeEntity(assertedAction)
	case nil:
		return errors.New("Action nil.")
	default:
		//		log.Warn(reflect.TypeOf(assertedAction))
		return errors.New("Unknown type. Cannot apply action")
	}
	return nil
}

func removeEntity(action *syncActions.RemoveEntity) error {
	log.Info("Removing entity:", action.ID.String())
	return errors.New("removeEntity not implemented")
}

//TODO: come up with a better way than looping every identity to find the entity
func updateEntity(action *syncActions.UpdateEntity) error {
	_database.entityLock.Lock()
	defer _database.entityLock.Unlock()
	for region, _ := range _database.entities {
		for zone, _ := range _database.entities[region] {
			for entityID, _ := range _database.entities[region][zone] {
				if entityID == action.ID {
					_database.entities[region][zone][entityID].position = action.Position
					_database.entities[region][zone][entityID].rotation = action.Rotation
					_database.entities[region][zone][entityID].lastUpdated = time.Now()
					return nil
				}
			}
		}
	}
	return errors.New("entity not found")
}

//func addEntity(action *syncActions.AddEntity) error {
func addEntity(entity Entity) error {
	if entity.ID == nil {
		return errors.New("attempted to add entity with nil ID")
	}
	_database.Lock()
	defer _database.Unlock()

	//	localEntity := Entity{definition: definition, position: pos, rotation: rot, lastAccessed: time.Now(), lastUpdated: time.Now()}

	if _, found := _database.entities[entity.position.Zone().Region()]; !found {
		log.Debug("Creating region:", entity.position.Zone().Region())
		_database.entities[entity.position.Zone().Region()] = make(map[mapping.Zone]map[uuid.UUID]*Entity)
		_database.entities[entity.position.Zone().Region()][entity.position.Zone()] = make(map[uuid.UUID]*Entity)
	} else if _, found := _database.entities[entity.position.Zone().Region()][entity.position.Zone()]; !found {
		log.Debug("Creating zone:", entity.position.Zone())
		_database.entities[entity.position.Zone().Region()][entity.position.Zone()] = make(map[uuid.UUID]*Entity)
	}

	//map[region][Zone][GameObject ID]game object
	_database.entities[entity.position.Zone().Region()][entity.position.Zone()][*entity.ID] = &entity
	log.Debug("Entity added:", entity.ID.String())
	return nil
}

func loadDatabase() error {
	dbFile, err := os.Open(DATABASE_FILE)
	if err != nil {
		return err
	}
	defer dbFile.Close()

	decoder := gob.NewDecoder(dbFile)
	err = decoder.Decode(_database)
	if err != nil {
		return err
	}
	return nil
}

func saveDatabase() {
	dbFile, err := os.Create(DATABASE_FILE)
	if err != nil {
		log.Warn("Failed to write options file with err: ", err)
		return
	}
	encoder := gob.NewEncoder(dbFile)
	if err := encoder.Encode(_database); err != nil {
	}
	dbFile.Close()

}

func shutdown() {
	log.Info("Shutdown called")
	os.Exit(0)
}

func getEntityPosition(id *uuid.UUID) (mapping.Position, error) {
	for region, _ := range _database.entities {
		for zone, _ := range _database.entities[region] {
			for entityID, _ := range _database.entities[region][zone] {
				if entityID == *id {
					return _database.entities[region][zone][entityID].position, nil
				}
			}
		}
	}
	return mapping.Position{}, errors.New("entity does not exist")
}
