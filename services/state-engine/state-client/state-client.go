package stateClient

import (
	"errors"
	"fmt"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"

	"gitlab.com/g3n/engine/math32"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-types"
	"gitlab.com/gamerscomplete/uuid"
	//	"encoding/gob"
)

const (
	STATE_ENGINE_SERVICE_NAME = "state-engine"
	VERSION                   = 1
)

type Client struct {
	conn    *csTools.ServerConnObj
	timeout int
}

func NewClient(conn *csTools.ServerConnObj) *Client {
	//	gob.Register(&syncActions.AddEntity{})
	return &Client{conn: conn, timeout: 10}
}

func (client *Client) SetTimeout(timeout int) {
	client.timeout = timeout
}

func (client *Client) PlaceOnGrid(gridID uuid.UUID, pos [3]int, block uuid.UUID) error {
	request := stateTypes.PlaceOnGrid{GridID: gridID, Position: pos, Block: block}
	if err := client.conn.PackSendReplyTimeout(client.timeout, "PLACE_ON_GRID", STATE_ENGINE_SERVICE_NAME, client.conn.ID, request, nil); err != nil {
		return fmt.Errorf("failed to place on grid: ", err)
	}
	return nil
}

func (client *Client) RemoveBlock(gridID uuid.UUID, pos [3]int) error {
	request := stateTypes.RemoveBlock{GridID: gridID, Position: pos}
	if err := client.conn.PackSendReplyTimeout(client.timeout, "REMOVE_BLOCK", STATE_ENGINE_SERVICE_NAME, client.conn.ID, request, nil); err != nil {
		return fmt.Errorf("failed to remove block: ", err)
	}
	return nil
}

func (client *Client) RemoveGrid(gridID uuid.UUID) error {
	var success bool
	if err := client.conn.PackSendReplyTimeout(client.timeout, "REMOVE_GRID", STATE_ENGINE_SERVICE_NAME, client.conn.ID, gridID, success); err != nil {
		return fmt.Errorf("failed to remove grid: ", err)
	}
	return nil
}

func (client *Client) CreateGrid(position math32.Vector3, rotation math32.Vector3, blockID uuid.UUID) (*uuid.UUID, error) {
	request := stateTypes.CreateGrid{BlockID: blockID, Position: position, Rotation: rotation}
	var reply uuid.UUID
	if err := client.conn.PackSendReplyTimeout(client.timeout, "CREATE_GRID", STATE_ENGINE_SERVICE_NAME, client.conn.ID, request, &reply); err != nil {
		return nil, fmt.Errorf("failed to create grid: ", err)
	}
	return &reply, nil
}

func (client *Client) GetEntityDefinition(id uuid.UUID) (*gameCore.EntityDefinition, error) {
	reply := gameCore.EntityDefinition{}
	if err := client.conn.PackSendReplyTimeout(client.timeout, "GET_ENTITY_DEFINITION", STATE_ENGINE_SERVICE_NAME, client.conn.ID, id, &reply); err != nil {
		return nil, fmt.Errorf("Failed to get entity definition: ", err)
	}
	return &reply, nil
}

func (client *Client) GetItemDefinition(id uuid.UUID) (gameCore.ItemDefinition, error) {
	reply := gameCore.ItemDefinition{}
	if err := client.conn.PackSendReplyTimeout(client.timeout, "GET_ITEM_DEFINITION", STATE_ENGINE_SERVICE_NAME, client.conn.ID, id, &reply); err != nil {
		return reply, fmt.Errorf("Failed to get item definition: ", err)
	}
	return reply, nil
}

func (client *Client) CreateItem(definition gameCore.ItemDefinition) (*uuid.UUID, error) {
	reply := stateTypes.CreateItemReply{}
	if err := client.conn.PackSendReplyTimeout(client.timeout, "CREATE_ITEM", STATE_ENGINE_SERVICE_NAME, client.conn.ID, stateTypes.CreateItem{Definition: definition}, &reply); err != nil {
		return nil, fmt.Errorf("Failed to create item: ", err)
	}
	return &reply.ID, nil
}

func (client *Client) GetInventory(id uuid.UUID) (map[uuid.UUID]int, error) {
	reply := stateTypes.GetInventoryReply{}
	if err := client.conn.PackSendReplyTimeout(client.timeout, "GET_INVENTORY", STATE_ENGINE_SERVICE_NAME, client.conn.ID, id, &reply); err != nil {
		return nil, fmt.Errorf("Failed to get inventory: %v", err)
	}
	if !reply.Success {
		return nil, fmt.Errorf("Failed to get inventory")
	}
	return reply.Inventory, nil
}

func (client *Client) CreateInventoryWithID(inventoryID *uuid.UUID, contents map[uuid.UUID]int) error {
	reply := stateTypes.CreateInventoryReply{}
	if err := client.conn.PackSendReplyTimeout(client.timeout, "CREATE_INVENTORY", STATE_ENGINE_SERVICE_NAME, client.conn.ID, stateTypes.CreateInventory{ID: inventoryID, Contents: contents}, &reply); err != nil {
		return fmt.Errorf("Failed to create inventory: ", err)
	}
	return nil
}

func (client *Client) GetEntityPosition(entityID uuid.UUID) (mapping.Position, error) {
	entityPosition := stateTypes.GetEntityPositionReply{}
	//TODO: Update to find real state engine instead of local
	if err := client.conn.PackSendReplyTimeout(client.timeout, "GET_ENTITY_POSITION", STATE_ENGINE_SERVICE_NAME, client.conn.ID, stateTypes.GetEntityPosition{ID: entityID}, &entityPosition); err != nil {
		return mapping.Position{}, fmt.Errorf("Failed to fetch player position for %s : %v", entityID.String(), err)
	}
	return entityPosition.Position, nil
}

func (client *Client) CreateEntityDefinition(definition gameCore.EntityDefinition) (uuid.UUID, error) {
	var definitionIDReply uuid.UUID
	if err := client.conn.PackSendReplyTimeout(client.timeout, "CREATE_ENTITY_DEFINITION", STATE_ENGINE_SERVICE_NAME, client.conn.ID, definition, &definitionIDReply); err != nil {
		return definitionIDReply, err
	}
	return definitionIDReply, nil
}

func (client *Client) CreateEntity(definitionID uuid.UUID, pos mapping.Position, rot math32.Vector3) (*uuid.UUID, error) {
	var createEntityReply uuid.UUID
	createEntityRequest := stateTypes.CreateEntity{DefinitionID: definitionID, Position: pos, Rotation: rot}
	if err := client.conn.PackSendReplyTimeout(client.timeout, "CREATE_ENTITY", STATE_ENGINE_SERVICE_NAME, client.conn.ID, createEntityRequest, &createEntityReply); err != nil {
		return nil, err
	}
	return &createEntityReply, nil
}

func (client *Client) GetSnapshotByZones(zones []mapping.Zone) (stateTypes.Snapshot, error) {
	getSnapshotRequest := stateTypes.GetSnapshotRequest{Zones: zones}
	var getSnapshotReply stateTypes.Snapshot
	err := client.conn.PackSendReplyTimeout(10, "GET_SNAPSHOT_BY_ZONES", STATE_ENGINE_SERVICE_NAME, client.conn.ID, getSnapshotRequest, &getSnapshotReply)
	return getSnapshotReply, err
}

func (client *Client) ApplyAction(action syncActions.Action) error {
	//	var applyActionReply stateTypes.ApplyActionReply
	if action == nil {
		return errors.New("Cannot apply nil action")
	}

	if err := client.conn.PackSendReplyTimeout(2, "APPLY_ACTION", STATE_ENGINE_SERVICE_NAME, client.conn.ID, &action, nil); err != nil {
		return fmt.Errorf("Applying action failed: %v", err)
	}

	return nil
}

func (client *Client) ApplyActions(actions []syncActions.Action) error {
	//	var applyActionsReply stateTypes.ApplyActionReply
	if err := client.conn.PackSendReplyTimeout(2, "APPLY_ACTIONS", STATE_ENGINE_SERVICE_NAME, client.conn.ID, &actions, nil); err != nil {
		return fmt.Errorf("Applying actions failed: %v", err)
	}
	return nil
}

func (client *Client) GetManagedZones() ([]mapping.Zone, error) {
	var getZonesManagedReply stateTypes.GetManagedZonesReply
	if err := client.conn.PackSendReplyTimeout(2, "GET_MANAGED_ZONES", STATE_ENGINE_SERVICE_NAME, client.conn.ID, nil, &getZonesManagedReply); err != nil {
		return nil, fmt.Errorf("Fetching managed Zones failed: %v", err)
	}
	return getZonesManagedReply.Zones, nil
}

//////////////////////////////
// Stats
//////////////////////////////
func (client *Client) GetDatabaseSize() (int, error) {
	var getDatabaseSizeReply stateTypes.GetDatabaseSizeReply
	if err := client.conn.PackSendReplyTimeout(2, "GET_DATABASE_SIZE", STATE_ENGINE_SERVICE_NAME, client.conn.ID, nil, &getDatabaseSizeReply); err != nil {
		return 0, fmt.Errorf("Fetching database size failed: %v", err)
	}
	return getDatabaseSizeReply.Size, nil
}

func (client *Client) GetEntityCount() (int, error) {
	var getEntityCountReply stateTypes.GetEntityCountReply
	if err := client.conn.PackSendReplyTimeout(2, "GET_ENTITY_COUNT", STATE_ENGINE_SERVICE_NAME, client.conn.ID, nil, &getEntityCountReply); err != nil {
		return 0, fmt.Errorf("Fetching entity count failed: %v", err)
	}
	return getEntityCountReply.Count, nil
}

func (client *Client) RegisterNotifyRegions(region []mapping.Region) error {
	if err := client.conn.PackSendReplyTimeout(2, "REGISTER_NOTIFY_REGIONS", STATE_ENGINE_SERVICE_NAME, client.conn.ID, region, nil); err != nil {
		return fmt.Errorf("Failed to register notify region: %v", err)
	}
	return nil
}

func (client *Client) RegisterNotifyZones(zone []mapping.Zone) error {
	if err := client.conn.PackSendReplyTimeout(2, "REGISTER_NOTIFY_ZONES", STATE_ENGINE_SERVICE_NAME, client.conn.ID, zone, nil); err != nil {
		return fmt.Errorf("Failed to register notify zone: %v", err)
	}
	return nil
}

func (client *Client) UnregisterNotifyZones(zone []mapping.Zone) error {
	if err := client.conn.PackSendReplyTimeout(2, "UNREGISTER_NOTIFY_ZONES", STATE_ENGINE_SERVICE_NAME, client.conn.ID, zone, nil); err != nil {
		return fmt.Errorf("Failed to unregister notify zone: %v", err)
	}
	return nil
}
func (client *Client) UnregisterNotifyRegions(region []mapping.Zone) error {
	if err := client.conn.PackSendReplyTimeout(2, "UNREGISTER_NOTIFY_REGIONS", STATE_ENGINE_SERVICE_NAME, client.conn.ID, region, nil); err != nil {
		return fmt.Errorf("Failed to unregister notify region: %v", err)
	}
	return nil
}
func (client *Client) UnregisterNotifyAll() error {
	if err := client.conn.PackSendReplyTimeout(2, "UNREGISTER_NOTIFY_ALL", STATE_ENGINE_SERVICE_NAME, client.conn.ID, nil, nil); err != nil {
		return fmt.Errorf("Failed to unregister notify all: %v", err)
	}
	return nil
}
