package main

import (
	log "github.com/sirupsen/logrus"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/services/state-engine/state-types"
	"gitlab.com/gamerscomplete/uuid"
)

func (database *Database) RegisterHandlers() {
	_serverConn.RegisterHandler("CREATE_GRID", _database.processCreateGrid)
	_serverConn.RegisterHandler("REMOVE_GRID", _database.processRemoveGrid)
	_serverConn.RegisterHandler("PLACE_ON_GRID", _database.processPlaceOnGrid)
	_serverConn.RegisterHandler("REMOVE_BLOCK", _database.processRemoveBlock)

	_serverConn.RegisterHandler("CREATE_ENTITY_DEFINITION", _database.processCreateEntityDefinition)
	_serverConn.RegisterHandler("GET_ENTITY_DEFINITION", _database.processGetEntityDefinition)
	_serverConn.RegisterHandler("CREATE_ENTITY", _database.processCreateEntity)
	_serverConn.RegisterHandler("CLONE_ENTITY", processCloneEntity)
	_serverConn.RegisterHandler("GET_ENTITY_POSITION", processGetEntityPosition)

	//TODO: Still need to add this
	//  _serverConn.RegisterHandler("ADD_TO_INVENTORY", _database.processAddToInventory)
	_serverConn.RegisterHandler("CREATE_INVENTORY", _database.processCreateInventory)
	_serverConn.RegisterHandler("GET_INVENTORY", _database.processGetInventory)
	_serverConn.RegisterHandler("CREATE_ITEM", _database.processCreateItem)
	_serverConn.RegisterHandler("GET_ITEM_DEFINITION", _database.processGetItemDefinition)

	_serverConn.RegisterHandler("GET_ENTITY_COUNT", processGetEntityCount)
	_serverConn.RegisterHandler("GET_MANAGED_ZONES", processGetManagedZones)
	_serverConn.RegisterHandler("GET_DATABASE_SIZE", processGetDatabaseSize)
	_serverConn.RegisterHandler("GET_SNAPSHOT_BY_ZONES", processGetSnapshotByZones)
	_serverConn.RegisterHandler("APPLY_ACTION", processApplyAction)
	_serverConn.RegisterHandler("APPLY_ACTIONS", processApplyActions)
	_serverConn.RegisterHandler("REGISTER_NOTIFY_ZONES", processRegisterNotifyZones)
	_serverConn.RegisterHandler("UNREGISTER_NOTIFY_ZONES", processUnregisterNotifyZones)
	_serverConn.RegisterHandler("REGISTER_NOTIFY_REGIONS", processRegisterNotifyRegions)
	_serverConn.RegisterHandler("UNREGISTER_NOTIFY_REGIONS", processUnregisterNotifyRegions)
	_serverConn.RegisterHandler("UNREGISTER_NOTIFY_ALL", processUnregisterNotifyAll)
}

func (database *Database) processRemoveBlock(message csTools.ServerMessage) {
	var request stateTypes.RemoveBlock
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn("failed to unpack message: ", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	err := database.RemoveBlock(request.GridID, request.Position)
	if err != nil {
		log.WithFields(log.Fields{
			"gridID":   request.GridID.String(),
			"position": request.Position,
		}).Warn("failed to remove block: ", err)
		_serverConn.ReplyError(message, err.Error())
		return

	}

	replyMessage := csTools.Message{Command: "REPLY"}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processPlaceOnGrid(message csTools.ServerMessage) {
	var request stateTypes.PlaceOnGrid
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn("failed to unpack message: ", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	err := database.PlaceOnGrid(request.GridID, request.Position, request.Block)
	if err != nil {
		log.WithFields(log.Fields{
			"gridID":   request.GridID.String(),
			"position": request.Position,
			"blockID":  request.Block.String(),
		}).Warn("failed to place block on grid: ", err)
		_serverConn.ReplyError(message, err.Error())
		return

	}

	replyMessage := csTools.Message{Command: "REPLY"}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processRemoveGrid(message csTools.ServerMessage) {
	var request uuid.UUID
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn("failed to unpack message: ", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	success := database.RemoveGrid(request)

	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(success); err != nil {
		log.Error("Failed to pack reply: ", err)
		_serverConn.ReplyError(message, "failed to pack reply")
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processCreateGrid(message csTools.ServerMessage) {
	var request stateTypes.CreateGrid
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn("failed to unpack message: ", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	log.WithFields(log.Fields{
		"position": request.Position,
	}).Debug("received create grid request")

	id, err := database.CreateGrid(request.Position, request.Rotation, request.BlockID)
	if err != nil {
		log.Warn("failed to create grid: ", err)
		_serverConn.ReplyError(message, err.Error())
		return

	}

	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(*id); err != nil {
		log.Error("Failed to pack reply: ", err)
		_serverConn.ReplyError(message, "failed to pack reply")
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processCreateItem(message csTools.ServerMessage) {
	var request stateTypes.CreateItem
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	id, err := database.CreateItem(request.ID, request.Definition)
	if err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return

	}

	reply := stateTypes.CreateItemReply{ID: *id, Success: true}
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		_serverConn.ReplyError(message, "failed to pack reply")
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processGetItemDefinition(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var request uuid.UUID
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	log.WithFields(log.Fields{
		"ID": request.String(),
	}).Debug("recieved get item definition request")

	itemDefinition, err := database.GetItemDefinition(request)
	if err != nil {
		log.WithFields(log.Fields{
			"ID": request.String(),
		}).Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return

	}

	if err := replyMessage.PackType(itemDefinition); err != nil {
		log.Error("Failed to pack reply", err)
		log.WithFields(log.Fields{
			"ID": request.String(),
		}).Warn("failed to pack reply:", err)
		_serverConn.ReplyError(message, "failed to pack reply")
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processCreateInventory(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var request stateTypes.CreateInventory
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	id, err := database.CreateInventory(request)
	if err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	reply := stateTypes.CreateInventoryReply{Success: true, ID: id}
	if err = replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		_serverConn.ReplyError(message, "failed to pack reply")
		return
	}
	log.WithFields(log.Fields{
		"ID": id.String(),
	}).Info("successfully created inventory")
	_serverConn.SendServerMessageReply(message, replyMessage)

}

func (database *Database) processGetInventory(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var request uuid.UUID
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	log.WithFields(log.Fields{
		"ID": request.String(),
	}).Debug("Processing get inventory request")

	inventory, err := database.GetInventory(request)
	if err != nil {
		log.WithFields(log.Fields{
			"ID": request.String(),
		}).Warn("failed to get inventory: ", err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	reply := stateTypes.GetInventoryReply{Success: true, Inventory: inventory}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processCreateEntityDefinition(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var request gameCore.EntityDefinition
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	entityDefinitionID, err := database.CreateEntityDefinition(request)
	if err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	if err := replyMessage.PackType(entityDefinitionID); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply "
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processGetEntityDefinition(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var request uuid.UUID
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	log.WithFields(log.Fields{
		"entityDefID": request.String(),
	}).Debug("processing get entity definition request")
	entityDefinition := database.GetEntityDefinition(request)
	if entityDefinition == nil {
		log.WithFields(log.Fields{
			"ID": request.String(),
		}).Warn("Attempted to fetch entity definition that was not managed")
		_serverConn.ReplyError(message, "entity definition not found")
		return
	}
	if err := replyMessage.PackType(entityDefinition); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply "
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func (database *Database) processCreateEntity(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var request stateTypes.CreateEntity
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	entityID, err := database.CreateEntity(request.DefinitionID, request.Position, request.Rotation)
	if err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	//	reply := stateTypes.CreateEntityReply{ID: *entityID}
	//	if err := replyMessage.PackType(reply); err != nil {
	if err := replyMessage.PackType(*entityID); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply "
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	log.WithFields(log.Fields{
		"ID": entityID.String(),
	}).Debug("Successfully created entity")
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processCloneEntity(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var cloneRequest stateTypes.CloneEntity
	if err := message.Data.UnpackType(&cloneRequest); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	newID, err := cloneEntity(cloneRequest.ID)
	if err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}

	reply := stateTypes.CreateEntityReply{ID: *newID}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply "
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)

}

func processRegisterNotifyZones(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var zones []mapping.Zone
	if err := message.Data.UnpackType(&zones); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	if err := registerNotifyZones(zones, message.FromService, message.FromID); err != nil {
		_serverConn.ReplyError(message, err.Error())
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processRegisterNotifyRegions(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	var regions []mapping.Region
	if err := message.Data.UnpackType(&regions); err != nil {
		log.Warn(err)
		_serverConn.ReplyError(message, err.Error())
		return
	}
	if err := registerNotifyRegions(regions, message.FromService, message.FromID); err != nil {
		_serverConn.ReplyError(message, err.Error())
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processUnregisterNotifyRegions(message csTools.ServerMessage) {
	_serverConn.ReplyError(message, "processUnregisterNotifyRegions not implemented")
}

func processUnregisterNotifyZones(message csTools.ServerMessage) {
	_serverConn.ReplyError(message, "processUnregisterNotifyZones not implemeneted")
}

func processUnregisterNotifyAll(message csTools.ServerMessage) {
	_serverConn.ReplyError(message, "Notify all not implemented")
}

func processApplyActions(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	log.Info("Recieved apply actions request")
	var actions []syncActions.Action
	if err := message.Data.UnpackType(&actions); err != nil {
		log.Warn("Failed to unpack actions:", err)
		replyMessage.Error = "Failed to unpack message"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	for _, value := range actions {
		if err := applyAction(value); err != nil {
			log.Warn("Failed to apply action:", err)
			replyMessage.Error = "Failed to apply action: " + err.Error()
			_serverConn.SendServerMessageReply(message, replyMessage)
			return
		}
	}
	//Not sure if this will cause errors with the other side expecting to unpack a type. Will test with nil's
	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processApplyAction(message csTools.ServerMessage) {
	log.Info("Recieved apply action request")
	replyMessage := csTools.Message{Command: "REPLY"}

	var action syncActions.Action
	if err := message.Data.UnpackType(&action); err != nil {
		log.Warn("Failed to unpack action:", err)
		replyMessage.Error = "Failed to unpack message"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	if err := applyAction(action); err != nil {
		log.Warn("Failed to apply action:", err)
		replyMessage.Error = "Failed to apply action: " + err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	//Have to send a dummy reply to not get packsendreply to error on EOF. Will make this unshitty later
	/*	reply := stateTypes.ApplyActionReply{}
		if err := replyMessage.PackType(reply); err != nil {
			log.Error("Failed to pack reply", err)
			replyMessage.Error = "Failed to pack reply "
			_serverConn.SendServerMessageReply(message, replyMessage)
			return
		}
	*/
	log.Info("Action successfully applied")
	_serverConn.SendServerMessageReply(message, replyMessage)

}

func processGetEntityCount(message csTools.ServerMessage) {
	log.Info("Recieved get entity count request")
	reply := stateTypes.GetEntityCountReply{Count: getEntityCount()}
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processGetDatabaseSize(message csTools.ServerMessage) {
	log.Info("Recieved database size request")
	reply := stateTypes.GetDatabaseSizeReply{Size: getDatabaseSize()}
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processGetManagedZones(message csTools.ServerMessage) {
	log.Info("Recieved managed zones request")
	reply := stateTypes.GetManagedZonesReply{Zones: getManagedZones()}
	replyMessage := csTools.Message{Command: "REPLY"}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processGetSnapshotByZones(message csTools.ServerMessage) {
	log.Info("Recieved get snapshot request")
	replyMessage := csTools.Message{Command: "REPLY"}
	var request stateTypes.GetSnapshotRequest
	if err := message.Data.UnpackType(&request); err != nil {
		log.Warn(err)
		replyMessage.Error = "Failed to unpack request"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	snapshot, err := getSnapshotByZones(request.Zones)
	if err != nil {
		log.Error("Failed to get snapshot:", err)
		replyMessage.Error = err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	if err := replyMessage.PackType(snapshot); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	_serverConn.SendServerMessageReply(message, replyMessage)
}

func processGetEntityPosition(message csTools.ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}

	entityPositionRequest := stateTypes.GetEntityPosition{}
	if err := message.Data.UnpackType(&entityPositionRequest); err != nil {
		log.Warn("Failed to unpack get_entity request")
		replyMessage.Error = "Failed to unpack get_entity request"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	position, err := getEntityPosition(&entityPositionRequest.ID)
	if err != nil {
		replyMessage.Error = err.Error()
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}

	reply := stateTypes.GetEntityPositionReply{Position: position}
	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		_serverConn.SendServerMessageReply(message, replyMessage)
		return
	}
	_serverConn.SendServerMessageReply(message, replyMessage)
}
