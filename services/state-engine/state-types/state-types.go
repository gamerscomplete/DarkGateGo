package stateTypes

import (
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/uuid"
)

type RemoveBlock struct {
	GridID   uuid.UUID
	Position [3]int
}

type PlaceOnGrid struct {
	GridID   uuid.UUID
	Position [3]int
	Block    uuid.UUID
}

type CreateGrid struct {
	Position math32.Vector3
	Rotation math32.Vector3
	BlockID  uuid.UUID
}

type GetInventoryReply struct {
	Success   bool
	Inventory map[uuid.UUID]int
}

// ID is optional. If nil a ID will be generated and returned
type CreateItem struct {
	ID         *uuid.UUID
	Definition gameCore.ItemDefinition
}

type CreateItemReply struct {
	ID      uuid.UUID
	Success bool
}

type CreateInventory struct {
	ID       *uuid.UUID
	Contents map[uuid.UUID]int
}

type CreateInventoryReply struct {
	ID      uuid.UUID
	Success bool
}

type ActionUpdate struct {
	//This should potentionally be a map instead of a slice
	Action syncActions.Action
}

type GetDatabaseSizeReply struct {
	Size int
}

type GetManagedZonesReply struct {
	Zones []mapping.Zone
}

type GetEntityCountReply struct {
	Count int
}

type CreateEntityTemplate struct {
	Definition gameCore.EntityDefinition
}

type CreateTemplateReply struct {
	ID uuid.UUID
}

type CreateEntity struct {
	DefinitionID uuid.UUID
	Position     mapping.Position
	Rotation     math32.Vector3
}

type CreateEntityReply struct {
	ID uuid.UUID
}

type CloneEntity struct {
	ID uuid.UUID
}

type GetSnapshotRequest struct {
	Zones []mapping.Zone
}

type Snapshot struct {
	Actions []syncActions.Action
}

type GetEntityPosition struct {
	ID uuid.UUID
}

type GetEntityPositionReply struct {
	Position mapping.Position
}
