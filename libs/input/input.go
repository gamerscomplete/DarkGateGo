//TODO: Change the name to something like gameInput to be less ambigous and allow the input variable name
package input

import (
	"gitlab.com/gamerscomplete/DarkGateGo/libs/tick"
)

type InputMask uint8

type UserInput struct {
	Tick tick.Tick
	InputMask
}

const (
	//Input bits
	FORWARD InputMask = 1 << iota
	BACK
	LEFT
	RIGHT
	UP   //Shouldnt be an input type
	DOWN //shoudlnt be an input type
	LMB
	RMB
)

func (inputMask InputMask) InputsApplied() bool {
	if inputMask == 0 {
		return false
	}
	return true
}

func (inputMask InputMask) HasInput(input InputMask) bool {
	return inputMask&input != 0
}

func (inputMask *InputMask) AddInput(input InputMask) {
	*inputMask |= input
}

func (inputMask *InputMask) ClearInput(input InputMask) {
	*inputMask &= ^input
}

func (inputMask *InputMask) Toggleinput(input InputMask) {
	*inputMask ^= input
}
