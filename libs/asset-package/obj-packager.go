package assetPackage

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/loader/obj"
	"gitlab.com/g3n/engine/material"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/uuid"
)

// Light gray default material used as when other materials cannot be loaded.
var defaultMat = &obj.Material{
	Diffuse:   math32.Color{R: 0.7, G: 0.7, B: 0.7},
	Ambient:   math32.Color{R: 0.7, G: 0.7, B: 0.7},
	Specular:  math32.Color{R: 0.5, G: 0.5, B: 0.5},
	Shininess: 30.0,
}

func PackageObj(objPath string, objMtl string) (map[uuid.UUID][]uuid.UUID, []IAssetPackage, error) {
	dec, err := obj.Decode(objPath, objMtl)
	if err != nil {
		return nil, nil, err
	}

	geoms := make(map[uuid.UUID][]uuid.UUID)
	var packageList []IAssetPackage

	for i := 0; i < len(dec.Objects); i++ {
		// Creates object geometry
		geom, err := dec.NewGeometry(&dec.Objects[i])
		if err != nil {
			return nil, nil, err
		}
		geomPackage := PackageGeometry(dec.Objects[i].Name, geom)
		packageList = append(packageList, geomPackage)

		// Single material
		if geom.GroupCount() == 1 {
			// get Material info from mtl file and ensure it's valid.
			// substitute default material if it is not.
			var matDesc *obj.Material
			var matName string
			if len(dec.Objects[i].Materials) > 0 {
				matName = dec.Objects[i].Materials[0]
			}
			matDesc = dec.Materials[matName]
			if matDesc == nil {
				matDesc = defaultMat
				// log warning
				log.WithFields(log.Fields{
					"name": dec.Objects[i].Name,
				}).Warn("could not find material. using default material")
			}

			mat := material.NewStandard(&matDesc.Diffuse)
			ambientColor := mat.AmbientColor()
			mat.SetAmbientColor(ambientColor.Multiply(&matDesc.Ambient))
			mat.SetSpecularColor(&matDesc.Specular)
			mat.SetShininess(matDesc.Shininess)
			// Loads material textures if specified
			err = dec.LoadTex(&mat.Material, matDesc)
			if err != nil {
				return nil, nil, err
			}

			materialPackage := PackageMaterial(matName, mat)
			geoms[*geomPackage.ID()] = []uuid.UUID{*materialPackage.ID()}

			packageList = append(packageList, materialPackage)
			continue
		}

		//    mesh := graphic.NewMesh(geom, nil)
		geoms[*geomPackage.ID()] = make([]uuid.UUID, geom.GroupCount())
		for idx := 0; idx < geom.GroupCount(); idx++ {
			group := geom.GroupAt(idx)

			// get Material info from mtl file and ensure it's valid.
			// substitute default material if it is not.
			var matDesc *obj.Material
			var matName string
			if len(dec.Objects[i].Materials) > group.Matindex {
				matName = dec.Objects[i].Materials[group.Matindex]
			}
			matDesc = dec.Materials[matName]
			if matDesc == nil {
				matDesc = defaultMat
				// log warning
				log.WithFields(log.Fields{
					"name": dec.Objects[i].Name,
				}).Warn("could not find material for %s. using default material")
			}

			// Creates material for mesh

			matGroup := material.NewStandard(&matDesc.Diffuse)
			ambientColor := matGroup.AmbientColor()
			matGroup.SetAmbientColor(ambientColor.Multiply(&matDesc.Ambient))
			matGroup.SetSpecularColor(&matDesc.Specular)
			matGroup.SetShininess(matDesc.Shininess)
			// Loads material textures if specified
			err = dec.LoadTex(&matGroup.Material, matDesc)
			if err != nil {
				return nil, nil, err
			}

			materialPackage := PackageMaterial(matName, matGroup)
			packageList = append(packageList, materialPackage)
			geoms[*geomPackage.ID()][idx] = *materialPackage.ID()
			//        mesh.AddGroupMaterial(matGroup, idx)
		}
	}

	return geoms, packageList, nil
}
