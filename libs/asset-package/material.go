package assetPackage

import (
	"bytes"
	"encoding/gob"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/material"
	"gitlab.com/g3n/engine/math32"
)

type MaterialPackage struct {
	*AssetPackage
	Data []byte
}

type Material struct {
	Shader       string
	ShaderUnique bool
	UseLights    material.UseLights
	Side         material.Side
	Transparent  bool
	Wireframe    bool
	Textures     []TexturePackage

	Standard  bool
	Ambient   math32.Color
	Diffuse   math32.Color
	Specular  math32.Color
	Emissive  math32.Color
	Opacity   float32
	Shininess float32
}

func PackageMaterial(name string, iMat material.IMaterial) *MaterialPackage {
	mat := iMat.GetMaterial()
	matPackage := MaterialPackage{AssetPackage: NewAssetPackage(name)}

	var transferMat Material
	if standard, asserted := iMat.(*material.Standard); asserted {
		transferMat.Standard = true
		transferMat.Ambient = standard.AmbientColor()
		transferMat.Diffuse = standard.DiffuseColor()
		transferMat.Specular = standard.SpecularColor()
		transferMat.Emissive = standard.EmissiveColor()
		transferMat.Opacity = standard.Opacity()
		transferMat.Shininess = standard.Shininess()
	}

	transferMat.Shader = mat.Shader()
	transferMat.ShaderUnique = mat.ShaderUnique()
	transferMat.UseLights = mat.UseLights()
	transferMat.Side = mat.Side()
	transferMat.Transparent = mat.Transparent()
	transferMat.Wireframe = mat.Wireframe()

	textures := mat.Textures()
	transferMat.Textures = make([]TexturePackage, len(textures))
	i := 0
	for _, value := range textures {
		//TODO: The texture names prolly shouldnt be the same as the material name, but I dont know what else they should be named right now
		transferMat.Textures[i] = *PackageTexture(name, value)
	}

	//encode
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(transferMat)
	if err != nil {
		log.Error(err)
		return nil
	}

	//compress
	matPackage.Data = Compress(buf.Bytes())

	return &matPackage
}

func (materialPackage *MaterialPackage) Material() material.IMaterial {
	decompressed, err := Decompress(materialPackage.Data)
	if err != nil {
		log.Error("failed to decompress material: ", err.Error())
		return nil
	}

	var outMaterial Material
	dec := gob.NewDecoder(bytes.NewReader(decompressed))
	err = dec.Decode(&outMaterial)
	if err != nil {
		log.Fatal(err)
		return nil
	}

	var iMat material.IMaterial
	if outMaterial.Standard {
		mat := material.NewStandard(&outMaterial.Diffuse)
		mat.SetAmbientColor(&outMaterial.Ambient)
		mat.SetSpecularColor(&outMaterial.Specular)
		mat.SetEmissiveColor(&outMaterial.Emissive)
		mat.SetShininess(outMaterial.Shininess)
		mat.SetOpacity(outMaterial.Opacity)
		iMat = mat
	} else {
		mat := material.NewMaterial()
		iMat = mat
	}

	if outMaterial.Shader != "" {
		iMat.GetMaterial().SetShader(outMaterial.Shader)
	}
	iMat.GetMaterial().SetShaderUnique(outMaterial.ShaderUnique)
	iMat.GetMaterial().SetUseLights(outMaterial.UseLights)
	iMat.GetMaterial().SetSide(outMaterial.Side)
	iMat.GetMaterial().SetTransparent(outMaterial.Transparent)
	iMat.GetMaterial().SetWireframe(outMaterial.Wireframe)
	log.Debug("Adding ", len(outMaterial.Textures), " textures")
	for _, value := range outMaterial.Textures {
		iMat.GetMaterial().AddTexture(value.Texture())
	}

	return iMat
}
