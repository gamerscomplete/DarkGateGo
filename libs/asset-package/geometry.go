package assetPackage

import (
	"bytes"
	"encoding/gob"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/geometry"
	"gitlab.com/g3n/engine/gls"
	"gitlab.com/g3n/engine/math32"
)

type GeometryPackage struct {
	*AssetPackage
	Data []byte
}

type Geometry struct {
	Indices math32.ArrayU32
	VBOs    []VBO
	Groups  []geometry.Group
}

type VBO struct {
	Buffer     math32.ArrayF32
	Attributes []gls.VBOattrib
}

func PackageGeometry(name string, iGeom geometry.IGeometry) *GeometryPackage {
	geomPackage := &GeometryPackage{AssetPackage: NewAssetPackage(name)}

	geom := iGeom.GetGeometry()
	var outGeom Geometry
	outGeom.Indices = geom.Indices()
	vbos := geom.VBOs()
	outGeom.VBOs = make([]VBO, len(vbos))
	for key, value := range vbos {
		vbo := VBO{}
		vbo.Buffer = *value.Buffer()
		vbo.Attributes = value.Attributes()
		outGeom.VBOs[key] = vbo
	}

	outGeom.Groups = make([]geometry.Group, geom.GroupCount())
	for i := 0; i < geom.GroupCount(); i++ {
		outGeom.Groups[i] = *geom.GroupAt(i)
	}

	//encode
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(outGeom)
	if err != nil {
		log.Error(err)
		return nil
	}

	//compress
	geomPackage.Data = Compress(buf.Bytes())

	return geomPackage
}

func (geometryPackage *GeometryPackage) Geometry() geometry.IGeometry {
	decompressed, err := Decompress(geometryPackage.Data)
	if err != nil {
		log.Error("failed to decompress geometry: ", err.Error())
		return nil
	}

	var outGeom Geometry
	dec := gob.NewDecoder(bytes.NewReader(decompressed))
	err = dec.Decode(&outGeom)
	if err != nil {
		log.Fatal(err)
		return nil
	}

	geom := geometry.NewGeometry()

	geom.AddGroupList(outGeom.Groups)

	geom.SetIndices(outGeom.Indices)

	for _, value := range outGeom.VBOs {
		geom.AddVBO(gls.NewVBO(value.Buffer).SetAttributes(value.Attributes))
	}
	return geom
}
