package assetPackage

import (
	"encoding/gob"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/uuid"
	"time"
)

type IAssetPackage interface {
	Name() string
	ID() *uuid.UUID
	Version() int
	CreateTime() time.Time
	LastModified() time.Time
}

//Had to do a hack on naming to get it to compile. go doesnt allow method names to match variable names
type AssetPackage struct {
	XName       string
	XID         *uuid.UUID
	XVersion    int
	XCreateTS   time.Time
	XModifiedTS time.Time
}

func init() {
	//	gob.Register(&ShaderPackage{})
	gob.Register(&GeometryPackage{})
	gob.Register(&MaterialPackage{})
	gob.Register(&TexturePackage{})
}

func NewAssetPackage(name string) *AssetPackage {
	newID, _ := uuid.NewV4()
	return &AssetPackage{XName: name, XID: newID, XVersion: 1, XCreateTS: time.Now()}
}

func (assetPackage *AssetPackage) Name() string {
	return assetPackage.XName
}
func (assetPackage *AssetPackage) ID() *uuid.UUID {
	if assetPackage == nil {
		log.Error("Attempted to get ID() from nil asset package")
		return nil
	}
	return assetPackage.XID
}
func (assetPackage *AssetPackage) Version() int {
	return assetPackage.XVersion
}
func (assetPackage *AssetPackage) LastModified() time.Time {
	return assetPackage.XModifiedTS
}
func (assetPackage *AssetPackage) CreateTime() time.Time {
	return assetPackage.XCreateTS
}
