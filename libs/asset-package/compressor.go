package assetPackage

import (
	"bytes"
	"compress/gzip"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
)

func Compress(s []byte) []byte {

	zipbuf := bytes.Buffer{}
	zipped := gzip.NewWriter(&zipbuf)
	zipped.Write(s)
	zipped.Close()
	return zipbuf.Bytes()
}

func Decompress(s []byte) ([]byte, error) {

	rdr, err := gzip.NewReader(bytes.NewReader(s))
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	data, err := ioutil.ReadAll(rdr)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	rdr.Close()
	return data, nil
}
