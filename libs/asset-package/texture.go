package assetPackage

import (
	"bytes"
	"encoding/gob"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/texture"
)

type TexturePackage struct {
	*AssetPackage
	Data []byte
}

type Texture struct {
	//UniformNamesSampler string
	//  UniformNamesInfo string

	Visible   bool
	MagFilter uint32
	MinFilter uint32

	RepeatX float32
	RepeatY float32

	OffsetX float32
	OffsetY float32

	WrapS uint32
	WrapT uint32

	TexWidth      int
	TexHeight     int
	TexFormat     int
	TexFormatType int
	TexIformat    int
	TexPix        []uint8
	//  Note          string
}

func PackageTexture(name string, tex *texture.Texture2D) *TexturePackage {
	texPackage := TexturePackage{AssetPackage: NewAssetPackage(name)}

	var texOut Texture
	texOut.Visible = tex.Visible()
	texOut.MagFilter = tex.MagFilter()
	texOut.MinFilter = tex.MinFilter()
	texOut.WrapS = tex.WrapS()
	texOut.WrapT = tex.WrapT()
	texOut.RepeatX, texOut.RepeatY = tex.Repeat()
	texOut.OffsetX, texOut.OffsetY = tex.Offset()
	var pix interface{}

	//Peter would love this one
	texOut.TexWidth, texOut.TexHeight, texOut.TexFormat, texOut.TexFormatType, texOut.TexIformat, pix = tex.Data()
	if pixAsserted, success := pix.([]uint8); !success {
		log.Warn("PackageMaterial failed to assert data to []uint8")
	} else {
		log.Debug("Adding texpix with length: ", len(pixAsserted))
		texOut.TexPix = pixAsserted
	}
	log.Debug("packaging texture with pixel count: ", len(texOut.TexPix))

	//encode
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(texOut)
	if err != nil {
		log.Error(err)
		return nil
	}
	texPackage.Data = Compress(buf.Bytes())

	return &texPackage
}

func (texturePackage *TexturePackage) Texture() *texture.Texture2D {
	decompressed, err := Decompress(texturePackage.Data)
	if err != nil {
		log.Error("failed to decompress texture: ", err.Error())
		return nil
	}

	var outTex Texture
	dec := gob.NewDecoder(bytes.NewReader(decompressed))
	err = dec.Decode(&outTex)
	if err != nil {
		log.Fatal(err)
		return nil
	}

	//  log.Debug("TexPix length: ", len(texturePackage.TexPix))
	tex := texture.NewTexture2DFromData(
		outTex.TexWidth,
		outTex.TexHeight,
		outTex.TexFormat,
		outTex.TexFormatType,
		outTex.TexIformat,
		outTex.TexPix,
	)
	tex.SetVisible(outTex.Visible)
	tex.SetMagFilter(outTex.MagFilter)
	tex.SetMinFilter(outTex.MinFilter)
	tex.SetWrapS(outTex.WrapS)
	tex.SetWrapT(outTex.WrapT)
	tex.SetRepeat(outTex.RepeatX, outTex.RepeatY)
	tex.SetOffset(outTex.OffsetX, outTex.OffsetY)
	return tex
}
