package mapping

import (
	"math"
)

const (
	REGION_SIZE = ZONE_SIZE * 10
)

type Region [3]int

func (region Region) Zones() []Zone {
	var offset [3]int
	if math.Abs(float64(region[0])) > 1 {
		//		offset[0] = (int(math.Abs(float64(region[0]))) - 1) * ZONE_SIZE
		offset[0] = (int(math.Abs(float64(region[0]))) - 1) * REGION_SIZE
	} else {
		offset[0] = 0
	}
	if math.Abs(float64(region[1])) > 1 {
		offset[1] = (int(math.Abs(float64(region[1]))) - 1) * REGION_SIZE
	} else {
		offset[1] = 0
	}
	if math.Abs(float64(region[2])) > 1 {
		offset[2] = (int(math.Abs(float64(region[2]))) - 1) * REGION_SIZE
	} else {
		offset[2] = 0
	}

	var zoneList []Zone
	for x := 0; x < REGION_SIZE/ZONE_SIZE; x++ {
		for y := 0; y < REGION_SIZE/ZONE_SIZE; y++ {
			for z := 0; z < REGION_SIZE/ZONE_SIZE; z++ {
				zoneList = append(zoneList,
					Zone{
						(offset[0] + x) * sign(region[0]),
						(offset[1] + y) * sign(region[1]),
						(offset[2] + z) * sign(region[2]),
					})
			}
		}
	}
	return zoneList
}
