package mapping

import (
	"fmt"
	"gitlab.com/g3n/engine/math32"
	"math"
)

type Position math32.Vector3

//////////////////////
/// Position utils ///
//////////////////////

func (position Position) Distance(positionTwo Position) float32 {
	distance := math.Sqrt(float64((position.X-positionTwo.X)*(position.X-positionTwo.X) +
		(position.Y-positionTwo.Y)*(position.Y-positionTwo.Y) +
		(position.Z-positionTwo.Z)*(position.Z-positionTwo.Z)))
	return float32(distance)
}

func (position Position) Region() Region {
	//Since we just need to divide by another 10 we can just feed it back through gitzone since it has already done it once for 10 and we want 100
	return position.Zone().Region()
}

func (position Position) Zone() Zone {
	var x = int(position.X / ZONE_SIZE)
	var y = int(position.Y / ZONE_SIZE)
	var z = int(position.Z / ZONE_SIZE)

	return Zone{getPos(x, position.X), getPos(y, position.Y), getPos(z, position.Z)}
}

func (position Position) InZone(zone Zone) bool {
	if position.Zone() == zone {
		return true
	}
	return false
}

func (position Position) Vector3() math32.Vector3 {
	return math32.Vector3(position)
}

func (position Position) String() string {
	return fmt.Sprintf("(%f, %f, %f)", position.X, position.Y, position.Z)
}

/// TO IMPLEMENT!! ///
/*

func Lerp() Position {
}
func lerp(t, px, py, qx, qy float32) (x, y float32) {
	return px + t*(qx-px), py + t*(qy-py)
}

func clamp(i, width int32) uint {
	if i < 0 {
		return 0
	}
	if i < width {
		return uint(i)
	}
	return uint(width)
}

func moveTowards() {

}
*/
