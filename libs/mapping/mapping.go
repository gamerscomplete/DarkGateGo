package mapping

//////////////////////
/// Private utils ///
/////////////////////
func sign(input int) int {
	if input >= 0 {
		return 1
	}
	return -1
}
func getPos(pos int, origPos float32) int {
	if pos == 0 {
		if origPos >= 0 {
			pos = 1
		} else {
			pos = -1
		}
	} else {
		pos += sign(pos)
	}
	return pos
}
