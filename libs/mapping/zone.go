package mapping

import (
	"math"
)

const (
	ZONE_SIZE = 1000
)

type Zone [3]int

//////////////////
/// Zone tools ///
//////////////////

func (zone Zone) Mul(c int) Zone {
	return Zone{zone[0] * c, zone[1] * c, zone[2] * c}
}

func (zone Zone) Add(zoneTwo Zone) Zone {
	return Zone{zone[0] + zoneTwo[0], zone[1] + zoneTwo[1], zone[2] + zoneTwo[2]}
}

func (zone Zone) Sub(zoneTwo Zone) Zone {
	return Zone{zone[0] - zoneTwo[0], zone[1] - zoneTwo[1], zone[2] - zoneTwo[2]}
}

func (zone *Zone) FindCenter() Position {
	var adder Zone
	if math.Abs(float64(zone[0])) > 1 {
		adder[0] = (ZONE_SIZE*zone[0] - (ZONE_SIZE * sign(zone[0]))) + ((ZONE_SIZE / 2) * sign(zone[0]))
	} else {
		adder[0] = (ZONE_SIZE / 2) * sign(zone[0])
	}
	if math.Abs(float64(zone[1])) > 1 {
		adder[1] = (ZONE_SIZE*zone[1] - (ZONE_SIZE * sign(zone[1]))) + ((ZONE_SIZE / 2) * sign(zone[1]))
	} else {
		adder[1] = (ZONE_SIZE / 2) * sign(zone[1])
	}
	if math.Abs(float64(zone[2])) > 1 {
		adder[2] = (ZONE_SIZE*zone[2] - (ZONE_SIZE * sign(zone[2]))) + ((ZONE_SIZE / 2) * sign(zone[2]))
	} else {
		adder[2] = (ZONE_SIZE / 2) * sign(zone[2])
	}
	return adder.ToPosition()
}

func (zone Zone) ToPosition() Position {
	return Position{X: float32(zone[0]), Y: float32(zone[1]), Z: float32(zone[2])}
}

func (zone Zone) GetSurrounding(distance int) []Zone {
	var zones []Zone
	for x := -distance; x <= distance; x++ {
		for y := -distance; y <= distance; y++ {
			for z := -distance; z <= distance; z++ {
				/*				zones = append(zones, Zone{
									zone[0] + getPos(x, float32(x)),
									zone[1] + getPos(y, float32(y)),
									zone[2] + getPos(z, float32(z)),
								})
				*/
				zones = append(zones, Zone{zone[0] + x, zone[1] + y, zone[2] + z})
				//              zones = append(zones, mgl.Vec3{zone[0] + float32(x), zone[1] + float32(y), zone[2] + float32(z)})
			}
		}
	}
	return zones
}

func (zone Zone) Region() Region {
	var x = zone[0] / REGION_SIZE
	var y = zone[1] / REGION_SIZE
	var z = zone[2] / REGION_SIZE

	if x == 0 {
		x = 1
	}
	if y == 0 {
		y = 1
	}
	if z == 0 {
		z = 1
	}

	return Region{x, y, z}
	//	return Region{getPos(x, float32(zone[0])), getPos(y, float32(zone[1])), getPos(z, float32(zone[2]))}
}
