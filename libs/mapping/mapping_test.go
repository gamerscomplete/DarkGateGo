package mapping

import (
	"testing"
)

func TestGetZones(t *testing.T) {
	startingPosition := Position{-10, 0, 50}

	for _, value := range startingPosition.Region().Zones() {
		if value.Region() != startingPosition.Region() {
			t.Error("Recieved a zone outside region. Should have been region:", startingPosition.Region(), "Region returned:", value.Region())
			return
		}
	}
}

func TestFindCenter(t *testing.T) {
	startingPosition := Position{0, -500, 700}
	startingZone := startingPosition.Zone()
	zoneCenter := startingZone.FindCenter()
	if zoneCenter.Zone() != startingZone {
		t.Error("value out of bounds of zone it was created from. From Zone:", startingZone, "Center:", zoneCenter, "Ending zone:", zoneCenter.Zone())
	}
}

/*
func TestGetRegionFromPosition(t *testing.T) {
	position := Position{0,0,0}
	position.Region()
}
*/
