package security

import ()

const (
	VIEW_USER PermissionMask = 1 << iota
	EDIT_USER
	VIEW_ENTITY
	EDIT_ENTITY
	POST_NEWS
)

type PermissionMask uint64

func (mask PermissionMask) HasPermission(permission PermissionMask) bool {
	return mask&permission != 0
}

func (mask *PermissionMask) AddPermission(permission PermissionMask) {
	*mask |= permission
}

func (mask *PermissionMask) RemovePermission(permission PermissionMask) {
	*mask &= ^permission
}
