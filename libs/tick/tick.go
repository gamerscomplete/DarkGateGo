package tick

import (
	"time"
)

const (
	TICKS_PER_SECOND = 60
	MS_PER_TICK      = 1000 / TICKS_PER_SECOND
)

type Tick uint64

func CurrentTick() Tick {
	return Tick(uint64(time.Now().UnixNano()/int64(time.Millisecond)) / MS_PER_TICK)
}

func (tick *Tick) TimeTillNextTick() time.Duration {
	return time.Duration((int64(time.Millisecond) * MS_PER_TICK * (int64(*tick) + 1)) - time.Now().UnixNano())
}

/*
func TimeTillTick(currentTick Tick) time.Duration {
    return time.Duration((int64(time.Millisecond) * MS_PER_TICK * (int64(currentTick) + 1)) - time.Now().UnixNano())
}

*/
