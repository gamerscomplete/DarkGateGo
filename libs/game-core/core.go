package gameCore

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/graphic"
	//	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-manager"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/tick"
	"gitlab.com/gamerscomplete/uuid"
	//    "gitlab.com/g3n/engine/experimental/physics"
	"reflect"
	"sync"
	"time"
)

const (
	BLOCK_SIZE = 1
)

var (
	gCore *GameCore
)

type GameCore struct {
	*core.Node
	assMan     *assetManager.AssetManager
	entities   map[uuid.UUID]*Entity
	entityLock sync.RWMutex

	grids    map[uuid.UUID]*Grid
	gridLock sync.RWMutex

	entityDefinitions map[uuid.UUID]*EntityDefinition
	entityDefLock     sync.RWMutex

	fetcher iFetcher

	shouldStop bool

	fixedUpdateCallbacks map[string]func(tick tick.Tick, delta time.Duration)

	updatesChan chan updateRecord

	//	simulation *physics.Simulation

}

type updateRecord struct {
	tick    tick.Tick
	actions []syncActions.Action
}

//We are using an interface to take in all of the function calls needed for fetchers without taking them all in individually
type iFetcher interface {
	GetEntityDefinition(uuid.UUID) (*EntityDefinition, error)
}

//This allows things to get the core without having to explictely passed it on everything.
//	The big one is it allows entities to not know about the core but still do their callbacks for things like updating position
func Core() *GameCore {
	return gCore
}

func NewGameCore(assMan *assetManager.AssetManager, fetcher iFetcher) *GameCore {
	coreNode := new(core.Node)
	gameCore := GameCore{
		Node:                 coreNode,
		assMan:               assMan,
		entityDefinitions:    make(map[uuid.UUID]*EntityDefinition),
		entities:             make(map[uuid.UUID]*Entity),
		fetcher:              fetcher,
		fixedUpdateCallbacks: make(map[string]func(tick tick.Tick, delta time.Duration)),
		grids:                make(map[uuid.UUID]*Grid),
		//		simulation:           physics.NewSimulation(coreNode),
	}
	//    gravity := physics.NewConstantForceField(&math32.Vector3{0, -0.98, 0})
	//	gameCore.simulation.AddForceField(gravity)

	coreNode.Init(&gameCore)
	gCore = &gameCore
	return &gameCore
}

func (gameCore *GameCore) GetEntity(id uuid.UUID) *Entity {
	gameCore.entityLock.RLock()
	defer gameCore.entityLock.RUnlock()
	if entity, found := gameCore.entities[id]; found {
		return entity
	}
	return nil
}

//func (gameCore *GameCore) GetGrids() map[uuid.UUID]*Grid {
func (gameCore *GameCore) GetGrids() []*Grid {
	var returnList []*Grid
	gameCore.gridLock.RLock()
	for _, value := range gameCore.grids {
		returnList = append(returnList, value)
	}
	gameCore.gridLock.RUnlock()
	return returnList
}

func (gameCore *GameCore) RegisterFixedUpdateCallback(name string, callback func(tick tick.Tick, delta time.Duration)) {
	if _, found := gameCore.fixedUpdateCallbacks[name]; found {
		log.WithFields(log.Fields{
			"name": name,
		}).Warn("overwriting fixed update callback")
	}
	gameCore.fixedUpdateCallbacks[name] = callback
}

func (gameCore *GameCore) UnregisterFixedUpdateCallback(name string) {
	delete(gameCore.fixedUpdateCallbacks, name)
}

func (gameCore *GameCore) FixedUpdate(tick tick.Tick, delta time.Duration) {
	//    gameCore.simulation.Step(float32(delta.Seconds()))
	for key, _ := range gameCore.fixedUpdateCallbacks {
		gameCore.fixedUpdateCallbacks[key](tick, delta)
	}
}

/*
func (gameCore *GameCore) Node() *core.Node {
	return gameCore.root
}
*/

func (gameCore *GameCore) CreateSnapshot(since tick.Tick) []syncActions.Action {
	var snapshot []syncActions.Action
	gameCore.entityLock.RLock()
	for _, value := range gameCore.entities {
		snapshot = append(snapshot, &syncActions.AddEntity{
			EntityID:     value.id,
			DefinitionID: value.defID,
			Position:     mapping.Position(value.GetNode().Position()),
			Rotation:     value.GetNode().Rotation(),
		})
	}
	gameCore.entityLock.RUnlock()

	gameCore.gridLock.RLock()
	for key, value := range gameCore.grids {
		blockMap := make(map[[3]int]uuid.UUID)
		value.blockLock.RLock()
		for key1, value1 := range value.blocks {
			blockMap[key1.Array()] = value1.BlockID
		}
		value.blockLock.RUnlock()
		snapshot = append(snapshot, &syncActions.AddGrid{
			GridID:   key,
			Position: value.Position(),
			Rotation: value.Rotation(),
			Blocks:   blockMap,
		})
	}
	gameCore.gridLock.RUnlock()

	log.WithFields(log.Fields{
		"snapshotLength": len(snapshot),
	}).Info("snapshot created")
	return snapshot
}

func (gameCore *GameCore) ApplyActions(actions []syncActions.Action) {
	log.WithFields(log.Fields{
		"count": len(actions),
	}).Debug("game-core applying actions")
	var wg sync.WaitGroup
	wg.Add(len(actions))
	for _, value := range actions {
		go func(actionType syncActions.Action) {
			switch action := actionType.(type) {
			case *syncActions.AddEntity:
				entity, err := gameCore.CreateEntityFromDefinition(action.DefinitionID, false)
				if err != nil {
					log.WithFields(log.Fields{
						"definitionID": action.DefinitionID.String(),
					}).Error("failed to load entity from definition: ", err)
					//continue
					return
				}
				entity.id = action.EntityID
				//				vec := math32.Vector3(action.Position)
				entity.SetPositionVec(action.Position)
				entity.SetRotationVec(&action.Rotation)
				gameCore.AddEntity(entity)
			case *syncActions.UpdateEntity:
				entity := gameCore.GetEntity(action.ID)
				if entity == nil {
					log.WithFields(log.Fields{
						"entityID": action.ID.String(),
					}).Error("failed to get entity for update")
					return
				}
				entity.SetPositionVec(action.Position)
				entity.SetRotationVec(&action.Rotation)
			case *syncActions.AddGrid:
				log.WithFields(log.Fields{
					"gridID": action.GridID.String(),
				}).Debug("adding grid to core")
				gameCore.gridLock.Lock()
				gridNode := new(core.Node)
				gameCore.grids[action.GridID] = &Grid{
					id:     action.GridID,
					blocks: make(map[GridPos]*Block),
					Node:   gridNode,
				}
				gridNode.Init(gameCore.grids[action.GridID])
				gameCore.Add(gameCore.grids[action.GridID])
				gameCore.grids[action.GridID].SetPositionVec(&action.Position)
				gameCore.grids[action.GridID].SetRotationVec(&action.Rotation)
				gameCore.gridLock.Unlock()
				log.WithFields(log.Fields{
					"gridID":     action.GridID.String(),
					"position":   action.Position,
					"rotation":   action.Rotation,
					"blockCount": len(action.Blocks),
				}).Debug("grid successfully added")

				for key, value := range action.Blocks {
					gameCore.PlaceOnGrid(value, action.GridID, key)
				}
			case *syncActions.PlaceOnGrid:
				gameCore.PlaceOnGrid(action.BlockID, action.GridID, action.Position)
			case *syncActions.RemoveGrid:
				gameCore.gridLock.Lock()
				if _, found := gameCore.grids[action.GridID]; !found {
					gameCore.gridLock.Unlock()
					return
				}
				gameCore.Remove(gameCore.grids[action.GridID])
				delete(gameCore.grids, action.GridID)
				gameCore.gridLock.Unlock()
			case *syncActions.RemoveBlock:
				gameCore.gridLock.Lock()
				if _, found := gameCore.grids[action.GridID]; !found {
					log.WithFields(log.Fields{
						"gridID": action.GridID.String(),
					}).Error("unable to remove block. not found")
					gameCore.gridLock.Unlock()
					return
				}
				newGridPos := GridPos{
					X: action.Pos[0],
					Y: action.Pos[1],
					Z: action.Pos[2],
				}
				gameCore.grids[action.GridID].blockLock.Lock()
				if _, found := gameCore.grids[action.GridID].blocks[newGridPos]; !found {
					log.WithFields(log.Fields{
						"gridID":  action.GridID.String(),
						"GridPos": newGridPos,
					}).Error("unable to remove block. not found")
					gameCore.grids[action.GridID].blockLock.Unlock()
					gameCore.gridLock.Unlock()
					return
				}
				gameCore.grids[action.GridID].Remove(gameCore.grids[action.GridID].blocks[newGridPos])
				delete(gameCore.grids[action.GridID].blocks, newGridPos)
				gameCore.grids[action.GridID].blockLock.Unlock()
				gameCore.gridLock.Unlock()

			default:
				log.WithFields(log.Fields{
					"type": reflect.TypeOf(action),
				}).Error("unknown action type")
			}
			wg.Done()
		}(value)
	}
	wg.Wait()
	log.WithFields(log.Fields{
		"count": len(actions),
		//		"entityCount": len(gameCore.entities),
	}).Info("successfully applied snapshot")
}

func (gameCore *GameCore) PlaceOnGrid(blockID uuid.UUID, gridID uuid.UUID, position [3]int) {
	log.WithFields(log.Fields{
		"blockID":  blockID.String(),
		"gridID":   gridID.String(),
		"position": position,
	}).Debug("placing block on grid")
	entity, err := gameCore.CreateEntityFromDefinition(blockID, false)
	if err != nil {
		log.WithFields(log.Fields{
			"definitionID": blockID.String(),
		}).Error("failed to load entity from definition: ", err)
		return
	}

	gameCore.gridLock.Lock()
	if _, found := gameCore.grids[gridID]; !found {
		gameCore.gridLock.Unlock()
		log.WithFields(log.Fields{
			"gridID":   gridID.String(),
			"blockID":  blockID.String(),
			"position": position,
		}).Error("failed to place block on grid. grid not found")
		return
	}
	newGridPos := GridPos{
		X: position[0],
		Y: position[1],
		Z: position[2],
	}
	blockNode := new(core.Node)
	newBlock := &Block{
		BlockID: blockID,
		GridPos: newGridPos,
		Entity:  entity,
		Node:    blockNode,
	}
	blockNode.Init(newBlock)
	blockNode.Add(entity)
	gameCore.grids[gridID].Add(newBlock)
	//	gridVec := gameCore.grids[gridID].Position()
	newBlock.SetPosition(
		float32((BLOCK_SIZE * position[0])),
		float32((BLOCK_SIZE * position[1])),
		float32((BLOCK_SIZE * position[2])),
		//		gridVec.X+float32((BLOCK_SIZE*position[0])),
		//		gridVec.Y+float32((BLOCK_SIZE*position[1])),
		//		gridVec.Z+float32((BLOCK_SIZE*position[2])),
	)
	gameCore.grids[gridID].blockLock.Lock()
	gameCore.grids[gridID].blocks[newGridPos] = newBlock
	gameCore.grids[gridID].blockLock.Unlock()
	gameCore.gridLock.Unlock()
	log.WithFields(log.Fields{
		"blockID":  blockID.String(),
		"gridID":   gridID.String(),
		"position": position,
	}).Debug("successfully added block to grid")
}

func (gameCore *GameCore) AddEntity(entity *Entity) {
	log.WithFields(log.Fields{
		"entityID": entity.id.String(),
	}).Debug("adding entity to game core")
	gameCore.entityLock.Lock()
	gameCore.entities[entity.id] = entity
	gameCore.entityLock.Unlock()
	gameCore.Add(entity)
}

func (gameCore *GameCore) RemoveEntity(entity *Entity) {
	gameCore.entityLock.Lock()
	delete(gameCore.entities, entity.ID())
	gameCore.entityLock.Unlock()
	gameCore.Remove(entity)
}

func (gameCore *GameCore) CreateEntityFromDefinition(defID uuid.UUID, unique bool) (*Entity, error) {
	var definition *EntityDefinition
	var found bool
	//TODO: Make this not block for every new definition
	gameCore.entityDefLock.Lock()
	if definition, found = gameCore.entityDefinitions[defID]; !found {
		var err error
		log.WithFields(log.Fields{
			"definitionID": defID.String(),
		}).Debug("entity definition not stored, fetching.")
		definition, err = gameCore.fetcher.GetEntityDefinition(defID)
		if err != nil {
			gameCore.entityDefLock.Unlock()
			return nil, errors.New("failed to fetch entity definition")
		}
		gameCore.entityDefinitions[defID] = definition
	}
	gameCore.entityDefLock.Unlock()

	var loadList []uuid.UUID
	for geomID, mats := range definition.Geometries {
		loadList = append(loadList, geomID)
		for _, value := range mats {
			loadList = append(loadList, value)
		}
	}

	if err := gameCore.assMan.LoadAssets(loadList); err != nil {
		return nil, err
	}

	newNode := new(core.Node)
	newEntity := &Entity{Node: newNode, defID: defID}
	newNode.Init(newEntity)
	for geomID, mats := range definition.Geometries {
		geom, err := gameCore.assMan.GetGeom(geomID, unique)
		if err != nil {
			return nil, err
		}

		var mesh *graphic.Mesh
		if len(mats) == 0 {
			return nil, errors.New("no materials provided for geometry")
		} else if len(mats) == 1 {
			mat, err := gameCore.assMan.GetMat(mats[0], unique)
			if err != nil {
				return nil, err
			}
			mesh = graphic.NewMesh(geom, mat)
			newNode.Add(mesh)
		} else {
			mesh = graphic.NewMesh(geom, nil)
			for key, _ := range mats {
				mat, err := gameCore.assMan.GetMat(mats[key], unique)
				if err != nil {
					return nil, err
				}
				mesh.AddGroupMaterial(mat, key)
			}
			newNode.Add(mesh)
		}
	}
	if definition.Scale != nil {
		newNode.SetScaleVec(definition.Scale)
	}

	return newEntity, nil
}

func (gameCore *GameCore) MainLoop() {
	var frameAvg time.Duration
	//        go logStats(&frameAvg)
	var thisFrame time.Time
	var currentTick tick.Tick
	var sleepTime time.Duration
	var lastTick tick.Tick
	var delta time.Duration
	log.Info("gamecore mainloop starting")

	for {
		if gameCore.shouldStop {
			break
		}
		delta = time.Since(thisFrame)
		thisFrame = time.Now()
		currentTick = tick.CurrentTick()
		if lastTick < currentTick {
			if currentTick > (lastTick+1) && lastTick != 0 {
				log.WithFields(log.Fields{
					"missedTicks": currentTick - lastTick,
					"last":        lastTick,
					"current":     currentTick,
					"delta":       delta,
					"avg":         frameAvg,
				}).Info("missed tick")
			}
			gameCore.FixedUpdate(currentTick, delta)
			frameAvg = (frameAvg + time.Since(thisFrame)) / 2
			lastTick = currentTick
		} else {
			sleepTime = currentTick.TimeTillNextTick()
			time.Sleep(sleepTime)
			continue
		}
	}
}

func (gameCore *GameCore) Shutdown() {
	gameCore.shouldStop = true
}
