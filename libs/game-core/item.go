package gameCore

import (
	"gitlab.com/gamerscomplete/uuid"
)

type ItemAttribute uint16

const (
	UNKNOWN ItemAttribute = 0 << iota
	PLACEABLE
)

type ItemDefinition struct {
	Name               string
	Description        string
	IconTexPackage     uuid.UUID
	EntityDefinitionID uuid.UUID
}

type Item struct {
	Name          string
	Description   string
	ID            uuid.UUID
	IconTextureID uuid.UUID
	EntityID      uuid.UUID
	Attributes    ItemAttribute
}

func (item *Item) HasAttribute(attribute ItemAttribute) bool {
	return item.Attributes&attribute != 0
}

//TODO: Dont know if we want to allow adding attributes here as it needs to be managed to sync
func (item *Item) AddAttribute(attribute ItemAttribute) {
	item.Attributes = item.Attributes | attribute
}

func (item *Item) RemoveAttribute(attribute ItemAttribute) {
	item.Attributes = item.Attributes &^ attribute
}
