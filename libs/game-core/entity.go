package gameCore

import (
	//	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/uuid"
	//	"reflect"
)

type Entity struct {
	id    uuid.UUID
	defID uuid.UUID
	*core.Node
	//	*graphic.Mesh
	changed bool
}

type EntityDefinition struct {
	Name string

	Geometries map[uuid.UUID][]uuid.UUID

	Scale *math32.Vector3
	Tags  []string
}

func (entity *Entity) Update() {
}

func (entity *Entity) Changed() bool {
	return entity.changed
}

func (entity *Entity) SetChanged(changed bool) {
	entity.changed = changed
}

func (entity *Entity) ID() uuid.UUID {
	return entity.id
}

func (entity *Entity) SetPosition(x, y, z float32) {
	entity.changed = true
	entity.Node.SetPosition(x, y, z)
}

func (entity *Entity) TranslateOnAxis(direction *math32.Vector3, distance float32) {
	entity.changed = true
	entity.Node.TranslateOnAxis(direction, distance)
}

func (entity *Entity) RotateOnAxis(direction *math32.Vector3, distance float32) {
	entity.changed = true
	entity.Node.RotateOnAxis(direction, distance)
}

func (entity *Entity) SetPositionVec(pos mapping.Position) {
	entity.changed = true
	//	log.Debug("setting position on entity through wrapper")
	entity.Node.SetPosition(pos.X, pos.Y, pos.Z)
}

func (entity *Entity) SetOpacity(opacity float32) {
	/*
		imat := entity.GetGraphic().GetMaterial(0)
		if imat == nil {
			log.Error("material 0 of entity is nil")
		}

		type matI interface {
			//		AmbientColor() math32.Color
			SetOpacity(float32)
		}

		if v, ok := imat.(matI); ok {
			v.SetOpacity(opacity)
		} else {
			log.WithFields(log.Fields{
				"imatType": reflect.TypeOf(imat),
			}).Error("failed to assert entity to material for SetOpacity")
		}
	*/
}

func (entity *Entity) SetColor(color math32.Color) {
	/*
		imat := entity.GetGraphic().GetMaterial(0)

		type matI interface {
			//		AmbientColor() math32.Color
			SetColor(*math32.Color)
		}

		if v, ok := imat.(matI); ok {
			v.SetColor(&color)
		} else {
			log.Error("failed to assert entity to material for SetColor")
		}
	*/
}
