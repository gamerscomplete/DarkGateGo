//TODO: This whole blockid mapping belong to a grid is stupid, this should be a global level lookup that the server and the client sync/update

package gameCore

import (
	"gitlab.com/g3n/engine/core"
	//	"gitlab.com/g3n/engine/graphic"
	//	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/uuid"
	"sync"
)

type GridPos struct {
	X, Y, Z int
}

func (gridPos *GridPos) Array() [3]int {
	return [3]int{gridPos.X, gridPos.Y, gridPos.Z}
}

type Block struct {
	*core.Node
	BlockID uuid.UUID
	GridPos GridPos
	Entity  *Entity
}

type Grid struct {
	id uuid.UUID
	*core.Node
	blockLock sync.RWMutex
	blocks    map[GridPos]*Block
	//To save from storing a uuid for every block (100k blocks * uuid size is a lot)
	//	blockIDMapping map[int]Item
	//	position       mapping.Position
	//	rotation       math32.Quaternion
	//	idCount        int
	//	idLock         sync.Mutex
}

/*
func NewGrid(pos mapping.Position, rot math32.Quaternion) *Grid {
	grid := Grid{blocks: make(map[GridPos]Block), position: pos, rotation: rot}
	return &grid
}
*/

func (grid *Grid) ID() *uuid.UUID {
	return &grid.id
}

/*
func NewBlock(id uuid.UUID, pos GridPos) *Block {
	newBlock := Block{Node: core.NewNode(), BlockID: id, GridPos: pos}
	return &newBlock
}
*/

func (grid *Grid) Blocks() []*Block {
	var blocks []*Block
	for _, value := range grid.blocks {
		blocks = append(blocks, value)
	}
	return blocks
}

/*
func (grid *Grid) AddBlock(item Item, pos GridPos) {
	var found bool
	var blockID int
	grid.blockLock.RLock()
	for key, value := range grid.blockIDMapping {
		if value.ID == item.ID {
			found = true
			blockID = key
			break
		}
	}
	grid.blockLock.RUnlock()
	if !found {
		grid.idLock.Lock()
		blockID = grid.idCount
		grid.idLock.Unlock()
		grid.blockLock.Lock()
		grid.blockIDMapping[blockID] = item
		grid.blockLock.Unlock()
	}

	grid.blockLock.Lock()
	newBlock := NewBlock(blockID, pos)
	grid.blocks[pos] = *newBlock
	grid.blockLock.Unlock()
}
*/
