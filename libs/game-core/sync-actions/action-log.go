package syncActions

import (
	"gitlab.com/gamerscomplete/DarkGateGo/libs/tick"
	"sync"
)

type ActionLog struct {
	//TODO: Exporting this field so that I can use it in game-core, but should have a better way of accessing this to ensure race conditions dont fuck shit up
	Actions    map[tick.Tick][]Action
	oldestTick tick.Tick
	sync.RWMutex
}

func NewActionLog() *ActionLog {
	return &ActionLog{Actions: make(map[tick.Tick][]Action), oldestTick: tick.CurrentTick()}
}

func (actionLog *ActionLog) OldestTick() tick.Tick {
	return actionLog.oldestTick
}

func (actionLog *ActionLog) AddAction(action Action) {
	currentTick := tick.CurrentTick()
	actionLog.Lock()
	actionLog.Actions[currentTick] = append(actionLog.Actions[currentTick], action)
	actionLog.Unlock()
}

//TODO: This should probably just zero rather than create new so other stuff currently running doesnt get fucked
func (actionLog *ActionLog) Reset() {
	actionLog = NewActionLog()
}
