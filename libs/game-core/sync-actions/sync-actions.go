package syncActions

import (
	"encoding/gob"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/uuid"
)

type Action interface{}

type AddGrid struct {
	GridID   uuid.UUID
	Position math32.Vector3
	Rotation math32.Vector3
	Blocks   map[[3]int]uuid.UUID
}

type PlaceOnGrid struct {
	GridID   uuid.UUID
	BlockID  uuid.UUID
	Position [3]int
}

type RemoveGrid struct {
	GridID uuid.UUID
}

type RemoveBlock struct {
	GridID uuid.UUID
	Pos    [3]int
}

type AddEntity struct {
	Position mapping.Position
	Rotation math32.Vector3
	EntityID uuid.UUID
	//TODO: we should drop the definition ID on this all together and move to a system of fetching the definition by entity ID rather than straight definition ID. This would save a lot of data transfered
	DefinitionID uuid.UUID
}

type RemoveEntity struct {
	ID *uuid.UUID
}

type UpdateEntity struct {
	ID       uuid.UUID
	Position mapping.Position
	Rotation math32.Vector3
}

type AddPlayer struct {
	ID *uuid.UUID
	//Define as an array instead of a slice to keep from blowing up the encoder without specializing the encoder
	Username string
	Health   uint16
	EntityID *uuid.UUID
}

type UpdatePlayer struct {
	ID     *uuid.UUID
	Health uint16
}

type RemovePlayer struct {
	ID *uuid.UUID
}

func init() {
	gob.Register(&AddEntity{})
	gob.Register(&RemoveEntity{})
	gob.Register(&UpdateEntity{})
	gob.Register(&AddPlayer{})
	gob.Register(&RemovePlayer{})
	gob.Register(&UpdatePlayer{})
	gob.Register(&PlaceOnGrid{})
	gob.Register(&AddGrid{})
	gob.Register(&RemoveGrid{})
	gob.Register(&RemoveBlock{})

}
