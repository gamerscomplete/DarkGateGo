//TODO: protect the textures, materials, geometries, meshes maps

package assetManager

import (
	"encoding/gob"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/geometry"
	"gitlab.com/g3n/engine/graphic"
	"gitlab.com/g3n/engine/material"
	"gitlab.com/g3n/engine/texture"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	"gitlab.com/gamerscomplete/uuid"
	"os"
	//	"io/ioutil"
	//	"path/filepath"
	"sync"
)

type fetcherCallback func(*uuid.UUID) (assetPackage.IAssetPackage, error)

type AssetManager struct {
	assets    map[uuid.UUID]assetPackage.IAssetPackage
	assetLock sync.RWMutex

	callback         fetcherCallback
	storageDirectory string

	pendingAssets map[uuid.UUID]chan struct{}
	pendingLock   sync.RWMutex

	textures   map[uuid.UUID]*texture.Texture2D
	texLock    sync.RWMutex
	materials  map[uuid.UUID]material.IMaterial
	matLock    sync.RWMutex
	geometries map[uuid.UUID]geometry.IGeometry
	geomLock   sync.RWMutex
	meshes     map[uuid.UUID]*graphic.Mesh
	meshLock   sync.RWMutex
}

func NewAssetManager(fetcher fetcherCallback, storageDir string) *AssetManager {
	info, err := os.Stat(storageDir)
	if os.IsNotExist(err) {
		if err := os.Mkdir(storageDir, os.FileMode(0755)); err != nil {
			log.WithFields(log.Fields{
				"directory": storageDir,
			}).Fatal("failed to create asset storage directory")
			return nil
		} else {
			log.WithFields(log.Fields{
				"directory": storageDir,
			}).Info("asset storage directory created")
		}
	} else if err != nil {
		log.Panic("failed to stat asset directory: ", err)
	} else {
		if !info.IsDir() {
			log.WithFields(log.Fields{
				"directory": storageDir,
			}).Fatal("asset directory path already exists and is not a directory")
			return nil
		}
	}

	return &AssetManager{
		callback:         fetcher,
		assets:           make(map[uuid.UUID]assetPackage.IAssetPackage),
		pendingAssets:    make(map[uuid.UUID]chan struct{}),
		storageDirectory: storageDir,
		textures:         make(map[uuid.UUID]*texture.Texture2D),
		meshes:           make(map[uuid.UUID]*graphic.Mesh),
		materials:        make(map[uuid.UUID]material.IMaterial),
		geometries:       make(map[uuid.UUID]geometry.IGeometry),
	}
}

func (assetManager *AssetManager) haveAssetOnDisk(id uuid.UUID) bool {
	info, err := os.Stat(assetManager.storageDirectory + "/" + id.String() + ".asset")
	if os.IsNotExist(err) {
		return false
	} else if err != nil {
		log.Error("failed to stat asset: ", err)
	}

	return !info.IsDir()
}

func (assetManager *AssetManager) writeAsset(asset assetPackage.IAssetPackage, id uuid.UUID) error {
	dataFile, err := os.Create(assetManager.storageDirectory + "/" + id.String() + ".asset")
	if err != nil {
		return err
	}
	defer dataFile.Close()

	dataEncoder := gob.NewEncoder(dataFile)
	if err := dataEncoder.Encode(&asset); err != nil {
		return err
	}
	return nil
}

func (assetManager *AssetManager) loadAssetFromDisk(id uuid.UUID) error {
	log.WithFields(log.Fields{
		"assetID": id.String(),
	}).Debug("loading asset from disk")
	assetFile, err := os.Open(assetManager.storageDirectory + "/" + id.String() + ".asset")
	if err != nil {
		return err
	}
	defer assetFile.Close()

	var asset assetPackage.IAssetPackage
	decoder := gob.NewDecoder(assetFile)
	if err := decoder.Decode(&asset); err != nil {
		return err
	}

	assetManager.assetLock.Lock()
	assetManager.assets[id] = asset
	assetManager.assetLock.Unlock()
	return nil
}

/*
func (assetManager *AssetManager) GetMesh(id uuid.UUID) (*graphic.Mesh, error) {
	assetManager.meshLock.RLock()
	defer assetManager.meshLock.RUnlock()
	if mesh, found := assetManager.meshes[id]; found {
		return mesh, nil
	}
	//Dont have the mesh loaded, fetch it, store it, return it
	//TODO: Implement fetching. For now well just error
	return nil, errors.New("Mesh " + id.String() + " not loaded")
}
*/

func (manager *AssetManager) GetTexture(id uuid.UUID, unique bool) (*texture.Texture2D, error) {

	if !unique {
		manager.texLock.RLock()
		if tex, haveTexture := manager.textures[id]; haveTexture {
			manager.texLock.RUnlock()
			return tex, nil
		}
		manager.texLock.RUnlock()
	}

	asset, err := manager.GetAsset(id)
	if err != nil {
		return nil, err
	}

	texPackage, success := asset.(*assetPackage.TexturePackage)
	if !success {
		return nil, errors.New("Asset package is not a texture")
	}

	if unique {
		return texPackage.Texture(), nil
	}

	manager.texLock.Lock()
	defer manager.texLock.Unlock()

	manager.textures[id] = texPackage.Texture()
	return manager.textures[id], nil
}

func (manager *AssetManager) GetMat(id uuid.UUID, unique bool) (material.IMaterial, error) {
	if !unique {
		manager.matLock.RLock()
		if mat, found := manager.materials[id]; found {
			manager.matLock.RUnlock()
			return mat, nil
		}
		manager.matLock.RUnlock()
	}

	asset, err := manager.GetAsset(id)
	if err != nil {
		return nil, err
	}

	matPackage, success := asset.(*assetPackage.MaterialPackage)
	if !success {
		return nil, errors.New("Asset package is not a material")
	}

	if unique {
		return matPackage.Material(), nil
	}

	manager.matLock.Lock()
	defer manager.matLock.Unlock()

	manager.materials[id] = matPackage.Material()
	return manager.materials[id], nil
}

func (manager *AssetManager) GetGeom(id uuid.UUID, unique bool) (geometry.IGeometry, error) {
	if !unique {
		manager.geomLock.RLock()
		if geom, found := manager.geometries[id]; found {
			manager.geomLock.RUnlock()
			return geom, nil
		}
		manager.geomLock.RUnlock()
	}

	asset, err := manager.GetAsset(id)
	if err != nil {
		return nil, err
	}

	geomPackage, success := asset.(*assetPackage.GeometryPackage)
	if !success {
		return nil, errors.New("Asset package is not a geometry")
	}

	if unique {
		return geomPackage.Geometry(), nil
	}

	manager.geomLock.Lock()
	defer manager.geomLock.Unlock()
	manager.geometries[id] = geomPackage.Geometry()
	return manager.geometries[id], nil
}

func (manager *AssetManager) GetAsset(id uuid.UUID) (assetPackage.IAssetPackage, error) {
	manager.assetLock.RLock()
	if asset, found := manager.assets[id]; found {
		manager.assetLock.RUnlock()
		return asset, nil
	}
	manager.assetLock.RUnlock()

	//We didnt have the asset locally. Fetch it instead

	err := manager.LoadAsset(id)
	if err != nil {
		return nil, err
	}

	manager.assetLock.RLock()
	if asset, found := manager.assets[id]; found {
		manager.assetLock.RUnlock()
		return asset, nil
	}
	manager.assetLock.RUnlock()
	return nil, errors.New("asset not found")
}

func (manager *AssetManager) PendingAsset(id uuid.UUID) bool {
	manager.pendingLock.RLock()
	defer manager.pendingLock.RUnlock()
	if _, found := manager.pendingAssets[id]; found {
		return true
	}
	return false
}

func (manager *AssetManager) HaveAsset(id uuid.UUID) bool {
	manager.assetLock.RLock()
	_, found := manager.assets[id]
	manager.assetLock.RUnlock()
	return found
}

//LoadAssets is primarily used as a pretch to ensure the specified assets are available on a \
// fetch call to reduce latency of fetch if we know what we need ahead of time. Set blocking if you want to ensure the assets are fetched before moving on
func (manager *AssetManager) LoadAssets(assets []uuid.UUID) error {
	for _, value := range assets {
		if err := manager.LoadAsset(value); err != nil {
			return err
		}
	}
	return nil
}

//LoadAsset is used to prefetch an asset to ensure it is available when a fetch is called to reduce latency
//TODO: This function can currently "lose" pending asset requests during fetching if it fails to fetch or store the file
//	since subsequent requests for the same file would likely fail as well, just releasing the pending wait and having the next request fail isnt a good solution either
func (manager *AssetManager) LoadAsset(assetID uuid.UUID) error {
	if assetID == uuid.Zero {
		return errors.New("Attempted to load zero ID asset")
	}

	manager.assetLock.RLock()
	_, found := manager.assets[assetID]
	if found {
		manager.assetLock.RUnlock()
		return nil
	}
	manager.assetLock.RUnlock()

	manager.pendingLock.Lock()
	if finishChan, found := manager.pendingAssets[assetID]; found {
		//already pending, wait for it to finish
		manager.pendingLock.Unlock()
		<-finishChan
		return nil
	}
	//not pending, add to pending and kick off the loading
	finishChan := make(chan struct{})
	manager.pendingAssets[assetID] = finishChan
	manager.pendingLock.Unlock()

	var asset assetPackage.IAssetPackage
	if manager.haveAssetOnDisk(assetID) {
		if err := manager.loadAssetFromDisk(assetID); err != nil {
			return err
		}
		manager.pendingLock.Lock()
		delete(manager.pendingAssets, assetID)
		manager.pendingLock.Unlock()
		close(finishChan)
		return nil
	}

	log.WithFields(log.Fields{
		"assetID": assetID.String(),
	}).Debug("fetching asset")

	asset, err := manager.callback(&assetID)
	if err != nil {
		//Log this error. Since we may not be blocking this error cannot be returned
		log.WithFields(log.Fields{
			"assetID": assetID.String(),
		}).Error("Failed to fetch asset from callback:", err)
		return err
	}

	if err := manager.writeAsset(asset, assetID); err != nil {
		return err
	}

	manager.assetLock.Lock()
	manager.assets[assetID] = asset
	manager.assetLock.Unlock()

	manager.pendingLock.Lock()
	delete(manager.pendingAssets, assetID)
	manager.pendingLock.Unlock()
	close(finishChan)
	return nil
}

/*
func createAssetDB(dir string) error {
	fmt.Println("Creating assetDB")
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return err
	}

	var assetList []uuid.UUID
	for _, file := range files {
		extension := filepath.Ext(file.Name())
		if extension == ".asset" {
			//This is an asset
			//convert filename to uuid
			assetID, err := uuid.ParseHex(file.Name())
			if err != nil {
				fmt.Println("Failed to convert asset filename to uuid")
				continue
			}
			assetList = append(assetList, *assetID)
		} else {
			fmt.Println("Invalid file type:", extension)
		}
	}
	//	_assetDB = assetList
	return nil
}
*/
