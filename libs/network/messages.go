package network

import (
	"encoding/gob"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/uuid"
)

type Message interface {
}

type DeadConn struct{}

type ApplyActions struct {
	Actions []syncActions.Action
}

type SyncPosition struct {
	Position mapping.Position
	Rotation math32.Vector3
}

type TransitMessage struct {
	ID      uint32
	Reply   bool
	Command string
	Message
}

type AuthMessage struct {
	Token string
}

type AuthReply struct {
	Success      bool
	PlayerEntity uuid.UUID
	Snapshot     []syncActions.Action
	Inventory    map[uuid.UUID]int
}

type CreateGrid struct {
	Position math32.Vector3
	Rotation math32.Vector3
	BlockID  uuid.UUID
}

type RemoveBlock struct {
	GridID   uuid.UUID
	Position [3]int
}

type PlaceOnGrid struct {
	GridID   uuid.UUID
	BlockID  uuid.UUID
	Position [3]int
}

func init() {
	gob.Register(&SyncPosition{})
	gob.Register(&AuthMessage{})
	gob.Register(&AuthReply{})
	gob.Register(&PlaceOnGrid{})
	gob.Register(&CreateGrid{})
	gob.Register(&RemoveBlock{})
	gob.Register(&ApplyActions{})
	gob.Register(&DeadConn{})
}

func (message *TransitMessage) MessageID() uint32 {
	return message.ID
}
