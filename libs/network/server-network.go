package network

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"net"
	"strconv"
	"syscall"
)

type ServerNetwork struct {
	clients      []*Client
	shutdownChan chan struct{}
}

type Client struct {
	*NetConn
}

func StartServer(port int, messageProcessor func(TransitMessage, *Client)) (*ServerNetwork, error) {
	ln, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	log.Printf("Listen on %s", ln.Addr())

	serverNetwork := NewServerNetwork()

	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				log.Fatalf("failed to accept: %v", err)
				continue
			}
			log.Printf("Accepted Connection from RemoteAddr: %s", conn.RemoteAddr())
			client := NewClient(conn, messageProcessor)
			//TODO: make this less shitty
			serverNetwork.clients = append(serverNetwork.clients, client)
		}
	}()
	return serverNetwork, nil
}

func NewClient(conn net.Conn, processor func(TransitMessage, *Client)) *Client {
	//TODO: make this less hacky
	client := &Client{}
	client.NetConn = NewNetConn(
		conn,
		func(message TransitMessage) {
			processor(message, client)
		},
	)

	return client
}

func (serverNetwork *ServerNetwork) Shutdown() {
	close(serverNetwork.shutdownChan)
}

func NewServerNetwork() *ServerNetwork {
	return &ServerNetwork{shutdownChan: make(chan struct{})}
}

func (serverNetwork *ServerNetwork) Broadcast(message Message, blacklistClient *Client) {
	for key, value := range serverNetwork.clients {
		if value == blacklistClient {
			continue
		}
		err := value.SendMessage(message)
		if errors.Is(err, syscall.EPIPE) {
			log.Warn("detected dead pipe from client. removing.")
			value.NetConn.handler(TransitMessage{Message: &DeadConn{}})
			//TODO: something is fucked here because key+1 can be an issue if its on the end
			serverNetwork.clients = append(serverNetwork.clients[:key], serverNetwork.clients[key+1:]...)
		} else if err != nil {
			log.Error("failed to send message to client: ", err)
		}
	}
}
