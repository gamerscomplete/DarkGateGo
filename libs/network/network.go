package network

import (
	"encoding/gob"
	"errors"
	log "github.com/sirupsen/logrus"
	"io"
	"net"
	"reflect"
	"sync"
	"time"
)

type NetConn struct {
	conn         net.Conn
	messagesSent uint32
	jobs         map[uint32]chan Message
	idLock       sync.RWMutex
	messageID    uint32
	encoder      *gob.Encoder
	decoder      *gob.Decoder
	serving      bool
	handler      func(TransitMessage)
}

func NewNetConn(conn net.Conn, handler func(TransitMessage)) *NetConn {
	netConn := NetConn{
		jobs:      make(map[uint32]chan Message),
		messageID: 1,
		conn:      conn,
		encoder:   gob.NewEncoder(conn),
		decoder:   gob.NewDecoder(conn),
		handler:   handler,
	}

	//Launch a listener for the netconn
	go func() {
		for {
			incomingMessage := TransitMessage{}
			err := netConn.decoder.Decode(&incomingMessage)
			log.Debug("received incoming message")
			if err != nil {
				if err == io.EOF {
					log.Info("Connection disconected")
					return
				}
				log.Error("Decode failed:", err)
				continue
			}
			if incomingMessage.Reply {
				if _, found := netConn.jobs[incomingMessage.ID]; !found {
					log.Warn("Recieved reply to untracked job. Discarding")
					continue
				}
				netConn.jobs[incomingMessage.ID] <- incomingMessage.Message
			} else {
				log.Info("dispatching incoming message to handler")
				go handler(incomingMessage)
			}
		}

	}()

	return &netConn
}

//For internal use. Actually sends the message. SendMessage is used downstream which takes just a Message
func (conn *NetConn) sendMessage(message TransitMessage) error {
	//	ppid := 1
	/*	info := &sctp.SndRcvInfo{
			Stream: uint16(ppid),
			PPID:   uint32(conn.messagesSent),
		}
	*/
	log.WithFields(log.Fields{
		"ID":          message.ID,
		"reply":       message.Reply,
		"command":     message.Command,
		"payloadType": reflect.TypeOf(message.Message),
	}).Debug("sending message")

	err := conn.encoder.Encode(message)
	if err != nil {
		return err
	}
	//	_, err = conn.SCTPWrite(data, info)
	conn.messagesSent++
	return err
}

func (conn *NetConn) Reply(tMessage TransitMessage, message Message) error {
	transitMessage := TransitMessage{ID: tMessage.ID, Reply: true, Message: message}
	return conn.sendMessage(transitMessage)
}

func (conn *NetConn) ReplyByID(tID uint32, message Message) error {
	transitMessage := TransitMessage{ID: tID, Reply: true, Message: message}
	return conn.sendMessage(transitMessage)
}

//For one way message sending
func (conn *NetConn) SendMessage(message Message) error {
	//TODO: Less duplication with 2 way messages
	conn.idLock.Lock()
	messageID := conn.messageID
	conn.messageID++
	conn.idLock.Unlock()
	transitMessage := TransitMessage{ID: messageID, Reply: false, Message: message}
	return conn.sendMessage(transitMessage)
}

//Will handle sending a message and recieving the reply with a timeout
func (conn *NetConn) SendTwoWayMessage(message Message, inputMessage *Message, timeout time.Duration) error {
	conn.idLock.Lock()
	messageID := conn.messageID
	conn.messageID++
	conn.idLock.Unlock()

	transitMessage := TransitMessage{ID: messageID, Reply: false, Message: message}

	//Adding the channel to the replies list before sending the message to prevent race conditions
	returnChan := make(chan Message)
	conn.jobs[messageID] = returnChan

	if err := conn.sendMessage(transitMessage); err != nil {
		return err
	}

	select {
	case tempMessage := <-returnChan:
		//	case *inputMessage = <-returnChan:
		*inputMessage = tempMessage
		//		fmt.Println("Recieved message:", tempMessage)
		//got message back
		return nil
	case <-time.After(timeout):
		return errors.New("Timed out waiting for reply")
	}
}

func (conn *NetConn) Conn() net.Conn {
	return conn.conn
}
