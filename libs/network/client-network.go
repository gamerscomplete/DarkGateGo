package network

import (
	log "github.com/sirupsen/logrus"
	"net"
	"strconv"
	"strings"
)

type ClientNetwork struct {
	*NetConn
}

func DialServer(addr string, port int, handler func(TransitMessage)) (*ClientNetwork, error) {
	ips := []net.IPAddr{}

	for _, i := range strings.Split(addr, ",") {
		if a, err := net.ResolveIPAddr("ip", i); err == nil {
			log.Printf("Resolved address '%s' to %s", i, a)
			ips = append(ips, *a)
		} else {
			log.Printf("Error resolving address '%s': %v", i, err)
		}
	}

	conn, err := net.Dial("tcp", addr+":"+strconv.Itoa(port))
	if err != nil {
		log.Fatalf("failed to dial: %v", err)
		return nil, err
	}
	//	log.Printf("Dial LocalAddr: %s; RemoteAddr: %s", conn.LocalAddr(), conn.RemoteAddr())
	return &ClientNetwork{NetConn: NewNetConn(conn, handler)}, nil
	//    ppid := 0
}
