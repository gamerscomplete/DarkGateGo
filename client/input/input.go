package input

import (
	//	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/window"
)

type InputRouter struct {
	capturing bool
	inCapture []window.Key
	captured  chan []window.Key

	//Keys currently pressed down
	downKeys []window.Key

	activeController   *Controller
	dynamicControllers map[string]*Controller
	staticControllers  map[string]*Controller

	locked bool
}

func NewInputRouter(iWindow window.IWindow) *InputRouter {
	router := &InputRouter{
		dynamicControllers: make(map[string]*Controller),
		staticControllers:  make(map[string]*Controller),
		downKeys:           make([]window.Key, 0),
		captured:           make(chan []window.Key, 1),
	}

	iWindow.Subscribe(window.OnKeyDown, router.onKey)
	iWindow.Subscribe(window.OnKeyUp, router.onKey)
	iWindow.Subscribe(window.OnCursor, router.onCursor)
	iWindow.Subscribe(window.OnMouseDown, router.onMouse)
	iWindow.Subscribe(window.OnScroll, router.onScroll)

	return router
}

func (inputRouter *InputRouter) GetBinds() map[string][]Binding {

	allBinds := make(map[string][]Binding)

	for k, v := range inputRouter.dynamicControllers {
		allBinds[k] = v.Bindings
	}

	for k, v := range inputRouter.staticControllers {
		allBinds[k] = v.Bindings
	}

	return allBinds
}

func (inputRouter *InputRouter) Lock() {
	inputRouter.locked = true
}

func (inputRouter *InputRouter) Unlock() {
	inputRouter.locked = false
}

//RegisterController takes the name of the controller, the bindings list with default keys bound,
//	if the controller is static (keys always register) or dynamic (keys only register when dynamic controller is active
func (inputRouter *InputRouter) RegisterController(name string, controller Controller, static bool) {
	if static {
		inputRouter.staticControllers[name] = &controller
	} else {
		if len(inputRouter.dynamicControllers) == 0 {
			inputRouter.activeController = &controller
		}
		inputRouter.dynamicControllers[name] = &controller
	}
}

func (inputRouter *InputRouter) UpdateBind(controller string, bind string, keys []window.Key) {
	if _, found := inputRouter.staticControllers[controller]; found {
		for k, v := range inputRouter.staticControllers[controller].Bindings {
			if v.Name == bind {
				inputRouter.staticControllers[controller].Bindings[k].Keys = keys
				return
			}
		}
	}

	if _, found := inputRouter.dynamicControllers[controller]; found {
		for k, v := range inputRouter.dynamicControllers[controller].Bindings {
			if v.Name == bind {
				inputRouter.dynamicControllers[controller].Bindings[k].Keys = keys
				return
			}
		}
	}

}

func (inputRouter *InputRouter) CaptureInput() []window.Key {
	if inputRouter.capturing {
		log.Warn("attempted to start capturing input when already in capture state")
		return nil
	}

	//TODO: clear current pressed keys and any active bindings
	//	inputRouter.captured = make([]window.Key, 0)
	inputRouter.capturing = true

	capturedKeys := <-inputRouter.captured
	inputRouter.inCapture = make([]window.Key, 0)
	inputRouter.capturing = false
	return capturedKeys
}

func (inputRouter *InputRouter) onKey(evname string, ev interface{}) {
	if inputRouter.locked {
		return
	}
	//Capture the keys instead of parsing them for key bind assignments
	kev := ev.(*window.KeyEvent)

	var state bool
	if evname == window.OnKeyDown {
		state = true
	} else {
		state = false
	}

	if inputRouter.capturing {
		if state {
			//Add
			inputRouter.inCapture = append(inputRouter.inCapture, kev.Key)
		} else {
			//Any keyup ends capture
			inputRouter.captured <- inputRouter.inCapture
		}
		return
	}

	//This is kind of kludgy since it is checking the whole slice everytime regardless of state, but I want detection of double down events to know if keystrokes are getting lost or out of order
	var inDownKeys bool
	for key, _ := range inputRouter.downKeys {
		if inputRouter.downKeys[key] == kev.Key {
			inDownKeys = true
			if state {
				log.WithFields(log.Fields{
					"key": kev.Key,
				}).Error("got down key while key is already in the downkey map")
			} else {
				//remove the key from the slice
				inputRouter.downKeys[key] = inputRouter.downKeys[len(inputRouter.downKeys)-1]
				inputRouter.downKeys[len(inputRouter.downKeys)-1] = 0
				inputRouter.downKeys = inputRouter.downKeys[:len(inputRouter.downKeys)-1]
				break
			}
		}
	}

	if !inDownKeys {
		if state {
			//key is down and not in slice already so add it
			inputRouter.downKeys = append(inputRouter.downKeys, kev.Key)
		}
	}

	var found bool
	//process inputs for static controllers first
	for _, value := range inputRouter.staticControllers {
		if inputRouter.handleInput(kev.Key, state, value) {
			found = true
			break
		}
	}

	if !found && inputRouter.activeController != nil {
		inputRouter.handleInput(kev.Key, state, inputRouter.activeController)
	}
}

//returns true if the input dispatched a callback and false if it did not
func (inputRouter *InputRouter) handleInput(key window.Key, state bool, controller *Controller) bool {
	if binding := controller.hasKey(key); binding != nil {
		if !state && binding.active {
			//any key up belonging to this group cancels the command
			controller.InputCallback(binding.Name, false)
			binding.active = false
			return true
		}
		var complete bool
		//check if rest of sequence is complete
		if len(binding.Keys) == 1 {
			complete = true
		} else {
			for k, _ := range binding.Keys {
				if binding.Keys[k] == key {
					//is current key, move on
					continue
				}
				var found bool
				for k2, _ := range inputRouter.downKeys {
					if inputRouter.downKeys[k2] == binding.Keys[k] {
						found = true
					}
				}
				if !found {
					return false
				}
			}
			complete = true
		}
		if complete {
			controller.InputCallback(binding.Name, state)
			binding.active = state
			return true
		}
	}
	return false
}

func (inputRouter *InputRouter) onCursor(evname string, ev interface{}) {
	if inputRouter.activeController != nil && inputRouter.activeController.CursorCallback != nil {
		inputRouter.activeController.CursorCallback(evname, ev)
	}
}
func (inputRouter *InputRouter) onMouse(evname string, ev interface{}) {
	if inputRouter.activeController != nil && inputRouter.activeController.MouseCallback != nil {
		inputRouter.activeController.MouseCallback(evname, ev)
	}
}
func (inputRouter *InputRouter) onScroll(evname string, ev interface{}) {
	if inputRouter.activeController != nil && inputRouter.activeController.ScrollCallback != nil {
		inputRouter.activeController.ScrollCallback(evname, ev)
	}
}
