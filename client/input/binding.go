package input

import (
	"gitlab.com/g3n/engine/window"
)

type Binding struct {
	Keys   []window.Key
	Name   string
	active bool
}

func (bind *Binding) hasKey(key window.Key) bool {
	for _, v := range bind.Keys {
		if v == key {
			return true
		}
	}
	return false
}
