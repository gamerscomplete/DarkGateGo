package input

import (
	"gitlab.com/g3n/engine/window"
)

type Controller struct {
	Bindings       []Binding
	CursorCallback func(evname string, ev interface{})
	InputCallback  func(command string, state bool)
	MouseCallback  func(evname string, ev interface{})
	ScrollCallback func(evname string, ev interface{})
}

func (controller *Controller) hasKey(key window.Key) *Binding {
	for _, v := range controller.Bindings {
		if v.hasKey(key) {
			return &v
		}
	}
	return nil
}

//func (controller *Controller)
