package main

import (
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
)

func main() {
	var flagUsername string
	var flagPassword string
	var flagLevel string
	flag.StringVar(&flagUsername, "username", "", "Username to use for logging in")
	flag.StringVar(&flagPassword, "password", "", "Password to use for logging in")
	flag.StringVar(&flagLevel, "level", "warn", "logging level")
	flag.Parse()

	level, err := log.ParseLevel(flagLevel)
	if err != nil {
		log.Warn("failed to parse log level from flag. defaulting to warn")
		log.SetLevel(log.WarnLevel)
	} else {
		log.SetLevel(level)
	}

	log.SetFormatter(&log.TextFormatter{
		ForceColors: true,
	})

	game, err := NewGame()
	if err != nil {
		fmt.Println("Failed to create game:", err)
		return
	}

	if flagUsername != "" && flagPassword != "" {
		if loginMessage, success := game.doLogin(flagUsername, flagPassword); !success {
			fmt.Println(loginMessage)
			game.registerUser(flagUsername, flagPassword)
			game.doLogin(flagUsername, flagPassword)
		}
	}

	go game.core.MainLoop()
	game.app.Run()
}
