package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/window"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"strconv"
	"strings"
)

type Chat struct {
	root   *gui.Window
	input  *gui.Edit
	output *gui.Label

	enabled bool

	messagePos    int
	messages      []string
	messageSender func(string) error
	game          *Game
}

func NewChat(messageSender func(string) error, game *Game) *Chat {
	chat := Chat{
		root:          gui.NewWindow(300, 400),
		messages:      make([]string, 5),
		messageSender: messageSender,
		game:          game,
	}
	chat.root.SetLayout(gui.NewFillLayout(true, true))
	chat.root.SetPosition(10, 300)
	chat.root.SetBorders(0, 0, 0, 0)

	chat.output = gui.NewLabel("")
	chat.output.SetPosition(0, 5)
	chat.root.Add(chat.output)

	chat.input = gui.NewEdit(250, "press [enter] to send message")
	chat.input.SetPosition(10, 150)

	chat.root.Add(chat.input)
	chat.input.Subscribe(gui.OnKeyDown, chat.onChatKey)

	chat.input.SetEnabled(false)
	chat.input.SetRenderable(false)

	return &chat
}

func (chat *Chat) Enabled() bool {
	return chat.enabled
}

func (chat *Chat) Enable(enable bool) {
	if chat.enabled == enable {
		return
	}

	chat.enabled = enable

	chat.input.SetRenderable(enable)
	chat.input.SetEnabled(enable)

	if enable {
		gui.Manager().SetKeyFocus(chat.input)
	}
}

func (chat *Chat) appendMessage(username string, message string) {
	if chat.messagePos > cap(chat.messages)-1 {
		chat.messagePos = 0
	}

	chat.messages[chat.messagePos] = username + ": " + message
	chat.messagePos++

	var text string

	if len(chat.messages) < 1 {
		return
	}
	pos := chat.messagePos
	for i := 0; i < len(chat.messages); i++ {
		if pos > cap(chat.messages)-1 {
			pos = 0
		}
		text = text + "\n" + chat.messages[pos]
		pos++
	}
	chat.output.SetText(text)
}

func (chat *Chat) Root() *gui.Window {
	return chat.root
}

func (chat *Chat) onChatKey(evname string, ev interface{}) {
	kev := ev.(*window.KeyEvent)
	switch kev.Key {
	case window.KeyEnter:
		chat.parseInput(chat.input.Text())
		chat.input.SetText("")
	}
}

func (chat *Chat) parseInput(input string) {
	if input == "" {
		return
	}

	if string(input[0]) == "/" {
		chat.parseCommand(input)
		return
	}

	if err := chat.messageSender(input); err != nil {
		log.Error("failed to send chat message: ", err)
	}
}

func (chat *Chat) sendSystemMessage(input string) {
	chat.appendMessage("SYSTEM", input)
}

func (chat *Chat) parseCommand(input string) {
	fields := strings.Fields(string(input[1:]))
	if len(fields) == 0 {
		return
	}

	switch strings.ToLower(fields[0]) {
	case "level":
		level, err := log.ParseLevel(fields[1])
		if err != nil {
			chat.sendSystemMessage("failed to parse log level: " + err.Error())
		} else {
			log.SetLevel(level)
			chat.sendSystemMessage("log level set to " + fields[1])
		}
	case "echo":
		chat.sendSystemMessage(strings.Join(fields[1:], " "))
	case "pos":
		playerEntity := chat.game.core.GetEntity(chat.game.playerEntityID)
		if playerEntity == nil {
			log.Error("chat failed to get player entity from core")
			return
		}
		pos := playerEntity.Position()
		chat.sendSystemMessage("position - " + pos.String())
		/*
			case "tp":
				//TODO: parse the position from the message
				playerEntity := chat.game.core.GetEntity(chat.game.playerEntityID)
				if playerEntity == nil {
					log.Error("chat failed to get player entity from core")
					return
				}
				playerEntity.SetPosition(
		*/
	case "setspawn":
		//if there is any arguements we want to use those for positions
		var spawnPoint mapping.Position
		var spawnRot math32.Vector3
		if len(fields) > 1 {
			if len(fields) != 4 {
				chat.sendSystemMessage("invalid number of arguements. setspawn takes either 0 or 3 arguements. no arguements is current position, otherwise arguements are x y z")
				return
			}
			if x, err := strconv.ParseFloat(fields[1], 32); err != nil {
				chat.sendSystemMessage("setspawn unable to convert X coord: " + err.Error())
				return
			} else {
				spawnPoint.X = float32(x)
			}
			if y, err := strconv.ParseFloat(fields[2], 32); err != nil {
				chat.sendSystemMessage("setspawn unable to convert Y coord: " + err.Error())
				return
			} else {
				spawnPoint.Y = float32(y)
			}
			if z, err := strconv.ParseFloat(fields[3], 32); err != nil {
				chat.sendSystemMessage("setspawn unable to convert Z coord: " + err.Error())
				return
			} else {
				spawnPoint.Z = float32(z)
			}
		} else {
			playerEntity := chat.game.core.GetEntity(chat.game.playerEntityID)
			if playerEntity == nil {
				log.Error("chat failed to get player entity from core")
				return
			}
			spawnPoint = mapping.Position(playerEntity.Position())
			spawnRot = playerEntity.Rotation()
		}

		if success, err := chat.game.network.controlClient.SetSpawn(spawnPoint, spawnRot); err != nil {
			log.Error("failed to set spawn: ", err)
			chat.sendSystemMessage("failed to set spawn: " + err.Error())
			return
		} else if !success {
			chat.sendSystemMessage("setspawn unknown failure")
			return
		}
		chat.sendSystemMessage("spawn successfully set to: " + spawnPoint.String())
	default:
		chat.sendSystemMessage("unknown command - '" + input + "'")
	}
}
