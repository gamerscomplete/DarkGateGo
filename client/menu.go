package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/window"
	"gitlab.com/gamerscomplete/DarkGateGo/client/input"
	"time"
)

const (
	DOUBLE_CLICK_SPEED = time.Second * 1
)

type Menu struct {
	enabled    bool
	rootWindow *gui.Window
	rootNode   *core.Node
	input      *input.InputRouter
	table      *gui.Table

	binding bool
}

func NewMenu(router *input.InputRouter) *Menu {
	width, height := GetApp().GetSize()

	newMenu := &Menu{
		rootWindow: gui.NewWindow(float32(width), float32(height)),
		rootNode:   core.NewNode(),
		input:      router,
	}

	genRows := func() []map[string]interface{} {
		values := make([]map[string]interface{}, 0)

		//Not using the name yet so using _
		for controllerName, value := range newMenu.input.GetBinds() {
			for _, v := range value {
				rval := make(map[string]interface{})
				rval["name"] = v.Name
				rval["keys"] = KeysString(v.Keys)
				rval["controller"] = controllerName
				values = append(values, rval)
			}
		}
		return values
	}

	newMenu.rootNode.Add(newMenu.rootWindow)

	// Creates Table
	//	tableY := mb.Position().Y + mb.Height() + 10
	//	tableY := 10
	tableY := 0
	tableColumns := []gui.TableColumn{
		{Id: "name", Header: "Name", Width: 100, Minwidth: 32, Align: gui.AlignLeft, Format: "%s", Resize: false, Expand: 0},
		{Id: "keys", Header: "Bound", Width: 100, Minwidth: 32, Align: gui.AlignCenter, Format: "%s", Resize: true, Expand: 1},
		{Id: "controller", Hidden: true},
	}
	tab, err := gui.NewTable(float32(width), float32(height), tableColumns)
	if err != nil {
		panic(err)

	}
	// Sets the table data
	rows := genRows()
	tab.SetRows(rows)
	tab.SetBorders(1, 1, 1, 1)
	tab.SetPosition(0, float32(tableY))
	tab.SetMargins(10, 10, 10, 10)
	newMenu.rootWindow.Add(tab)
	newMenu.table = tab
	/*
	   // Resize table
	   a.DemoPanel().SubscribeID(gui.OnResize, a, func(evname string, ev interface{}) {
	       tab.SetSize(a.DemoPanel().ContentWidth(), a.DemoPanel().ContentHeight()-tableY)
	   })
	*/
	newMenu.disable()

	type rowClick struct {
		time time.Time
		row  int
	}

	var lastClick rowClick

	var tce gui.TableClickEvent
	tab.Subscribe(gui.OnTableClick, func(evname string, ev interface{}) {
		tce = ev.(gui.TableClickEvent)
		if tce.Button == window.MouseButtonRight || tce.Header || newMenu.binding {
			return
		}

		//This should always be true, demo had it checking though so leaving the check
		if tce.Row >= 0 {
			if !time.Now().After(lastClick.time.Add(DOUBLE_CLICK_SPEED)) && !newMenu.binding && tce.Row == lastClick.row {
				row := tab.Row(tce.Row)
				if _, found := row["controller"]; !found {
					log.Error("couldnt find controller column")
					return
				}

				newMenu.binding = true
				handleFunc := func() {
					captured := newMenu.input.CaptureInput()
					lastClick.row = -1
					newMenu.binding = false
					//Update the input to have the new bind
					//					controllerName := row["controller"].(string)
					newMenu.input.UpdateBind(row["controller"].(string), row["name"].(string), captured)
					//update the row to reflect the changed bind
					tab.SetCell(tce.Row, "keys", KeysString(captured))
				}
				go handleFunc()
			} else {
				lastClick.time = time.Now()
				lastClick.row = tce.Row
			}
			return
		}
	})
	return newMenu
}

func (menu *Menu) Toggle() {
	if menu.enabled {
		menu.Enable(false)
	} else {
		menu.Enable(true)
	}
}

func (menu *Menu) Enable(state bool) {
	if state {
		menu.enable()
	} else {
		menu.disable()
	}
}

func (menu *Menu) Enabled() bool {
	return menu.enabled
}

func (menu *Menu) enable() {
	menu.enabled = true
	menu.rootWindow.SetEnabled(true)
	menu.rootWindow.SetRenderable(true)
	//	menu.table.SetRenderable(true)
	//	menu.table.SetEnabled(true)
}

func (menu *Menu) disable() {
	menu.enabled = false
	menu.rootWindow.SetEnabled(false)
	menu.rootWindow.SetRenderable(false)
	//	menu.table.SetRenderable(false)
	//	menu.table.SetEnabled(false)
}

func (menu *Menu) Node() *core.Node {
	return menu.rootNode
}

func KeysString(keys []window.Key) string {
	var outString string

	//	keysLength := len(keys)
	for k, _ := range keys {
		//		if keysLength - 1 != k {
		if len(outString) == 0 {
			outString = KeyString(keys[k])
		} else {
			outString = outString + ", " + KeyString(keys[k])
		}
	}
	return outString
}

func KeyString(key window.Key) string {
	switch key {
	case window.KeyA:
		return "a"
	case window.KeyB:
		return "b"
	case window.KeyC:
		return "c"
	case window.KeyD:
		return "d"
	case window.KeyE:
		return "e"
	case window.KeyF:
		return "f"
	case window.KeyG:
		return "g"
	case window.KeyH:
		return "h"
	case window.KeyI:
		return "i"
	case window.KeyJ:
		return "J"
	case window.KeyK:
		return "k"
	case window.KeyL:
		return "l"
	case window.KeyM:
		return "m"
	case window.KeyN:
		return "n"
	case window.KeyO:
		return "o"
	case window.KeyP:
		return "p"
	case window.KeyQ:
		return "q"
	case window.KeyR:
		return "r"
	case window.KeyS:
		return "s"
	case window.KeyT:
		return "t"
	case window.KeyU:
		return "u"
	case window.KeyV:
		return "v"
	case window.KeyW:
		return "w"
	case window.KeyX:
		return "x"
	case window.KeyY:
		return "y"
	case window.KeyZ:
		return "z"
	case window.KeyEscape:
		return "escape"
	case window.KeySpace:
		return "space"
	case window.KeyApostrophe:
		return "'"
	case window.KeyComma:
		return ","
	case window.KeyMinus:
		return "-"
	case window.KeyPeriod:
		return "."
	case window.KeySlash:
		return "/"
	case window.KeySemicolon:
		return ";"
	case window.KeyEqual:
		return "="
	case window.KeyLeftBracket:
		return "["
	case window.KeyRightBracket:
		return "]"
	case window.KeyBackslash:
		return "\\"
	case window.KeyGraveAccent:
		return "`"
	case window.KeyEnter:
		return "enter"
	case window.KeyTab:
		return "tab"
	case window.KeyBackspace:
		return "backspace"
	case window.KeyInsert:
		return "insert"
	case window.KeyDelete:
		return "delete"
	case window.KeyRight:
		return "right"
	case window.KeyLeft:
		return "left"
	case window.KeyUp:
		return "up"
	case window.KeyDown:
		return "down"
	case window.KeyPageUp:
		return "page up"
	case window.KeyPageDown:
		return "page down"
	case window.KeyHome:
		return "home"
	case window.KeyEnd:
		return "end"
	case window.KeyCapsLock:
		return "caps lock"
	case window.KeyScrollLock:
		return "scroll lock"
	case window.KeyNumLock:
		return "num lock"
	case window.KeyPrintScreen:
		return "print screen"
	case window.KeyPause:
		return "pause"
	case window.Key0:
		return "0"
	case window.Key1:
		return "1"
	case window.Key2:
		return "2"
	case window.Key3:
		return "3"
	case window.Key4:
		return "4"
	case window.Key5:
		return "5"
	case window.Key6:
		return "6"
	case window.Key7:
		return "7"
	case window.Key8:
		return "8"
	case window.Key9:
		return "9"
	case window.KeyF1:
		return "f1"
	case window.KeyF2:
		return "f2"
	case window.KeyF3:
		return "f3"
	case window.KeyF4:
		return "f4"
	case window.KeyF5:
		return "f5"
	case window.KeyF6:
		return "f6"
	case window.KeyF7:
		return "f7"
	case window.KeyF8:
		return "f8"
	case window.KeyF9:
		return "f9"
	case window.KeyF10:
		return "f10"
	case window.KeyF11:
		return "f11"
	case window.KeyF12:
		return "f12"
	case window.KeyF13:
		return "f13"
	case window.KeyF14:
		return "f14"
	case window.KeyF15:
		return "f15"
	case window.KeyF16:
		return "f16"
	case window.KeyF17:
		return "f17"
	case window.KeyF18:
		return "f18"
	case window.KeyF19:
		return "f19"
	case window.KeyF20:
		return "f20"
	case window.KeyF21:
		return "f21"
	case window.KeyF22:
		return "f22"
	case window.KeyF23:
		return "f23"
	case window.KeyF24:
		return "f24"
	case window.KeyF25:
		return "f25"
	case window.KeyLeftShift:
		return "left shift"
	case window.KeyLeftControl:
		return "left control"
	case window.KeyLeftAlt:
		return "left alt"
	case window.KeyLeftSuper:
		return "left super"
	case window.KeyRightShift:
		return "right shift"
	case window.KeyRightControl:
		return "right control"
	case window.KeyRightAlt:
		return "right alt"
	case window.KeyRightSuper:
		return "right super"
	}
	return "unknown key"
}
