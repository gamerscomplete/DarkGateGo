package controller

import (
	"gitlab.com/g3n/engine/camera"
	"gitlab.com/gamerscomplete/DarkGateGo/client/input"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/tick"
	"time"
)

type IController interface {
	SetCamera(*camera.Camera)
	//	OnKey(string, interface{})
	OnCursor(evname string, ev interface{})
	Update(time.Duration)
	FixedUpdate(tick.Tick)
	Enable(bool)
	Enabled() bool
	DefaultBindings() []input.Binding
	OnInput(name string, state bool)
}
