package player

import (
	"gitlab.com/g3n/engine/gui"
)

type Toolbar struct {
	displaying  bool
	panel       *gui.Panel
	slots       []*Slot
	height      float32
	slotSize    float32
	numSlots    int
	slotPadding float32
	borderSize  float32
	activeSlot  int
}

//Toggles the toolbar slot and returns if the slot was enabled or disabled
func (toolbar *Toolbar) ToggleSlot(slot int) bool {
	if toolbar.ActiveSlot() == slot {
		//disable slot, otherwise enable slot and disable active slot
		toolbar.DisableActiveSlot()
		return false
	}
	toolbar.DisableActiveSlot()
	toolbar.SetSlotActive(slot)
	return true
}

//Disables whichever slot is active
func (toolbar *Toolbar) DisableActiveSlot() {
	if toolbar.ActiveSlot() == 0 {
		return
	}
	toolbar.slots[toolbar.activeSlot].SetActive(false)
	toolbar.activeSlot = -1
}

func (toolbar *Toolbar) enabled() bool {
	return toolbar.displaying
}

func (toolbar *Toolbar) SetSlotActive(slotNum int) {
	if slotNum > len(toolbar.slots) {
		slotNum = 1
	} else if slotNum < 1 {
		slotNum = len(toolbar.slots)
	}
	if toolbar.activeSlot != -1 {
		//disable the old active slot
		toolbar.slots[toolbar.activeSlot].SetActive(false)
	}
	//if the requested slot is empty dont activate it
	if toolbar.slots[slotNum-1].item == nil {
		toolbar.activeSlot = -1
		return
	}
	toolbar.activeSlot = slotNum - 1
	//enable the requested slot
	toolbar.slots[toolbar.activeSlot].SetActive(true)
}

func (toolbar *Toolbar) ActiveSlot() int {
	return toolbar.activeSlot + 1
}

func (toolbar *Toolbar) ActiveItem() *Item {
	if toolbar.activeSlot == -1 {
		return nil
	}
	return toolbar.slots[toolbar.activeSlot].item
}

func (toolbar *Toolbar) Enable(enable bool) {
	toolbar.displaying = enable
	toolbar.panel.SetEnabled(enable)
	toolbar.panel.SetRenderable(enable)
	for key, _ := range toolbar.slots {
		toolbar.slots[key].image.SetEnabled(enable)
		toolbar.slots[key].image.SetRenderable(enable)
	}
}

func (toolbar *Toolbar) addItem(item Item) bool {
	slot := toolbar.findFirstEmpty()
	if slot < 0 {
		return false
	}
	toolbar.slots[slot].item = &item
	toolbar.slots[slot].image.SetTexture(item.Icon)
	return true
}

func (toolbar *Toolbar) findFirstEmpty() int {
	for key, _ := range toolbar.slots {
		if toolbar.slots[key].item == nil {
			return key
		}
	}
	return -1
}
