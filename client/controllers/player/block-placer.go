package player

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/camera"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/experimental/collision"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"reflect"
	"time"
)

const (
	CUBE_SIZE = 1
)

//TODO: This probably doesnt need to be its own type
type Raycast struct {
	rc *collision.Raycaster
}

type BlockPlacer struct {
	root   *core.Node
	placer *gameCore.Entity
	//	camera       *camera.Camera
	caster       Raycast
	gameCore     *gameCore.GameCore
	snappedGrid  *gameCore.Grid
	snappedBlock *gameCore.Block
	snappedPos   gameCore.GridPos
	holdDistance float32
	opacity      float32
	color        math32.Color
}

func NewBlockPlacer(gameCore *gameCore.GameCore) *BlockPlacer {
	placer := &BlockPlacer{
		root:         core.NewNode(),
		gameCore:     gameCore,
		holdDistance: 5.0,
		opacity:      0.55,
		color:        math32.Color{0.5, 0.5, 0.75},
	}

	placer.caster.rc = collision.NewRaycaster(&math32.Vector3{}, &math32.Vector3{})
	placer.caster.rc.LinePrecision = 0.05
	placer.caster.rc.PointPrecision = 0.05
	return placer
}

func (placer *BlockPlacer) PlacerPosition() math32.Vector3 {
	return placer.placer.Position()
}

func (placer *BlockPlacer) PlacerRotation() math32.Vector3 {
	return placer.placer.Rotation()
}

func (placer *BlockPlacer) PlacerEnabled() bool {
	if placer.placer == nil {
		return false
	}
	return true
}

func (placer *BlockPlacer) Node() *core.Node {
	return placer.root
}

func (placer *BlockPlacer) EnablePlacer(entity *gameCore.Entity) {
	if placer.placer != nil {
		placer.root.Remove(placer.placer)
	}
	placer.placer = entity
	placer.placer.SetOpacity(placer.opacity)
	//	placer.placer.SetColor(placer.color)
	placer.root.Add(entity)
}

func (placer *BlockPlacer) DisablePlacer() {
	log.Info("disabling placer")
	placer.root.Remove(placer.placer)
	placer.placer = nil
}

func (placer *BlockPlacer) Update(camera *camera.Camera, diffTime time.Duration) {
	if placer == nil || placer.placer == nil {
		return
	}
	var rcPos math32.Vector3
	camera.WorldPosition(&rcPos)

	rcDir := math32.Vector3{0, 0, -1}

	var camQuat math32.Quaternion
	camera.WorldQuaternion(&camQuat)
	rcDir.ApplyQuaternion(&camQuat)

	placer.caster.rc.Set(&rcPos, &rcDir)

	//TODO: using the first grid hit isnt going to be a safe way of doing this \
	//	if there are 2 grids close to each other, the grid B may be checked first and hit before grid A is checked which would have been hit first logically
	found := false
	grids := placer.gameCore.GetGrids()
	/*
		if len(grids) > 0 {
			log.WithFields(log.Fields{
				"count": len(grids),
			}).Debug("check grids for collision")
		}
	*/
	for _, grid := range grids {
		/*
			log.WithFields(log.Fields{
				"childCount": len(grid.Node.Children()),
			}).Debug("checking intersections on grid")
		*/
		intersects := placer.caster.rc.IntersectObjects(grid.Children(), true)
		if len(intersects) == 0 {
			//			log.Debug("no intersections found")
			continue
		}
		found = true

		hitPoint := intersects[0].Point

		hitBlock, asserted := intersects[0].Object.(*gameCore.Block)
		if !asserted {
			parent := intersects[0].Object.Parent()
			if parent == nil {
				log.WithFields(log.Fields{
					"type": reflect.TypeOf(intersects[0].Object),
				}).Error("failed to assert intersected object to gameCore.Block")
				continue
			}
			hitBlock, asserted = parent.(*gameCore.Block)
			if !asserted {
				hitBlock, asserted = parent.Parent().(*gameCore.Block)
				if !asserted {
					log.WithFields(log.Fields{
						"type": reflect.TypeOf(parent),
					}).Warn("failed to assert inner inner block parent")
					continue
				}
			}
		}
		var transformPoint math32.Vector3
		hitBlock.WorldPosition(&transformPoint)
		//		transformPoint := hitBlock.Position()

		gPos := gridPos(transformPoint, hitPoint, hitBlock.GridPos)

		placer.snappedGrid = grid
		placer.snappedBlock = hitBlock
		placer.snappedPos = gPos

		snappedPos := snapPos(transformPoint, hitPoint)
		placer.placer.SetPositionVec(mapping.Position(snappedPos))
		break
	}

	if !found {
		placer.snappedGrid = nil
		placer.snappedBlock = nil

		rcDir.MultiplyScalar(placer.holdDistance)
		//reusing rcpos since we were already done with it and we need a new instance to add with here
		rcPos.Add(&rcDir)
		placer.placer.SetPositionVec(mapping.Position(rcPos))
	}
}

func gridPos(transformPos, hitPos math32.Vector3, gridPos gameCore.GridPos) gameCore.GridPos {
	if transformPos.Y-hitPos.Y == -0.5 {
		//top
		return gameCore.GridPos{X: gridPos.X, Y: gridPos.Y + 1, Z: gridPos.Z}
	} else if transformPos.Y-hitPos.Y == 0.5 {
		//bottom
		return gameCore.GridPos{X: gridPos.X, Y: gridPos.Y - 1, Z: gridPos.Z}
	} else if transformPos.X-hitPos.X == 0.5 {
		//right
		return gameCore.GridPos{X: gridPos.X - 1, Y: gridPos.Y, Z: gridPos.Z}
	} else if transformPos.X-hitPos.X == -0.5 {
		//left
		return gameCore.GridPos{X: gridPos.X + 1, Y: gridPos.Y, Z: gridPos.Z}
	} else if transformPos.Z-hitPos.Z == 0.5 {
		//front
		return gameCore.GridPos{X: gridPos.X, Y: gridPos.Y, Z: gridPos.Z - 1}
	} else if transformPos.Z-hitPos.Z == -0.5 {
		//back
		return gameCore.GridPos{X: gridPos.X, Y: gridPos.Y, Z: gridPos.Z + 1}
	} else {
		log.WithFields(log.Fields{
			"transformPos": transformPos,
			"hitPos":       hitPos,
			"gridPos":      gridPos,
		}).Error("gridPos failed to determine cube side")
	}
	return gameCore.GridPos{}
}

func snapPos(transformPos, hitPos math32.Vector3) math32.Vector3 {
	if transformPos.Y-hitPos.Y == -0.5 {
		//top
		return math32.Vector3{X: transformPos.X, Y: transformPos.Y + CUBE_SIZE, Z: transformPos.Z}
	} else if transformPos.Y-hitPos.Y == 0.5 {
		//bottom
		return math32.Vector3{X: transformPos.X, Y: transformPos.Y - CUBE_SIZE, Z: transformPos.Z}
	} else if transformPos.X-hitPos.X == -0.5 {
		//right
		return math32.Vector3{X: transformPos.X + CUBE_SIZE, Y: transformPos.Y, Z: transformPos.Z}
	} else if transformPos.X-hitPos.X == 0.5 {
		//left
		return math32.Vector3{X: transformPos.X - CUBE_SIZE, Y: transformPos.Y, Z: transformPos.Z}
	} else if transformPos.Z-hitPos.Z == -0.5 {
		//front
		return math32.Vector3{X: transformPos.X, Y: transformPos.Y, Z: transformPos.Z + CUBE_SIZE}
	} else if transformPos.Z-hitPos.Z == 0.5 {
		//back
		return math32.Vector3{X: transformPos.X, Y: transformPos.Y, Z: transformPos.Z - CUBE_SIZE}
	} else {
		log.WithFields(log.Fields{
			"transformPos": transformPos,
			"hitPos":       hitPos,
		}).Error("snapPos failed to determine cube side")
	}
	return math32.Vector3{}
}
