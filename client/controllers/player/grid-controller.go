package player

import (
	"gitlab.com/g3n/engine/camera"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
)

type GridController struct {
	camera *camera.Camera
	grid   *gameCore.Grid

	enabled bool
}

func NewGridController(cam *camera.Camera, grid *gameCore.Grid) *GridController {
	return &GridController{
		camera: cam,
		grid:   grid,
	}
}

func (gridController *GridController) SetCamera(cam *camera.Camera) {
	gridController.camera = cam
}

func (gridController *GridController) SetGrid(grid *gameCore.Grid) {
	gridController.grid = grid
}
