package player

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/camera"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/experimental/collision"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/window"
	"gitlab.com/gamerscomplete/DarkGateGo/client/input"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/network"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/tick"
	"time"
)

const (
	CMD_MAG_BOOTS = iota
	CMD_FORWARD
	CMD_BACKWARD
	CMD_LEFT
	CMD_RIGHT
	CMD_ROLL_LEFT
	CMD_ROLL_RIGHT
	CMD_DOWN
	CMD_UP
	CMD_BOOST
)

type PlayerController struct {
	camera *camera.Camera
	caster Raycast
	entity *gameCore.Entity
	//TODO: Dont really like that this is part of the player controller, but currently need it for the block placer
	network *network.ClientNetwork

	keyState map[int]bool

	rotSpeed        float32
	moveSpeed       float32
	verticalSpeed   float32
	rollSpeed       float32
	boostMultiplier float32
	rotStart        math32.Vector2

	Inventory *Inventory

	blockPlacer *BlockPlacer

	node *core.Node

	enabled bool
}

func NewPlayerController(cam *camera.Camera, entity *gameCore.Entity, inventory *Inventory, gCore *gameCore.GameCore, net *network.ClientNetwork) *PlayerController {
	entity.SetVisible(false)

	playerController := PlayerController{
		node:        core.NewNode(),
		camera:      cam,
		entity:      entity,
		network:     net,
		Inventory:   inventory,
		blockPlacer: NewBlockPlacer(gCore),
		caster: Raycast{
			rc: collision.NewRaycaster(&math32.Vector3{}, &math32.Vector3{}),
		},
		keyState:        make(map[int]bool),
		boostMultiplier: 25,
		rotSpeed:        0.004,
		moveSpeed:       0.25,
		verticalSpeed:   0.30,
		rollSpeed:       0.03,
	}

	//TODO: This is dumb that it needs to be shared all over, make it not that
	playerController.Inventory.SetPlacer(playerController.blockPlacer)

	playerController.node.Add(playerController.blockPlacer.Node())

	return &playerController
}

func (controller *PlayerController) Node() *core.Node {
	return controller.node
}

func (controller *PlayerController) OnCursor(evname string, ev interface{}) {
	mev := ev.(*window.CursorEvent)
	if controller.Enabled() && controller.entity != nil && !controller.Inventory.Enabled() {
		log.WithFields(log.Fields{
			"rotStart": controller.rotStart,
			"mev":      mev,
		}).Debug("updating entity axis")
		controller.entity.RotateOnAxis(&math32.Vector3{0, 1, 0}, -(mev.Xpos-controller.rotStart.X)*controller.rotSpeed)
		controller.entity.RotateOnAxis(&math32.Vector3{1, 0, 0}, -(mev.Ypos-controller.rotStart.Y)*controller.rotSpeed)
	}

	controller.rotStart.Set(mev.Xpos, mev.Ypos)
}

func (controller *PlayerController) Enabled() bool {
	return controller.enabled
}

func (controller *PlayerController) Enable(enable bool) {
	controller.enabled = enable
	controller.Inventory.toolbar.Enable(enable)
	if !enable {
		controller.Inventory.Enable(enable)
	}
}

func (controller *PlayerController) SetCamera(cam *camera.Camera) {
	controller.camera = cam
}

func (controller *PlayerController) SetEntity(entity *gameCore.Entity) {
	controller.entity = entity
}

func (controller *PlayerController) DefaultBindings() []input.Binding {
	defaultBindings := []input.Binding{
		input.Binding{
			Name: "forward",
			Keys: []window.Key{
				window.KeyW,
			},
		},
		input.Binding{
			Name: "back",
			Keys: []window.Key{
				window.KeyS,
			},
		},
		input.Binding{
			Name: "strafe_left",
			Keys: []window.Key{
				window.KeyA,
			},
		},
		input.Binding{
			Name: "strafe_right",
			Keys: []window.Key{
				window.KeyD,
			},
		},
		input.Binding{
			Name: "roll_left",
			Keys: []window.Key{
				window.KeyQ,
			},
		},
		input.Binding{
			Name: "roll_right",
			Keys: []window.Key{
				window.KeyE,
			},
		},
		input.Binding{
			Name: "up",
			Keys: []window.Key{
				window.KeySpace,
			},
		},
		input.Binding{
			Name: "down",
			Keys: []window.Key{
				window.KeyC,
			},
		},
		input.Binding{
			Name: "mag_boots",
			Keys: []window.Key{
				window.KeyM,
			},
		},
		input.Binding{
			Name: "boost",
			Keys: []window.Key{
				window.KeyLeftShift,
			},
		},
		input.Binding{
			Name: "inventory",
			Keys: []window.Key{
				window.KeyTab,
			},
		},
		input.Binding{
			Name: "toolbar_slot_1",
			Keys: []window.Key{
				window.Key1,
			},
		},
		input.Binding{
			Name: "toolbar_slot_2",
			Keys: []window.Key{
				window.Key2,
			},
		},
		input.Binding{
			Name: "toolbar_slot_3",
			Keys: []window.Key{
				window.Key3,
			},
		},
		input.Binding{
			Name: "toolbar_slot_4",
			Keys: []window.Key{
				window.Key4,
			},
		},
		input.Binding{
			Name: "toolbar_slot_5",
			Keys: []window.Key{
				window.Key5,
			},
		},
		input.Binding{
			Name: "toolbar_slot_6",
			Keys: []window.Key{
				window.Key6,
			},
		},
		input.Binding{
			Name: "toolbar_slot_7",
			Keys: []window.Key{
				window.Key7,
			},
		},
	}
	return defaultBindings
}

func (controller *PlayerController) OnInput(binding string, state bool) {
	if !controller.enabled {
		return
	}
	switch binding {
	case "mag_boots":
		//use keyup rather than down since we dont want to toggle twice when the keystate changes from down to up
		if !state {
			controller.keyState[CMD_MAG_BOOTS] = !controller.keyState[CMD_MAG_BOOTS]
		}
	case "forward":
		controller.keyState[CMD_FORWARD] = state
	case "back":
		controller.keyState[CMD_BACKWARD] = state
	case "strafe_left":
		controller.keyState[CMD_LEFT] = state
	case "strafe_right":
		controller.keyState[CMD_RIGHT] = state
	case "roll_left":
		controller.keyState[CMD_ROLL_LEFT] = state
	case "roll_right":
		controller.keyState[CMD_ROLL_RIGHT] = state
	case "down":
		controller.keyState[CMD_DOWN] = state
	case "up":
		controller.keyState[CMD_UP] = state
	case "boost":
		controller.keyState[CMD_BOOST] = state
	case "inventory":
		if !state {
			enabled := controller.Inventory.Enabled()
			controller.Inventory.Enable(!enabled)
		}
	case "toolbar_slot_1":
		if !state {
			//if the slot was set to active then enable the placer
			if controller.Inventory.toolbar.ToggleSlot(1) {
				controller.blockPlacer.EnablePlacer(controller.Inventory.toolbar.ActiveItem().Entity)
			}
		}
	case "toolbar_slot_2":
		if !state {
			if controller.Inventory.toolbar.ToggleSlot(2) {
				controller.blockPlacer.EnablePlacer(controller.Inventory.toolbar.ActiveItem().Entity)
			}
		}
	case "toolbar_slot_3":
		if !state {
			if controller.Inventory.toolbar.ToggleSlot(3) {
				controller.blockPlacer.EnablePlacer(controller.Inventory.toolbar.ActiveItem().Entity)
			}
		}
	case "toolbar_slot_4":
		if !state {
			if controller.Inventory.toolbar.ToggleSlot(4) {
				controller.blockPlacer.EnablePlacer(controller.Inventory.toolbar.ActiveItem().Entity)
			}
		}
	case "toolbar_slot_5":
		if !state {
			if controller.Inventory.toolbar.ToggleSlot(5) {
				controller.blockPlacer.EnablePlacer(controller.Inventory.toolbar.ActiveItem().Entity)
			}
		}
	case "toolbar_slot_6":
		if !state {
			if controller.Inventory.toolbar.ToggleSlot(6) {
				controller.blockPlacer.EnablePlacer(controller.Inventory.toolbar.ActiveItem().Entity)
			}
		}
	case "toolbar_slot_7":
		if !state {
			if controller.Inventory.toolbar.ToggleSlot(7) {
				controller.blockPlacer.EnablePlacer(controller.Inventory.toolbar.ActiveItem().Entity)
			}
		}
	default:
		log.Error("unknown key")
	}
}

func (controller *PlayerController) FixedUpdate(tick tick.Tick) {
}

func (controller *PlayerController) Update(deltaTime time.Duration) {
	if !controller.enabled || controller.Inventory.Enabled() {
		return
	}

	controller.blockPlacer.Update(controller.camera, deltaTime)

	var multiplier float32
	if controller.keyState[CMD_BOOST] {
		multiplier = controller.boostMultiplier
	} else {
		multiplier = 1
	}

	if controller.keyState[CMD_FORWARD] {
		//move forward
		controller.entity.TranslateOnAxis(&math32.Vector3{0, 0, 1}, -controller.moveSpeed*multiplier)
	} else if controller.keyState[CMD_BACKWARD] {
		//move backwards
		controller.entity.TranslateOnAxis(&math32.Vector3{0, 0, 1}, controller.moveSpeed*multiplier)
	}

	if controller.keyState[CMD_LEFT] {
		//move sideways left
		controller.entity.TranslateOnAxis(&math32.Vector3{1, 0, 0}, -controller.moveSpeed)
	} else if controller.keyState[CMD_RIGHT] {
		//move sideways right
		controller.entity.TranslateOnAxis(&math32.Vector3{1, 0, 0}, controller.moveSpeed)
	}

	if controller.keyState[CMD_ROLL_LEFT] {
		controller.entity.RotateOnAxis(&math32.Vector3{0, 0, 1}, controller.rollSpeed)
	} else if controller.keyState[CMD_ROLL_RIGHT] {
		controller.entity.RotateOnAxis(&math32.Vector3{0, 0, 1}, -controller.rollSpeed)
	}

	if controller.keyState[CMD_UP] {
		controller.entity.TranslateOnAxis(&math32.Vector3{0, 1, 0}, controller.verticalSpeed)
	} else if controller.keyState[CMD_DOWN] {
		controller.entity.TranslateOnAxis(&math32.Vector3{0, 1, 0}, -controller.verticalSpeed)
	}

	var rcPos math32.Vector3
	controller.entity.WorldPosition(&rcPos)

	rcDir := math32.Vector3{0, -1, 0}
	var entQuat math32.Quaternion
	controller.entity.WorldQuaternion(&entQuat)
	rcDir.ApplyQuaternion(&entQuat)

	controller.caster.rc.Set(&rcPos, &rcDir)
}

func (playerController *PlayerController) OnScroll(evname string, ev interface{}) {
	if event, found := ev.(*window.ScrollEvent); found {
		playerController.Inventory.toolbar.SetSlotActive(playerController.Inventory.toolbar.ActiveSlot() - int(event.Yoffset))
		activeItem := playerController.Inventory.toolbar.ActiveItem()
		if activeItem != nil {
			playerController.blockPlacer.EnablePlacer(activeItem.Entity)
		}
	}
}

func (playerController *PlayerController) OnMouse(evname string, ev interface{}) {
	//check if block placer is active
	if !playerController.blockPlacer.PlacerEnabled() || playerController.Inventory.Enabled() {
		return
	}
	me, found := ev.(*window.MouseEvent)
	if !found {
		log.Error("Unable to determine mouse event type")
	}
	activeItem := playerController.Inventory.toolbar.ActiveItem()
	if activeItem == nil {
		return
	}

	//left click
	if me.Button == 0 {
		if playerController.blockPlacer.snappedBlock != nil {
			log.WithFields(log.Fields{
				"gridID":   playerController.blockPlacer.snappedGrid.ID().String(),
				"blockID":  activeItem.EntityDefinitionID.String(),
				"position": playerController.blockPlacer.snappedPos,
			}).Debug("placing block on grid")
			addBlockMessage := network.PlaceOnGrid{
				GridID:  *playerController.blockPlacer.snappedGrid.ID(),
				BlockID: activeItem.EntityDefinitionID,
				Position: [3]int{
					playerController.blockPlacer.snappedPos.X,
					playerController.blockPlacer.snappedPos.Y,
					playerController.blockPlacer.snappedPos.Z,
				},
			}
			if err := playerController.network.SendMessage(addBlockMessage); err != nil {
				log.Warn("failed place on grid: ", err)
				return
			}
		} else {
			log.WithFields(log.Fields{
				//                  "pos": playerController.blockPlacer.PlacerPosition().String(),
				//                  "rot": playerController.blockPlacer.PlacerRotation().String(),
				"blockID": activeItem.EntityDefinitionID.String(),
			}).Debug("creating grid")

			createGridMessage := network.CreateGrid{
				Position: playerController.blockPlacer.PlacerPosition(),
				Rotation: playerController.blockPlacer.PlacerRotation(),
				BlockID:  activeItem.EntityDefinitionID,
			}
			if err := playerController.network.SendMessage(createGridMessage); err != nil {
				log.Warn("failed to create grid: ", err)
				return
			}
		}
		//right click
	} else if me.Button == 1 {
		if playerController.blockPlacer.snappedBlock != nil && playerController.blockPlacer.snappedGrid != nil {
			removeBlockMessage := network.RemoveBlock{
				GridID: *playerController.blockPlacer.snappedGrid.ID(),
				Position: [3]int{
					playerController.blockPlacer.snappedBlock.GridPos.X,
					playerController.blockPlacer.snappedBlock.GridPos.Y,
					playerController.blockPlacer.snappedBlock.GridPos.Z,
				},
			}
			if err := playerController.network.SendMessage(removeBlockMessage); err != nil {
				log.WithFields(log.Fields{
					"blockID": playerController.blockPlacer.snappedBlock.BlockID.String(),
				}).Warn("failed to remove block from grid")
			}
		}
	}
}
