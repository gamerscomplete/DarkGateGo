package player

import (
	"fmt"
	"github.com/nfnt/resize"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/app"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/texture"
	"gitlab.com/g3n/engine/window"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/uuid"
	"image"
	"reflect"
)

type Inventory struct {
	slots       []*Slot
	slotSize    float32
	slotPadding float32
	borderSize  float32
	panel       *gui.Panel
	displaying  bool
	rows        int
	columns     int
	toolbar     *Toolbar
	defaultTex  *texture.Texture2D

	dragging   bool
	dragPanel  *gui.Image
	dragItem   *Item
	dragSlot   int
	dragSource string

	app *app.Application

	placer *BlockPlacer
}

type Item struct {
	Name               string
	Description        string
	Count              int
	Icon               *texture.Texture2D
	Entity             *gameCore.Entity
	EntityDefinitionID uuid.UUID
}

func NewInventory(app *app.Application, scene *core.Node) *Inventory {
	inventory := Inventory{
		slotSize:    64,
		slotPadding: 5,
		rows:        5,
		columns:     6,
		displaying:  false,
		app:         app,
	}
	invWidth := (float32(inventory.slotSize+1) + (inventory.slotPadding * 2) + (inventory.borderSize * 2)) * float32(inventory.columns)
	invHeight := (float32(inventory.slotSize+1) + (inventory.slotPadding * 2) + (inventory.borderSize * 2)) * float32(inventory.rows)

	inventory.panel = gui.NewPanel(invWidth, invHeight)

	inventory.slots = make([]*Slot, inventory.rows*inventory.columns)

	rgba, err := texture.DecodeImage("assets/ducttapecross.jpg")
	if err != nil {
		log.Fatal("Error decoding texture:", err.Error())
		return nil
	}

	icon := resize.Resize(64, 64, rgba, resize.Lanczos3)
	iconRGBA, ok := icon.(*image.RGBA)
	if !ok {
		log.Fatal("unable to cast image to rgba: ", reflect.TypeOf(icon))
	}
	iconTex := texture.NewTexture2DFromRGBA(iconRGBA)

	inventory.defaultTex = iconTex

	i := 0
	for x := 0; x < inventory.rows; x++ {
		for y := 0; y < inventory.columns; y++ {
			im := gui.NewImageFromTex(inventory.defaultTex)

			// Slot padding * 2 since its padded on both sides
			im.SetPosition(
				(inventory.slotSize+(inventory.slotPadding*2)+(inventory.borderSize*2))*float32(y),
				(inventory.slotSize+(inventory.slotPadding*2)+(inventory.borderSize*2))*float32(x),
			)

			im.SetMargins(1, 1, 1, 1)
			im.SetBorders(
				inventory.borderSize,
				inventory.borderSize,
				inventory.borderSize,
				inventory.borderSize,
			)
			im.SetPaddings(
				inventory.slotPadding,
				inventory.slotPadding,
				inventory.slotPadding,
				inventory.slotPadding,
			)
			im.SetBordersColor(math32.NewColor("black"))
			im.SetColor4(&math32.Color4{255, 255, 255, 0})
			im.SetContentSize(inventory.slotSize, inventory.slotSize)
			inventory.panel.Add(im)

			slot := Slot{image: im}
			inventory.slots[i] = &slot

			curSlot := i
			slotEventDown := func(name string, ev interface{}) {
				if slot.item == nil {
					return
				}
				if inventory.app.KeyState().Pressed(window.KeyLeftShift) {
					emptySlot := inventory.toolbar.findFirstEmpty()
					if emptySlot < 0 {
						return
					}
					inventory.toolbar.slots[emptySlot].item = slot.item
					oldTex := inventory.toolbar.slots[emptySlot].image.SetTexture(slot.item.Icon)
					slot.image.SetTexture(oldTex)
					slot.item = nil
					return
				}

				inventory.dragging = true
				inventory.dragPanel.SetTexture(slot.item.Icon)
				inventory.dragPanel.SetEnabled(true)
				inventory.dragPanel.SetRenderable(true)
				inventory.dragItem = slot.item
				inventory.dragSlot = curSlot
				inventory.dragSource = "inventory"
			}
			slotEventUp := func(name string, ev interface{}) {
				if !inventory.dragging {
					return
				}
				curItem := inventory.slots[curSlot].item
				inventory.slots[curSlot].item = inventory.dragItem
				oldTex := inventory.slots[curSlot].image.SetTexture(inventory.dragItem.Icon)

				if inventory.dragSource == "toolbar" {
					inventory.toolbar.slots[inventory.dragSlot].item = curItem
					inventory.toolbar.slots[inventory.dragSlot].image.SetTexture(oldTex)
				} else {
					inventory.slots[inventory.dragSlot].item = curItem
					inventory.slots[inventory.dragSlot].image.SetTexture(oldTex)
				}

			}
			im.Subscribe(gui.OnMouseDown, slotEventDown)
			im.Subscribe(gui.OnMouseUp, slotEventUp)
			/*          im.Subscribe(gui.OnCursor, slotEvent)
			            im.Subscribe(gui.OnCursorEnter, slotEvent)
			            im.Subscribe(gui.OnCursorLeave, slotEvent)
			*/
			i++
		}
	}
	inventory.Enable(false)
	inventory.CreateToolbar(inventory.defaultTex)
	width, height := app.GetSize()
	inventory.Resize(width, height)
	scene.Add(inventory.panel)

	inventory.app.Subscribe(window.OnMouseUp, func(evname string, ev interface{}) {
		if !inventory.dragging {
			return
		}
		inventory.dragging = false
		inventory.dragPanel.SetEnabled(false)
		inventory.dragPanel.SetRenderable(false)
	})

	inventory.dragPanel = gui.NewImageFromTex(inventory.defaultTex)
	inventory.dragPanel.SetSize(64, 64)
	inventory.dragPanel.SetEnabled(false)
	inventory.dragPanel.SetRenderable(false)
	scene.Add(inventory.toolbar.panel)
	scene.Add(inventory.dragPanel)

	return &inventory
}

func (inventory *Inventory) SetPlacer(placer *BlockPlacer) {
	inventory.placer = placer
}

func (inventory *Inventory) CreateToolbar(icon *texture.Texture2D) {
	toolbar := Toolbar{
		slotSize:    64,
		height:      64,
		numSlots:    5,
		slotPadding: 5,
		borderSize:  1,
		activeSlot:  -1,
	}
	toolbar.panel = gui.NewPanel(
		(float32(toolbar.height+1)+(toolbar.slotPadding*2)+(toolbar.borderSize*2))*float32(toolbar.numSlots),
		float32(toolbar.height+1)+(toolbar.slotPadding*2)+(toolbar.borderSize*2),
	)

	toolbar.slots = make([]*Slot, toolbar.numSlots)
	for i := 0; i < toolbar.numSlots; i++ {
		im := gui.NewImageFromTex(icon)
		// Slot padding * 2 since its padded on both sides
		im.SetPosition((toolbar.slotSize+(toolbar.slotPadding*2)+(toolbar.borderSize*2))*float32(i), 0)
		im.SetMargins(1, 1, 1, 1)
		im.SetBorders(
			toolbar.borderSize,
			toolbar.borderSize,
			toolbar.borderSize,
			toolbar.borderSize,
		)
		im.SetPaddings(
			toolbar.slotPadding,
			toolbar.slotPadding,
			toolbar.slotPadding,
			toolbar.slotPadding,
		)
		im.SetBordersColor(math32.NewColor("black"))
		im.SetColor4(&math32.Color4{255, 255, 255, 0})
		im.SetContentSize(toolbar.slotSize, toolbar.slotSize)
		slot := Slot{image: im}
		toolbar.slots[i] = &slot
		toolbar.panel.Add(im)

		curSlot := i
		slotEventDown := func(name string, ev interface{}) {
			if slot.item == nil {
				fmt.Println("nil sloot")
				return
			}

			if inventory.app.KeyState().Pressed(window.KeyLeftShift) {
				emptySlot := inventory.findFirstEmpty()
				if emptySlot < 0 {
					fmt.Println("no empty slots")
					return
				}
				inventory.slots[emptySlot].item = slot.item
				oldTex := inventory.slots[emptySlot].image.SetTexture(slot.item.Icon)
				slot.image.SetTexture(oldTex)
				slot.item = nil
				return
			}
			fmt.Println("key not pressed")

			inventory.dragging = true
			inventory.dragPanel.SetTexture(slot.item.Icon)
			inventory.dragPanel.SetEnabled(true)
			inventory.dragPanel.SetRenderable(true)
			inventory.dragItem = slot.item
			inventory.dragSlot = curSlot
			inventory.dragSource = "toolbar"
		}
		slotEventUp := func(name string, ev interface{}) {
			if !inventory.dragging {
				return
			}
			curItem := inventory.toolbar.slots[curSlot].item
			inventory.toolbar.slots[curSlot].item = inventory.dragItem
			oldTex := inventory.toolbar.slots[curSlot].image.SetTexture(inventory.dragItem.Icon)

			if inventory.dragSource == "toolbar" {
				inventory.toolbar.slots[inventory.dragSlot].item = curItem
				inventory.toolbar.slots[inventory.dragSlot].image.SetTexture(oldTex)
			} else {
				inventory.slots[inventory.dragSlot].item = curItem
				inventory.slots[inventory.dragSlot].image.SetTexture(oldTex)
			}
			//if the current slot is active we may need to change the active placer
			if inventory.toolbar.slots[curSlot].active {
				//only need to worry about changing the placer if it is currently enabled while swapping slots
				if inventory.placer.PlacerEnabled() {
					//if the current slot is not now empty then enable the placer to that slot, otherwise turn the placer off since the current slot just became empty
					if inventory.toolbar.slots[curSlot].item.Entity != nil {
						inventory.placer.EnablePlacer(inventory.toolbar.slots[curSlot].item.Entity)
					} else {
						inventory.placer.DisablePlacer()
					}
				}
			}
		}
		im.Subscribe(gui.OnMouseDown, slotEventDown)
		im.Subscribe(gui.OnMouseUp, slotEventUp)

	}
	inventory.toolbar = &toolbar
}

func (inventory *Inventory) Resize(width, height int) {
	inventory.toolbar.panel.SetPosition(
		(float32(width)-((inventory.toolbar.slotSize+(inventory.toolbar.slotPadding*2))*float32(inventory.toolbar.numSlots))-(inventory.toolbar.borderSize*2))/2,
		float32(height)-inventory.toolbar.height-10-(inventory.toolbar.slotPadding*2)-(inventory.toolbar.borderSize*2),
	)
}

func (inventory *Inventory) Enable(enable bool) {
	inventory.displaying = enable
	inventory.panel.SetEnabled(enable)
	inventory.panel.SetRenderable(enable)
	for key, _ := range inventory.slots {
		if inventory.slots[key].image == nil {
			log.Warn("hit empty key trying to disable:", key)
			continue
		}
		inventory.slots[key].image.SetEnabled(enable)
		inventory.slots[key].image.SetRenderable(enable)
	}
	if enable {
		inventory.app.IWindow.SetCursorMode(window.CursorNormal)
	} else {
		inventory.app.IWindow.SetCursorMode(window.CursorDisabled)
	}
}

func (inventory *Inventory) Enabled() bool {
	return inventory.displaying
}

func (inventory *Inventory) AddItem(item Item) bool {
	slot := inventory.findFirstEmpty()
	if slot < 0 {
		return false
	}

	inventory.slots[slot].item = &item
	inventory.slots[slot].image.SetTexture(item.Icon)
	return true
}

func (inventory *Inventory) findFirstEmpty() int {
	for key, _ := range inventory.slots {
		if inventory.slots[key].item == nil {
			return key
		}
	}
	return -1
}

func (inventory *Inventory) Update() {
	if inventory.dragging {
		win := inventory.app.IWindow.(*window.GlfwWindow)
		x, y := win.GetCursorPos()
		inventory.dragPanel.SetPosition(float32(x)+3, float32(y)+3)
	}
}
