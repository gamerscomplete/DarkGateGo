package player

import (
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/math32"
)

type Slot struct {
	active bool
	image  *gui.Image
	item   *Item
}

func (slot *Slot) Active() bool {
	return slot.active
}

func (slot *Slot) SetActive(active bool) {
	slot.active = active
	if active {
		slot.image.SetBordersColor(math32.NewColor("white"))
		//Change the background color between the image and borders to something different
	} else {
		slot.image.SetBordersColor(math32.NewColor("black"))
		//reset the background color
	}
}
