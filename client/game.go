package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/g3n/engine/core"
	//	"gitlab.com/g3n/engine/experimental/physics"
	"gitlab.com/g3n/engine/graphic"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/window"
	"gitlab.com/gamerscomplete/DarkGateGo/client/controllers"
	"gitlab.com/gamerscomplete/DarkGateGo/client/controllers/player"
	"gitlab.com/gamerscomplete/DarkGateGo/client/input"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-manager"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/game-core/sync-actions"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/mapping"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/network"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/tick"
	"gitlab.com/gamerscomplete/terrain-manager"
	"gitlab.com/gamerscomplete/uuid"
	"reflect"
	"time"
)

const (
	CONTROL_ADDR = "gameserver.dark-gate.com"
	CONTROL_PORT = 6897

	ASSET_DIR = "assets/downloads"
)

type Game struct {
	terrainManager *terrainManager.TerrainManager
	app            *App
	cursorEnabled  bool
	inventory      *player.Inventory
	//TODO: This should be moved into its own loading type
	loadingInfoText *gui.Label
	sceneName       string
	loginScene      *core.Node
	loadingScene    *core.Node
	gameScene       *core.Node
	network         *Network
	syncQueue       []syncActions.Action
	rawInventory    map[uuid.UUID]int
	assMan          *assetManager.AssetManager
	core            *gameCore.GameCore
	playerEntityID  uuid.UUID
	playerEntity    *gameCore.Entity
	inputRouter     *input.InputRouter
	controller      controller.IController

	menu *Menu

	chat *Chat
}

func NewGame() (*Game, error) {
	game := Game{}
	game.app = CreateApp()

	game.loginScene = core.NewNode()
	game.loadingScene = core.NewNode()
	game.gameScene = core.NewNode()

	game.setupLoginScene()
	game.setupLoadingScene()

	game.inputRouter = input.NewInputRouter(game.app.IWindow)
	gameDefaultBindings := []input.Binding{
		input.Binding{
			Name: "exit",
			Keys: []window.Key{
				window.KeyF1,
			},
		},
		input.Binding{
			Name: "chat",
			Keys: []window.Key{
				window.KeyEnter,
			},
		},
		input.Binding{
			Name: "menu",
			Keys: []window.Key{
				window.KeyEscape,
			},
		},
	}
	gameController := input.Controller{
		Bindings:      gameDefaultBindings,
		InputCallback: game.onInput,
	}

	game.inputRouter.RegisterController("game", gameController, true)

	game.network = NewNetwork(game.messageProcessor, &game)
	if err := game.network.ConnectControl(CONTROL_ADDR, CONTROL_PORT); err != nil {
		return &game, err
	}

	game.assMan = assetManager.NewAssetManager(game.network.FetchAsset, ASSET_DIR)

	game.core = gameCore.NewGameCore(game.assMan, game.network.controlClient)
	game.gameScene.Add(game.core)

	game.LoadLoginScene()

	return &game, nil
}

func (game *Game) setupGameScene() {
	//Setup the physics simulation
	//	game.simulation = physics.NewSimulation(game.gameScene)

	//Create a constant force downwards
	//	gravity := physics.NewConstantForceField(&math32.Vector3{0, -0.98, 0})
	//	game.simulation.AddForceField(gravity)
	//Create a terrain manager config to set how far to render patches
	/*
		tConfig := terrainManager.DefaultConfig
		tConfig.MaxDistance = 120
		tConfig.MinDistance = 5
		tConfig.Distance = 10
	*/
	game.app.Subscribe(window.OnWindowSize, func(evname string, ev interface{}) { game.OnWindowResize() })

	/*
		//Setup the terrain manager
		var err error
		game.terrainManager, err = terrainManager.NewTerrainManager(tConfig)
		if err != nil {
			panic("Failed to create terrain manager: " + err.Error())

		}
		game.gameScene.Add(game.terrainManager.Node())
	*/

	game.core.RegisterFixedUpdateCallback("game", game.FixedUpdate)

	game.AddSkybox()
	//Drag panel must come after the inventory so that its zlayer is ontop of the inventory

	//	game.app.Subscribe(window.OnScroll, game.onScroll)

	//	game.app.Subscribe(window.OnMouseDown, game.onMouse)
}

func (game *Game) LoadLoginScene() {
	game.sceneName = "login"
	game.app.SwitchScene(game.loginScene)
}

//TODO: inventory stuffing should probably be moved to the player controller since its specific to that
func (game *Game) applyInventory(items map[uuid.UUID]int) {
	for key, value := range items {
		log.WithFields(log.Fields{
			"ItemDefinitionID": key.String(),
		}).Debug("fetching item definition")
		itemDefinition, err := game.network.controlClient.GetItemDefinition(key)
		if err != nil {
			log.Error("Failed to fetch item definition:", err)
			continue
		}

		log.WithFields(log.Fields{
			"IconTexPackageID": itemDefinition.IconTexPackage.String(),
		}).Debug("loading icon package")
		if err := game.assMan.LoadAsset(itemDefinition.IconTexPackage); err != nil {
			log.Error("Failed to load texture package:", err)
			continue
		}

		icon, err := game.assMan.GetTexture(itemDefinition.IconTexPackage, false)
		if err != nil {
			log.Error("Failed to get texture from asset manager:", err)
			continue
		}

		entity, err := game.core.CreateEntityFromDefinition(itemDefinition.EntityDefinitionID, true)
		if err != nil {
			log.Error("Failed to create entity from definition: ", err)
			continue
		}

		//TODO: item does not belong in controllers, maybe moved to game-core or similar
		newItem := player.Item{
			Name:               itemDefinition.Name,
			Description:        itemDefinition.Description,
			Icon:               icon,
			Count:              value,
			Entity:             entity,
			EntityDefinitionID: itemDefinition.EntityDefinitionID,
		}
		if !game.inventory.AddItem(newItem) {
			log.Error("Failed to add item to inventory")
			continue
		}
		log.Debug("Added item ", itemDefinition.Name, " to inventory")
	}
}

//This is called once the player is logged in and kicks off the loading sequence. Should probably be renamed to something better
func (game *Game) LoadLoadingScene() {
	//Change to the loading screen to show something while the game scene is being setup
	game.sceneName = "loading"
	game.app.SwitchScene(game.loadingScene)

	//
	game.setupGameScene()
	inventory := player.NewInventory(game.app.Application, game.gameScene)
	game.inventory = inventory
	log.WithFields(log.Fields{
		"count": len(game.syncQueue),
	}).Info("applying snapshot")
	game.core.ApplyActions(game.syncQueue)
	game.applyInventory(game.rawInventory)
	game.playerEntity = game.core.GetEntity(game.playerEntityID)
	if game.playerEntity == nil {
		log.WithFields(log.Fields{
			"playerEntityID": game.playerEntityID.String(),
		}).Fatal("failed to get entity from id")
	}

	game.playerEntity.Add(game.app.Camera())
	game.app.Camera().SetPositionVec(&math32.Vector3{0, 0, 0})
	game.app.Camera().SetRotationVec(&math32.Vector3{0, 0, 0})

	//create a player controller
	pController := player.NewPlayerController(game.app.Camera(), game.playerEntity, inventory, game.core, game.network.gameServerClient)
	game.controller = pController
	game.gameScene.Add(pController.Node())

	playerController := input.Controller{
		Bindings:       pController.DefaultBindings(),
		InputCallback:  pController.OnInput,
		CursorCallback: pController.OnCursor,
		MouseCallback:  pController.OnMouse,
		ScrollCallback: pController.OnScroll,
	}

	game.inputRouter.RegisterController("player", playerController, false)

	game.menu = NewMenu(game.inputRouter)
	game.gameScene.Add(game.menu.Node())

	//
	game.chat = NewChat(game.network.controlClient.SendChatMessage, game)
	game.gameScene.Add(game.chat.Root())
	game.network.controlClient.RegisterChatHandler(game.chat.appendMessage)

	game.LoadGameScene()
}

func (game *Game) LoadGameScene() {
	game.sceneName = "game"
	game.app.SwitchScene(game.gameScene)
	game.disableCursor()
	game.controller.Enable(true)
}

func (game *Game) setupLoginScene() {
	//create a full screen panel
	width, height := game.app.GetSize()
	background, err := gui.NewImage("assets/fire_ring.jpg")
	if err != nil {
		fmt.Println("Failed to load the login background:", err)
	}
	background.SetContentSize(float32(width), float32(height))
	//create 2 Gedit boxes
	username := gui.NewEdit(100, "Username")
	username.SetPosition(10, 10)
	gui.Manager().SetKeyFocus(username)
	password := gui.NewEdit(100, "Password")
	username.Subscribe(gui.OnKeyDown, func(evname string, ev interface{}) {
		kev := ev.(*window.KeyEvent)
		if kev.Key == window.KeyTab {
			gui.Manager().SetKeyFocus(password)
		}
	})

	messagePanel := gui.NewLabel("")
	messagePanel.SetPosition(10, 70)

	password.SetPosition(10, 40)
	password.Subscribe(gui.OnKeyDown, func(evname string, ev interface{}) {
		kev := ev.(*window.KeyEvent)
		if kev.Key == window.KeyEnter {
			if reason, success := game.doLogin(username.Text(), password.Text()); !success {
				fmt.Println("Login failed")
				//TODO: setting messagepanel text should be done in dologin
				messagePanel.SetText(reason)
			}
		}
	})

	registerButton := gui.NewButton("register")
	registerButton.SetPosition(120, 35)
	registerButton.Label.SetFontSize(12)
	registerButton.Subscribe(gui.OnClick, func(name string, ev interface{}) {
		if reason, success := game.registerUser(username.Text(), password.Text()); !success {
			messagePanel.SetText(reason)
		} else {
			messagePanel.SetText("Registration successful")
		}
	})
	background.Add(registerButton)

	game.loginScene.Add(background)
	background.Add(username)
	background.Add(password)
	background.Add(messagePanel)

	//TODO: look at implementing a password obfuscation editbox
}

func (game *Game) registerUser(username string, password string) (string, bool) {
	if message, success := game.network.RegisterUser(username, password); !success {
		fmt.Println("Register failed")
		return message, false
	}
	return "", true
}

func (game *Game) doLogin(username string, password string) (string, bool) {
	if message, success := game.network.Login(username, password); !success {
		fmt.Println("Login failed:", message)
		return message, false
	}
	go game.LoadLoadingScene()
	return "", true
}

func (game *Game) setupLoadingScene() {
	width, height := game.app.GetSize()
	background := gui.NewPanel(float32(width), float32(height))
	loadingText := gui.NewLabel("loading..")
	loadingText.SetPosition(float32(width/2), float32(height/2))

	game.loadingInfoText = gui.NewLabel("Loading: ")
	game.loadingInfoText.SetPosition(float32(width/2), float32(height/2)+40)

	game.loadingScene.Add(background)
	background.Add(loadingText)
	background.Add(game.loadingInfoText)
}

func (game *Game) onInput(binding string, state bool) {
	switch binding {
	case "exit":
		game.shutdown()
	case "menu":
		fmt.Println("menu input recieved", state, game.menu.Enabled())
		if !state {
			fmt.Println("state up")
			if game.menu.Enabled() {
				fmt.Println("game enabled. disabling")
				game.menu.Enable(false)
				game.controller.Enable(true)
				game.disableCursor()
			} else {
				game.menu.Enable(true)
				game.controller.Enable(false)
				game.enableCursor()
			}
		}
	case "chat":
		if !state {
			if game.chat.Enabled() {
				game.chat.Enable(false)
				game.controller.Enable(true)
			} else {
				game.chat.Enable(true)
				game.controller.Enable(false)
			}
		}
	default:
		log.WithFields(log.Fields{
			"binding": binding,
		}).Error("unknown input command for game")
	}
}

func (game *Game) lockInput() {
	game.controller.Enable(!game.controller.Enabled())
	if game.controller.Enabled() {
		fmt.Println("Enabling controller")
	} else {
		fmt.Println("Disabling controller")
	}
}

/*
func (game *Game) resetCursorPos() {
	x, y := game.app.GetSize()
	game.setCursorPos(x/2, y/2)
}

func (game *Game) setCursorPos(x, y int) {
	game.app.IWindow.SetCursorPos(x, y)
}
*/

func (game *Game) toggleCursor() {
	if game.cursorEnabled {
		game.disableCursor()
	} else {
		game.enableCursor()
	}
}

func (game *Game) disableCursor() {
	game.cursorEnabled = false
	game.app.IWindow.SetCursorMode(window.CursorDisabled)
}

func (game *Game) enableCursor() {
	game.cursorEnabled = true
	game.app.IWindow.SetCursorMode(window.CursorNormal)
	//	game.app.freelook.first = true
}

//func (game *Game) Update(a *App, deltaTime time.Duration) {
func (game *Game) FixedUpdate(tick tick.Tick, deltaTime time.Duration) {

	//TODO: Change both of these update functions to use the update signature from App so they can just be subscribed individually
	if game.controller != nil {
		game.controller.FixedUpdate(tick)
		game.controller.Update(deltaTime)
	}

	//	game.terrainManager.Update(game.app.Camera().Position())
	//	game.simulation.Step(float32(deltaTime.Seconds()))

	if game.inventory != nil {
		game.inventory.Update()
	}

	if game.playerEntity != nil && game.playerEntity.Changed() {
		if err := game.network.gameServerClient.SendMessage(network.SyncPosition{Position: mapping.Position(game.playerEntity.Position()), Rotation: game.playerEntity.Rotation()}); err != nil {
			log.Error("failed to send SyncPosition message")
		} else {
			game.playerEntity.SetChanged(false)
		}
	}
}

func (game *Game) shutdown() {
	fmt.Println("Shutdown recieved")
	game.app.Exit()
}

func (game *Game) AddSkybox() {
	//TODO: Move this into the asset management system in a preload
	skyboxData := graphic.SkyboxData{DirAndPrefix: "assets/skybox/red/", Extension: "png", Suffixes: [6]string{"bkg2_right1", "bkg2_left2", "bkg2_top3", "bkg2_bottom4", "bkg2_front5", "bkg2_back6"}}

	skybox, err := graphic.NewSkybox(skyboxData)
	if err != nil {
		fmt.Println("Failed to create skybox:", err)
		return
	}
	game.gameScene.Add(skybox)
}

func (game *Game) OnWindowResize() {
	width, height := game.app.GetSize()
	game.inventory.Resize(width, height)
}

//Process messages from the game server
func (game *Game) messageProcessor(message network.TransitMessage) {
	log.WithFields(log.Fields{
		"type": reflect.TypeOf(message.Message),
	}).Debug("client received message to processor")
	switch m := message.Message.(type) {
	case *network.ApplyActions:
		log.WithFields(log.Fields{
			"count": len(m.Actions),
		}).Debug("applying actions")
		game.core.ApplyActions(m.Actions)
	default:
		log.WithFields(log.Fields{
			"type": reflect.TypeOf(m),
		}).Error("received unknown message type")
	}
}
