package main

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/DarkGateGo/libs/asset-package"
	gameNet "gitlab.com/gamerscomplete/DarkGateGo/libs/network"
	"gitlab.com/gamerscomplete/DarkGateGo/services/control-stream/game-control-client"
	"gitlab.com/gamerscomplete/uuid"
	"time"
)

type Network struct {
	controlClient    *controlClient.Client
	controlConnected bool
	gameServerClient *gameNet.ClientNetwork
	gameConnected    bool
	messageProcessor func(gameNet.TransitMessage)
	game             *Game
}

func NewNetwork(processor func(gameNet.TransitMessage), game *Game) *Network {
	return &Network{messageProcessor: processor, game: game}
}

func (net *Network) ConnectControl(addr string, port int) error {
	if net.controlConnected {
		return errors.New("Attempted to connect to control server while already connected")
	}
	var err error
	net.controlClient, err = controlClient.NewClient(addr, port)
	if err == nil {
		net.controlConnected = true
	}
	return err
}

func (net *Network) FetchAsset(id *uuid.UUID) (assetPackage.IAssetPackage, error) {
	return net.controlClient.FetchAsset(id)
}

func (net *Network) Login(username, password string) (string, bool) {
	if !net.controlConnected {
		return "Not connected to control server", false
	}
	authReply, err := net.controlClient.Authenticate(username, password)
	if authReply.Reason != "" {
		log.Info("Authentication failed: ", authReply.Reason)
		return authReply.Reason, false
	} else if authReply.Token == "" {
		log.Info("Authentication failed: Empty token")
		return "Server error", false
	} else if err != nil {
		return "Authentication server failure", false
	}
	log.Info("Control authentication successful")

	net.gameServerClient, err = gameNet.DialServer(authReply.GameServer.Addr, authReply.GameServer.Port, net.messageProcessor)
	if err != nil {
		log.WithFields(log.Fields{
			"host": authReply.GameServer.Addr,
			"port": authReply.GameServer.Port,
		}).Fatal("Failed to connect to game server:", err)
		return "Server error", false
	}

	authMessage := gameNet.AuthMessage{Token: authReply.Token}
	//	var reply gameNet.AuthReply
	var reply gameNet.Message
	err = net.gameServerClient.SendTwoWayMessage(authMessage, &reply, time.Second*5)
	if err != nil {
		log.Info("Failed to authenticate to gameserver: " + err.Error())
		return "Server error", false
	}

	gameAuthReply, assertSuccess := reply.(*gameNet.AuthReply)
	if !assertSuccess {
		return "Assertion failure", false
	}

	if !gameAuthReply.Success {
		return "Failed to auth with game server", false
	}

	//TODO: Move these into the loading object instead of having the loading screen pull these in
	net.game.syncQueue = gameAuthReply.Snapshot
	net.game.rawInventory = gameAuthReply.Inventory
	net.game.playerEntityID = gameAuthReply.PlayerEntity

	net.gameConnected = true
	return "", true
}

func (net *Network) RegisterUser(username string, password string) (string, bool) {
	registerSuccess, registerErr := net.controlClient.Register(username, password)
	if !registerSuccess {
		//        log(1, "Registration failed:", registerErr)
		return registerErr, false
	} else if registerErr != "" {
		//      log(1, "Registration failed:", registerErr)
		return registerErr, false
	}
	//log(1, "Registration successful")
	return "", true
}
