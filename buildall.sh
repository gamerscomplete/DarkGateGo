#!/bin/bash

for i in $(ls services); do
	echo $i
	cd services/$i
	./build.sh || exit $?
	cd ../../
done
cd client && go build
